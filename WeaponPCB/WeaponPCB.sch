EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L WeaponPCB-rescue:ATtiny861A-PU-MCU_Microchip_ATtiny U1
U 1 1 5B4CB52F
P 2400 1950
F 0 "U1" H 1850 3000 50  0000 C CNN
F 1 "ATTINY861A-XUR" H 2400 1950 50  0000 C CNN
F 2 "Package_SO:TSSOP-20_4.4x6.5mm_P0.65mm" H 2450 1950 50  0001 C CIN
F 3 "" H 2400 1950 50  0001 C CNN
F 4 "ATTINY861A-XURCT-ND" H 2400 1950 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/ATTINY861A-XUR/ATTINY861A-XURCT-ND/2774371/?itemSeq=360453001" H 2400 1950 50  0001 C CNN "DigikeyPL"
	1    2400 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5B4CB7F9
P 1400 2000
F 0 "C1" H 1425 2100 50  0000 L CNN
F 1 "100n" H 1425 1900 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1438 1850 50  0001 C CNN
F 3 "" H 1400 2000 50  0001 C CNN
F 4 "1276-2449-1-ND" H 1400 2000 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/samsung-electro-mechanics/CL21B104MACNNNC/1276-2449-1-ND/3890535" H 1400 2000 50  0001 C CNN "DigikeyPL"
	1    1400 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5B4CB9E0
P 3300 2900
F 0 "R1" V 3380 2900 50  0000 C CNN
F 1 "10k" V 3300 2900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3230 2900 50  0001 C CNN
F 3 "" H 3300 2900 50  0001 C CNN
F 4 "" H 3300 2900 50  0001 C CNN "DigikeyPN"
F 5 "" H 3300 2900 50  0001 C CNN "DigikeyPL"
	1    3300 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 2750 3300 2750
Wire Wire Line
	3300 3050 3300 3400
Text Label 3300 3300 0    60   ~ 0
VCC
$Comp
L WeaponPCB-rescue:USB_A-Connector J1
U 1 1 5B4CBB2E
P 1250 6500
F 0 "J1" H 1050 6950 50  0000 L CNN
F 1 "Connection" H 1050 6850 50  0000 L CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x05_P4.6mm_D0.9mm_OD2.1mm_Relief" H 1400 6450 50  0001 C CNN
F 3 "" H 1400 6450 50  0001 C CNN
	1    1250 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 6300 2050 6300
Wire Wire Line
	1250 6900 1250 6950
Wire Wire Line
	1150 6900 1150 6950
Wire Wire Line
	1150 6950 1250 6950
Connection ~ 1250 6950
$Comp
L power:GND #PWR01
U 1 1 5B4CBF34
P 1250 7450
F 0 "#PWR01" H 1250 7200 50  0001 C CNN
F 1 "GND" H 1250 7300 50  0000 C CNN
F 2 "" H 1250 7450 50  0001 C CNN
F 3 "" H 1250 7450 50  0001 C CNN
	1    1250 7450
	1    0    0    -1  
$EndComp
Text Label 1850 6300 0    60   ~ 0
VIN
Text Notes 500  7800 0    60   ~ 0
Using USB cable here, but not following USB standard
Text Label 1650 6600 0    60   ~ 0
SCL
Text Label 1650 6500 0    60   ~ 0
SDA
Wire Wire Line
	3100 1350 4000 1350
Wire Wire Line
	3100 1150 4000 1150
Text Label 3700 1150 0    60   ~ 0
SDA
Text Label 3700 1350 0    60   ~ 0
SCL
Text Notes 1450 3050 0    60   ~ 0
Attiny 261/461/861
$Comp
L Connector_Generic:Conn_01x02 J13
U 1 1 5B4CCCE1
P 9150 3000
F 0 "J13" H 9150 3100 50  0000 C CNN
F 1 "IR_Cap" H 9150 2800 50  0000 C CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x02_P4.6mm_D0.9mm_OD2.1mm" H 9150 3000 50  0001 C CNN
F 3 "" H 9150 3000 50  0001 C CNN
F 4 "P5121-ND" H 9150 3000 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/panasonic-electronic-components/ECA-0JM153/P5121-ND/244980" H 9150 3000 50  0001 C CNN "DigikeyPL"
	1    9150 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 2350 6400 2400
Wire Wire Line
	3100 2550 6100 2550
Wire Wire Line
	6100 2550 6100 2600
$Comp
L power:GND #PWR04
U 1 1 5B4CE1F5
P 8700 6250
F 0 "#PWR04" H 8700 6000 50  0001 C CNN
F 1 "GND" H 8700 6100 50  0000 C CNN
F 2 "" H 8700 6250 50  0001 C CNN
F 3 "" H 8700 6250 50  0001 C CNN
	1    8700 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 5000 6400 4900
Wire Wire Line
	6400 4500 6400 4400
Wire Wire Line
	3100 2450 4950 2450
Wire Wire Line
	4950 4700 6100 4700
$Comp
L Connector_Generic:Conn_01x02 J11
U 1 1 5B4CEB7B
P 1950 4200
F 0 "J11" H 1950 4300 50  0000 C CNN
F 1 "Trigger" H 1950 4000 50  0000 C CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x02_P4.6mm_D0.9mm_OD2.1mm" H 1950 4200 50  0001 C CNN
F 3 "" H 1950 4200 50  0001 C CNN
	1    1950 4200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5B4CEC88
P 1600 5050
F 0 "#PWR06" H 1600 4800 50  0001 C CNN
F 1 "GND" H 1600 4900 50  0000 C CNN
F 2 "" H 1600 5050 50  0001 C CNN
F 3 "" H 1600 5050 50  0001 C CNN
	1    1600 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 4300 1600 4800
Wire Wire Line
	1600 4300 1750 4300
Connection ~ 1600 4800
Text Label 900  4200 0    60   ~ 0
Input_Trigger
Text Label 1000 4700 0    60   ~ 0
Input_Reload
Wire Wire Line
	3100 2650 4150 2650
Text Label 4150 2650 2    60   ~ 0
Input_Trigger
Wire Wire Line
	3100 1550 4000 1550
Text Label 3350 1550 0    60   ~ 0
Input_Reload
$Comp
L Connector_Generic:Conn_01x02 J8
U 1 1 5B4CCD4E
P 10350 5550
F 0 "J8" H 10350 5650 50  0000 C CNN
F 1 "Speaker" H 10350 5350 50  0000 C CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x02_P4.6mm_D0.9mm_OD2.1mm" H 10350 5550 50  0001 C CNN
F 3 "" H 10350 5550 50  0001 C CNN
	1    10350 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 2350 4000 2350
Text Label 4000 2350 2    60   ~ 0
Output_Speaker
$Comp
L Device:R R3
U 1 1 5B4CFBF5
P 7950 5550
F 0 "R3" V 8030 5550 50  0000 C CNN
F 1 "100" V 7950 5550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7880 5550 50  0001 C CNN
F 3 "" H 7950 5550 50  0001 C CNN
F 4 "" H 7950 5550 50  0001 C CNN "DigikeyPN"
F 5 "" H 7950 5550 50  0001 C CNN "DigikeyPL"
	1    7950 5550
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 5B4CFEB5
P 9000 5550
F 0 "R6" V 9080 5550 50  0000 C CNN
F 1 "8.2" V 9000 5550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8930 5550 50  0001 C CNN
F 3 "" H 9000 5550 50  0001 C CNN
F 4 "" H 9000 5550 50  0001 C CNN "DigikeyPN"
F 5 "" H 9000 5550 50  0001 C CNN "DigikeyPL"
	1    9000 5550
	0    1    1    0   
$EndComp
$Comp
L Device:CP C3
U 1 1 5B4CFF53
P 9400 5850
F 0 "C3" H 9425 5950 50  0000 L CNN
F 1 "2.2u" H 9425 5750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9438 5700 50  0001 C CNN
F 3 "" H 9400 5850 50  0001 C CNN
F 4 "1276-1183-1-ND" H 9400 5850 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/samsung-electro-mechanics/CL10A225KQ8NNNC/1276-1183-1-ND/3889269" H 9400 5850 50  0001 C CNN "DigikeyPL"
	1    9400 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 5550 8400 5550
Wire Wire Line
	8400 5150 8400 5550
Connection ~ 8400 5550
Wire Wire Line
	8700 5350 8700 5550
Wire Wire Line
	8700 5550 8850 5550
Connection ~ 8700 5550
Wire Wire Line
	9150 5550 9400 5550
Wire Wire Line
	9900 5550 10150 5550
Wire Wire Line
	9400 5700 9400 5550
Connection ~ 9400 5550
Wire Wire Line
	8700 6100 8700 6200
Wire Wire Line
	8700 6200 9400 6200
Wire Wire Line
	9400 6200 9400 6000
Connection ~ 8700 6200
Wire Wire Line
	10150 6200 10150 5650
Connection ~ 9400 6200
Wire Wire Line
	8700 4950 8700 4700
Text Label 8700 4950 1    60   ~ 0
VCC
Wire Wire Line
	7800 5550 7050 5550
Text Label 7100 5550 0    60   ~ 0
Output_Speaker
$Comp
L power:GND #PWR07
U 1 1 5B4D0744
P 6500 2000
F 0 "#PWR07" H 6500 1750 50  0001 C CNN
F 1 "GND" H 6500 1850 50  0000 C CNN
F 2 "" H 6500 2000 50  0001 C CNN
F 3 "" H 6500 2000 50  0001 C CNN
	1    6500 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5B4D0788
P 3700 1850
F 0 "R2" V 3780 1850 50  0000 C CNN
F 1 "820" V 3700 1850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3630 1850 50  0001 C CNN
F 3 "" H 3700 1850 50  0001 C CNN
F 4 "" H 3700 1850 50  0001 C CNN "DigikeyPN"
F 5 "" H 3700 1850 50  0001 C CNN "DigikeyPL"
	1    3700 1850
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 5B4D085B
P 4050 1750
F 0 "R4" V 4130 1750 50  0000 C CNN
F 1 "1000" V 4050 1750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3980 1750 50  0001 C CNN
F 3 "" H 4050 1750 50  0001 C CNN
F 4 "" H 4050 1750 50  0001 C CNN "DigikeyPN"
F 5 "" H 4050 1750 50  0001 C CNN "DigikeyPL"
	1    4050 1750
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 5B4D08B2
P 4350 1650
F 0 "R5" V 4430 1650 50  0000 C CNN
F 1 "560" V 4350 1650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4280 1650 50  0001 C CNN
F 3 "" H 4350 1650 50  0001 C CNN
F 4 "" H 4350 1650 50  0001 C CNN "DigikeyPN"
F 5 "" H 4350 1650 50  0001 C CNN "DigikeyPL"
	1    4350 1650
	0    1    1    0   
$EndComp
Wire Wire Line
	3100 1850 3550 1850
Wire Wire Line
	3100 1750 3900 1750
Wire Wire Line
	3100 1650 4200 1650
Wire Wire Line
	3100 2050 3850 2050
Wire Wire Line
	3100 2150 3850 2150
Wire Wire Line
	3100 2250 3850 2250
Text Label 3850 2050 2    60   ~ 0
Prog_MOSI
Text Label 3850 2250 2    60   ~ 0
Prog_SCK
Text Label 3150 2750 0    60   ~ 0
RESET
$Comp
L Connector_Generic:Conn_01x06 J2
U 1 1 5B4D26DA
P 5600 7100
F 0 "J2" H 5600 7400 50  0000 C CNN
F 1 "Programmer" H 5600 6700 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 5600 7100 50  0001 C CNN
F 3 "" H 5600 7100 50  0001 C CNN
	1    5600 7100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5800 6900 6600 6900
Wire Wire Line
	5800 7000 6600 7000
Wire Wire Line
	5800 7100 6600 7100
Wire Wire Line
	5800 7200 6600 7200
Wire Wire Line
	5800 7300 6600 7300
Wire Wire Line
	5800 7400 6600 7400
Text Label 5950 6900 0    60   ~ 0
VCC
Text Label 5950 7400 0    60   ~ 0
GND
Text Label 5950 7300 0    60   ~ 0
RESET
Text Label 5950 7000 0    60   ~ 0
Prog_MOSI
Text Label 5950 7100 0    60   ~ 0
Prog_MISO
Text Label 5950 7200 0    60   ~ 0
Prog_SCK
$Comp
L Connector_Generic:Conn_01x02 J3
U 1 1 5B4D32C7
P 5600 800
F 0 "J3" H 5600 900 50  0000 C CNN
F 1 "Vib" H 5600 600 50  0000 C CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x02_P4.6mm_D0.9mm_OD2.1mm" H 5600 800 50  0001 C CNN
F 3 "" H 5600 800 50  0001 C CNN
	1    5600 800 
	1    0    0    -1  
$EndComp
$Comp
L Device:R Rvib1
U 1 1 5B4D37C1
P 4700 800
F 0 "Rvib1" V 4780 800 50  0000 C CNN
F 1 "R" V 4700 800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4630 800 50  0001 C CNN
F 3 "" H 4700 800 50  0001 C CNN
	1    4700 800 
	0    1    1    0   
$EndComp
Wire Wire Line
	3850 800  4550 800 
Wire Wire Line
	3100 1250 4000 1250
Wire Wire Line
	4000 1450 3100 1450
Text Label 3350 1250 0    60   ~ 0
VIN_monitor
$Comp
L power:GND #PWR011
U 1 1 5B4DBE23
P 1300 3300
F 0 "#PWR011" H 1300 3050 50  0001 C CNN
F 1 "GND" H 1300 3150 50  0000 C CNN
F 2 "" H 1300 3300 50  0001 C CNN
F 3 "" H 1300 3300 50  0001 C CNN
	1    1300 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 6950 1250 7350
Wire Wire Line
	1600 4800 1600 5050
Wire Wire Line
	8400 5550 8400 5900
Wire Wire Line
	8700 5550 8700 5700
Wire Wire Line
	9400 5550 9600 5550
Wire Wire Line
	8700 6200 8700 6250
Wire Wire Line
	9400 6200 10150 6200
Wire Wire Line
	2400 850  2400 800 
Wire Wire Line
	2400 800  1400 800 
Wire Wire Line
	1400 2150 1400 3250
Wire Wire Line
	1400 3250 1900 3250
Wire Wire Line
	2400 3250 2400 3050
Wire Wire Line
	2500 850  2500 750 
Wire Wire Line
	2500 750  1050 750 
Wire Wire Line
	1050 2150 1050 3300
Wire Wire Line
	1050 3300 1300 3300
Wire Wire Line
	2500 3300 2500 3050
Connection ~ 1300 3300
Wire Wire Line
	1300 3300 2500 3300
$Comp
L power:GND #PWR0101
U 1 1 5B682381
P 1900 3400
F 0 "#PWR0101" H 1900 3150 50  0001 C CNN
F 1 "GND" H 1900 3250 50  0000 C CNN
F 2 "" H 1900 3400 50  0001 C CNN
F 3 "" H 1900 3400 50  0001 C CNN
	1    1900 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 3250 1900 3400
Connection ~ 1900 3250
Wire Wire Line
	1900 3250 2400 3250
Text Label 1150 750  0    50   ~ 0
VCC
Text Label 1500 800  0    50   ~ 0
VCC
Text Notes 4550 1600 0    50   ~ 0
blue - 2.9V\n
Text Notes 4250 1850 0    50   ~ 0
red
Text Notes 4000 1950 0    50   ~ 0
yellow
$Comp
L power:+5V #PWR0102
U 1 1 5B68B95A
P 2100 6300
F 0 "#PWR0102" H 2100 6150 50  0001 C CNN
F 1 "+5V" H 2115 6473 50  0000 C CNN
F 2 "" H 2100 6300 50  0001 C CNN
F 3 "" H 2100 6300 50  0001 C CNN
	1    2100 6300
	1    0    0    -1  
$EndComp
Connection ~ 2100 6300
Text Label 4850 6300 0    50   ~ 0
VCC
Wire Wire Line
	5050 6450 5050 6300
Connection ~ 5050 6300
Wire Wire Line
	5050 6300 5150 6300
Wire Wire Line
	5050 6750 5050 7350
Connection ~ 1250 7350
Wire Wire Line
	1250 7350 1250 7450
$Comp
L power:GND #PWR010
U 1 1 5B4D358D
P 5100 900
F 0 "#PWR010" H 5100 650 50  0001 C CNN
F 1 "GND" H 5100 750 50  0000 C CNN
F 2 "" H 5100 900 50  0001 C CNN
F 3 "" H 5100 900 50  0001 C CNN
	1    5100 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 6500 1950 6500
Wire Wire Line
	1550 6600 1950 6600
$Comp
L Device:R R7
U 1 1 5B6B3923
P 2050 7200
F 0 "R7" H 2120 7246 50  0000 L CNN
F 1 "100k" H 2120 7155 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1980 7200 50  0001 C CNN
F 3 "~" H 2050 7200 50  0001 C CNN
F 4 "" H 2050 7200 50  0001 C CNN "DigikeyPN"
F 5 "" H 2050 7200 50  0001 C CNN "DigikeyPL"
	1    2050 7200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5B6B39EF
P 2050 6750
F 0 "R8" H 2120 6796 50  0000 L CNN
F 1 "100k" H 2120 6705 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1980 6750 50  0001 C CNN
F 3 "~" H 2050 6750 50  0001 C CNN
F 4 "" H 2050 6750 50  0001 C CNN "DigikeyPN"
F 5 "" H 2050 6750 50  0001 C CNN "DigikeyPL"
	1    2050 6750
	1    0    0    -1  
$EndComp
Text Label 2500 6950 2    50   ~ 0
VIN_monitor
$Comp
L power:VCC #PWR0103
U 1 1 5B6E8A1F
P 4700 6300
F 0 "#PWR0103" H 4700 6150 50  0001 C CNN
F 1 "VCC" H 4717 6473 50  0000 C CNN
F 2 "" H 4700 6300 50  0001 C CNN
F 3 "" H 4700 6300 50  0001 C CNN
	1    4700 6300
	1    0    0    -1  
$EndComp
Connection ~ 4700 6300
Wire Wire Line
	4700 6300 5050 6300
Text Notes 4850 6150 0    50   ~ 0
around 4.5V
Text Notes 7000 2600 0    20   ~ 0
Desired peak current 1A\nForward voltage: 2.5V-3V
Text Notes 6900 2700 0    20   ~ 0
Vce @1A around 0.5V
Text Notes 5650 6650 0    50   ~ 0
Only use 3.3V for flashing
Text Notes 9150 5250 0    24   ~ 0
We get around 0.075W average AC power and a bandpass 50Hz-20kHz
Wire Wire Line
	4850 800  5400 800 
Wire Wire Line
	5100 900  5400 900 
Wire Wire Line
	1400 800  1400 1850
Wire Wire Line
	1050 750  1050 1850
$Comp
L Device:Q_NPN_BEC Q2
U 1 1 6044A354
P 6300 2600
F 0 "Q2" H 6491 2691 50  0000 L CNN
F 1 "DNBT8105" H 6491 2600 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6491 2509 50  0000 L CNN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=11255&prodName=SSM6L35FE" H 6300 2600 50  0001 C CNN
F 4 "DNBT8105DICT-ND" H 6300 2600 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/diodes-incorporated/DNBT8105-7/DNBT8105DICT-ND/989959" H 6300 2600 50  0001 C CNN "DigikeyPL"
	1    6300 2600
	1    0    0    -1  
$EndComp
$Comp
L LasertagComponents:SSM6L35 Q1
U 1 1 60457D41
P 8600 5150
F 0 "Q1" H 8790 5196 50  0000 L CNN
F 1 "SSM6L35" H 8790 5105 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-666" H 8800 5250 50  0001 C CNN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=11255&prodName=SSM6L35FE" H 8600 5150 50  0001 C CNN
	1    8600 5150
	1    0    0    -1  
$EndComp
$Comp
L LasertagComponents:SSM6L35 Q1
U 2 1 60458B7F
P 8600 5900
F 0 "Q1" H 8790 5946 50  0000 L CNN
F 1 "SSM6L35" H 8790 5855 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-666" H 8800 6000 50  0001 C CNN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=11255&prodName=SSM6L35FE" H 8600 5900 50  0001 C CNN
F 4 "SSM6L35FELMCT-ND" H 8600 5900 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/toshiba-semiconductor-and-storage/SSM6L35FE-LM/SSM6L35FELMCT-ND/2257811" H 8600 5900 50  0001 C CNN "DigikeyPL"
	2    8600 5900
	1    0    0    -1  
$EndComp
Text Label 4200 800  0    50   ~ 0
VIB
Text Label 3600 1450 0    50   ~ 0
VIB
$Comp
L Connector_Generic:Conn_01x03 J9
U 1 1 604F598A
P 4150 3900
F 0 "J9" H 4230 3942 50  0000 L CNN
F 1 "Prog_Switch" H 4230 3851 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 4150 3900 50  0001 C CNN
F 3 "~" H 4150 3900 50  0001 C CNN
	1    4150 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 3900 3600 3900
Wire Wire Line
	3950 3800 3600 3800
Wire Wire Line
	3950 4000 3600 4000
Text Label 3600 4000 0    50   ~ 0
IR2
Text Label 3600 3900 0    50   ~ 0
PB1
Text Label 3600 3800 0    50   ~ 0
Prog_MISO
Text Label 3850 2150 2    50   ~ 0
PB1
$Comp
L Device:Q_NPN_BEC Q4
U 1 1 60518701
P 6300 4700
F 0 "Q4" H 6491 4791 50  0000 L CNN
F 1 "DNBT8105" H 6491 4700 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6491 4609 50  0000 L CNN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=11255&prodName=SSM6L35FE" H 6300 4700 50  0001 C CNN
F 4 "DNBT8105DICT-ND" H 6300 4700 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/diodes-incorporated/DNBT8105-7/DNBT8105DICT-ND/989959" H 6300 4700 50  0001 C CNN "DigikeyLink"
	1    6300 4700
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NPN_BEC Q3
U 1 1 6053E2FA
P 6300 3650
F 0 "Q3" H 6491 3741 50  0000 L CNN
F 1 "DNBT8105" H 6491 3650 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6491 3559 50  0000 L CNN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=11255&prodName=SSM6L35FE" H 6300 3650 50  0001 C CNN
F 4 "DNBT8105DICT-ND" H 6300 3650 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/diodes-incorporated/DNBT8105-7/DNBT8105DICT-ND/989959" H 6300 3650 50  0001 C CNN "DigikeyLink"
	1    6300 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 4050 6400 3850
Wire Wire Line
	6100 3650 5850 3650
Text Label 5850 3650 0    50   ~ 0
IR2
Text Label 5450 2550 0    50   ~ 0
IR1
Wire Wire Line
	4950 2450 4950 4700
Wire Wire Line
	1750 4200 900  4200
Text Notes 10350 6200 0    50   ~ 0
8Ohm Speaker\n
$Comp
L Connector_Generic:Conn_01x04 J10
U 1 1 60623522
P 8700 3600
F 0 "J10" H 8780 3592 50  0000 L CNN
F 1 "IRFrontend" H 8780 3501 50  0000 L CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x04_P4.6mm_D0.9mm_OD2.1mm" H 8700 3600 50  0001 C CNN
F 3 "~" H 8700 3600 50  0001 C CNN
	1    8700 3600
	1    0    0    -1  
$EndComp
Text Label 8150 3700 0    50   ~ 0
IR2_SINK
Text Label 8150 3600 0    50   ~ 0
IR1_SINK
Text Label 8150 3800 0    50   ~ 0
FLASH_SINK
Wire Wire Line
	7400 3450 7400 3700
Wire Wire Line
	6400 3450 7400 3450
Wire Wire Line
	7400 3700 8500 3700
Wire Wire Line
	7900 4400 7900 3800
Wire Wire Line
	6400 4400 7900 4400
Wire Wire Line
	7900 3800 8500 3800
Wire Wire Line
	7750 2350 7750 3600
Wire Wire Line
	6400 2350 7750 2350
Wire Wire Line
	7750 3600 8500 3600
Wire Wire Line
	8850 3100 8950 3100
Wire Wire Line
	8000 3000 8000 3500
Wire Wire Line
	8000 3500 8500 3500
Wire Wire Line
	6400 2800 6400 2950
$Comp
L Connector_Generic:Conn_01x03 J4
U 1 1 60663DD4
P 2300 4700
F 0 "J4" H 2380 4742 50  0000 L CNN
F 1 "Reload" H 2380 4651 50  0000 L CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x03_P4.6mm_D0.9mm_OD2.1mm" H 2300 4700 50  0001 C CNN
F 3 "~" H 2300 4700 50  0001 C CNN
	1    2300 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	900  4700 2100 4700
Wire Wire Line
	1600 4800 2100 4800
Wire Wire Line
	2100 4600 1750 4600
Text Label 1750 4600 0    50   ~ 0
VCC
$Comp
L LasertagComponents:NCP380LSN05 U2
U 1 1 606B329B
P 3350 6600
F 0 "U2" H 3350 7165 50  0000 C CNN
F 1 "NCP380LSN05" H 3350 7074 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TSOT-23-5" H 3350 6600 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/NCP380-D.PDF" H 3350 6600 50  0001 C CNN
	1    3350 6600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 7100 3350 7350
Connection ~ 3350 7350
Wire Wire Line
	3350 7350 5050 7350
Wire Wire Line
	2950 6850 2900 6850
Wire Wire Line
	2900 6850 2900 7350
Connection ~ 2900 7350
Wire Wire Line
	2900 7350 3350 7350
NoConn ~ 2950 6700
Wire Wire Line
	3750 6300 4700 6300
$Comp
L Device:C_Small C5
U 1 1 60715558
P 2700 6800
F 0 "C5" H 2792 6846 50  0000 L CNN
F 1 "47u X5R" H 2792 6755 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 2700 6800 50  0001 C CNN
F 3 "~" H 2700 6800 50  0001 C CNN
F 4 "1276-3063-1-ND" H 2700 6800 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/samsung-electro-mechanics/CL31A476MPHNNNE/1276-3063-1-ND/3891149" H 2700 6800 50  0001 C CNN "DigikeyPL"
	1    2700 6800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 6900 2700 7350
Connection ~ 2700 7350
Wire Wire Line
	2700 7350 2900 7350
Wire Wire Line
	2700 6700 2700 6300
Connection ~ 2700 6300
Wire Wire Line
	2700 6300 2950 6300
$Comp
L Device:CP C6
U 1 1 607618D4
P 9750 5550
F 0 "C6" H 9775 5650 50  0000 L CNN
F 1 "220u" H 9775 5450 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 9788 5400 50  0001 C CNN
F 3 "" H 9750 5550 50  0001 C CNN
F 4 "493-2264-1-ND" H 9750 5550 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/nichicon/UUD1C221MCL1GS/493-2264-1-ND/590239" H 9750 5550 50  0001 C CNN "DigikeyPL"
	1    9750 5550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1250 7350 2050 7350
Wire Wire Line
	2100 6300 2700 6300
Connection ~ 2050 7350
Wire Wire Line
	2050 7350 2700 7350
Wire Wire Line
	2050 6600 2050 6300
Connection ~ 2050 6300
Wire Wire Line
	2050 6300 2100 6300
Wire Wire Line
	2050 6900 2050 6950
Wire Wire Line
	2500 6950 2050 6950
Connection ~ 2050 6950
Wire Wire Line
	2050 6950 2050 7050
Wire Wire Line
	8850 3300 8550 3300
Wire Wire Line
	8550 3300 8550 3200
Wire Wire Line
	8850 3100 8850 3300
$Comp
L Device:Net-Tie_2 NT1
U 1 1 6080047D
P 8750 2700
F 0 "NT1" V 8704 2744 50  0000 L CNN
F 1 "Net-Tie_2" V 8795 2744 50  0000 L CNN
F 2 "NetTie:NetTie-2_SMD_Pad0.5mm" H 8750 2700 50  0001 C CNN
F 3 "~" H 8750 2700 50  0001 C CNN
	1    8750 2700
	0    1    1    0   
$EndComp
Wire Wire Line
	8750 3000 8750 2800
Connection ~ 8750 3000
Wire Wire Line
	8750 3000 8950 3000
Wire Wire Line
	8400 2550 8400 2450
Wire Wire Line
	8750 2550 8750 2600
Wire Wire Line
	8400 2550 8750 2550
Text Label 8400 2450 0    50   ~ 0
VCC
Text Label 8100 3000 0    50   ~ 0
VCC_IR
Text Label 6400 2950 0    50   ~ 0
GND_IR
Text Label 6400 4050 0    50   ~ 0
GND_IR
Text Label 6400 5000 0    50   ~ 0
GND_IR
Text Label 8650 3300 0    50   ~ 0
GND_IR
$Comp
L Device:Net-Tie_2 NT2
U 1 1 60838D57
P 9200 3450
F 0 "NT2" V 9154 3494 50  0000 L CNN
F 1 "Net-Tie_2" V 9245 3494 50  0000 L CNN
F 2 "NetTie:NetTie-2_SMD_Pad0.5mm" H 9200 3450 50  0001 C CNN
F 3 "~" H 9200 3450 50  0001 C CNN
	1    9200 3450
	0    1    1    0   
$EndComp
Wire Wire Line
	9200 3350 8850 3350
Wire Wire Line
	8850 3350 8850 3300
Connection ~ 8850 3300
Wire Wire Line
	9200 3600 9200 3550
$Comp
L power:GND #PWR0104
U 1 1 6084415E
P 9200 3600
F 0 "#PWR0104" H 9200 3350 50  0001 C CNN
F 1 "GND" H 9200 3450 50  0000 C CNN
F 2 "" H 9200 3600 50  0001 C CNN
F 3 "" H 9200 3600 50  0001 C CNN
	1    9200 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C4
U 1 1 606BCA4F
P 5050 6600
F 0 "C4" H 5075 6700 50  0000 L CNN
F 1 "220u" H 5075 6500 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 5088 6450 50  0001 C CNN
F 3 "" H 5050 6600 50  0001 C CNN
F 4 "493-2264-1-ND" H 5050 6600 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/nichicon/UUD1C221MCL1GS/493-2264-1-ND/590239" H 5050 6600 50  0001 C CNN "DigikeyPL"
	1    5050 6600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 3000 8550 3000
$Comp
L Device:C_Small C7
U 1 1 606CB401
P 8550 3100
F 0 "C7" H 8642 3146 50  0000 L CNN
F 1 "47u X5R" H 8642 3055 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 8550 3100 50  0001 C CNN
F 3 "~" H 8550 3100 50  0001 C CNN
F 4 "1276-3063-1-ND" H 8550 3100 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/samsung-electro-mechanics/CL31A476MPHNNNE/1276-3063-1-ND/3891149" H 8550 3100 50  0001 C CNN "DigikeyPL"
	1    8550 3100
	1    0    0    -1  
$EndComp
Connection ~ 8550 3000
Wire Wire Line
	8550 3000 8750 3000
$Comp
L Device:C C2
U 1 1 606CEB0B
P 1050 2000
F 0 "C2" H 1075 2100 50  0000 L CNN
F 1 "100n" H 1075 1900 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1088 1850 50  0001 C CNN
F 3 "" H 1050 2000 50  0001 C CNN
F 4 "1276-2449-1-ND" H 1050 2000 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/samsung-electro-mechanics/CL21B104MACNNNC/1276-2449-1-ND/3890535" H 1050 2000 50  0001 C CNN "DigikeyPL"
	1    1050 2000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J5
U 1 1 606D7C39
P 6900 1750
F 0 "J5" H 6980 1742 50  0000 L CNN
F 1 "LEDCon" H 6980 1651 50  0000 L CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x04_P4.6mm_D0.9mm_OD2.1mm" H 6900 1750 50  0001 C CNN
F 3 "~" H 6900 1750 50  0001 C CNN
	1    6900 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 2000 6500 1950
Wire Wire Line
	6500 1950 6700 1950
Wire Wire Line
	3850 1850 6700 1850
Wire Wire Line
	4200 1750 6700 1750
Wire Wire Line
	4500 1650 6700 1650
$EndSCHEMATC
