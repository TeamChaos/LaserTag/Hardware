EESchema Schematic File Version 4
LIBS:MainPCB-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MainPCB-rescue:ATtiny861A-PU-MCU_Microchip_ATtiny U?
U 1 1 5B4D1017
P 5700 3800
AR Path="/5B4D1017" Ref="U?"  Part="1" 
AR Path="/5B4D0E2A/5B4D1017" Ref="U_DECT1"  Part="1" 
F 0 "U_DECT1" H 4800 4750 50  0000 C CNN
F 1 "ATTINY861-20PU" H 5600 3450 50  0000 C CNN
F 2 "Package_DIP:DIP-20_W7.62mm_Socket_LongPads" H 5700 3800 50  0001 C CIN
F 3 "" H 5700 3800 50  0001 C CNN
	1    5700 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5B4D10F2
P 4750 3700
F 0 "C5" H 4775 3800 50  0000 L CNN
F 1 "100n" H 4775 3600 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.8mm_W2.6mm_P2.50mm" H 4788 3550 50  0001 C CNN
F 3 "" H 4750 3700 50  0001 C CNN
F 4 "BC5137-ND" H 4750 3700 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/vishay-bc-components/K104K10X7RF53L2/BC5137-ND/2820500" H 4750 3700 50  0001 C CNN "DigikeyPL"
	1    4750 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R10
U 1 1 5B4D121A
P 6650 4750
F 0 "R10" V 6730 4750 50  0000 C CNN
F 1 "10k" V 6650 4750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6580 4750 50  0001 C CNN
F 3 "" H 6650 4750 50  0001 C CNN
F 4 "CF14JT10K0CT-ND" H 6650 4750 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/stackpole-electronics-inc/CF14JT10K0/CF14JT10K0CT-ND/1830374" H 6650 4750 50  0001 C CNN "DigikeyPL"
	1    6650 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 4600 6650 4600
Wire Wire Line
	6650 4900 6650 5200
Text Label 6650 5100 1    60   ~ 0
VCC
Wire Wire Line
	6800 4250 6800 4200
Wire Wire Line
	6800 4200 6400 4200
Wire Wire Line
	8400 4350 6750 4350
Wire Wire Line
	6750 4350 6750 4300
Wire Wire Line
	6750 4300 6400 4300
Wire Wire Line
	8400 4450 6700 4450
Wire Wire Line
	6700 4450 6700 4400
Wire Wire Line
	6700 4400 6400 4400
Wire Wire Line
	8400 4550 6650 4550
Wire Wire Line
	6650 4550 6650 4500
Wire Wire Line
	6650 4500 6400 4500
Text Notes 6350 5650 0    60   ~ 0
Either use switch to disconnect detector from power or use switch to disconnect detector at MOSI pin
Wire Wire Line
	5700 2700 4750 2700
Wire Wire Line
	4750 2700 4750 3550
Wire Wire Line
	4750 3850 4750 5000
Wire Wire Line
	4750 5000 5700 5000
Wire Wire Line
	5700 5000 5700 4900
Wire Wire Line
	4450 3850 4450 5100
Wire Wire Line
	4450 5100 5800 5100
Wire Wire Line
	5800 5100 5800 4900
Wire Wire Line
	5800 2600 5800 2700
Text Label 5000 2700 0    50   ~ 0
VCC
Text Label 4950 5000 0    50   ~ 0
GND
Text Label 4700 5100 0    50   ~ 0
GND
$Comp
L Device:C C4
U 1 1 5B714B59
P 4450 3700
F 0 "C4" H 4475 3800 50  0000 L CNN
F 1 "100n" H 4475 3600 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.8mm_W2.6mm_P2.50mm" H 4488 3550 50  0001 C CNN
F 3 "" H 4450 3700 50  0001 C CNN
F 4 "BC5137-ND" H 4450 3700 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/vishay-bc-components/K104K10X7RF53L2/BC5137-ND/2820500" H 4450 3700 50  0001 C CNN "DigikeyPL"
	1    4450 3700
	1    0    0    -1  
$EndComp
Text Notes 8300 3950 0    20   ~ 0
Needs to be disconnected for programming.\nIs connected to AVR_MOSI via tristate line driver
Text GLabel 6650 3000 2    50   Input ~ 0
I2C_SDA
Text GLabel 6650 3200 2    50   Input ~ 0
I2C_SCL
Wire Wire Line
	4450 2600 5000 2600
Wire Wire Line
	4450 3550 4450 2600
$Comp
L power:+3.3V #PWR0114
U 1 1 5B79B5C8
P 5000 2600
F 0 "#PWR0114" H 5000 2450 50  0001 C CNN
F 1 "+3.3V" H 5015 2773 50  0000 C CNN
F 2 "" H 5000 2600 50  0001 C CNN
F 3 "" H 5000 2600 50  0001 C CNN
	1    5000 2600
	1    0    0    -1  
$EndComp
Connection ~ 5000 2600
Wire Wire Line
	5000 2600 5800 2600
Wire Wire Line
	6400 3000 6650 3000
Wire Wire Line
	6650 3200 6400 3200
Text HLabel 6650 3100 2    50   Input ~ 0
Detector_Available
Wire Wire Line
	6650 3100 6400 3100
Text HLabel 8400 4250 2    50   Input ~ 0
DETECTOR_HEAD1
Text HLabel 8400 4350 2    50   Input ~ 0
DETECTOR_HEAD2
Text HLabel 8400 4450 2    50   Input ~ 0
DETECTOR_HEAD3
Text HLabel 8400 4550 2    50   Input ~ 0
DETECTOR_HEAD4
Text HLabel 8650 3800 0    50   Output ~ 0
DETC_TO_AVRMOSI
$Comp
L Connector_Generic:Conn_01x06 JAVRProg1
U 1 1 5DF376AA
P 1650 3450
AR Path="/5B4D0E2A/5DF376AA" Ref="JAVRProg1"  Part="1" 
AR Path="/5DF376AA" Ref="J?"  Part="1" 
F 0 "JAVRProg1" H 1650 3750 50  0000 C CNN
F 1 "AVR_Programmer" H 1650 3050 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 1650 3450 50  0001 C CNN
F 3 "" H 1650 3450 50  0001 C CNN
F 4 "RND 205-00627" H 1650 3450 50  0001 C CNN "ReicheltPN"
	1    1650 3450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1850 3250 2450 3250
Wire Wire Line
	1850 3750 2450 3750
Text Label 2450 3750 2    60   ~ 0
GND
NoConn ~ 2450 3250
Wire Wire Line
	1850 3650 2500 3650
Text Label 2500 3650 2    50   ~ 0
AVR_RESET
Wire Wire Line
	1850 3450 2450 3450
Wire Wire Line
	1850 3550 2450 3550
Text Label 2450 3350 2    50   ~ 0
AVR_MOSI
Text Label 2450 3450 2    50   ~ 0
AVR_MISO
Text Label 2450 3550 2    50   ~ 0
AVR_SCLK
Text Label 6800 3900 2    50   ~ 0
AVR_MOSI
Text Label 6800 4000 2    50   ~ 0
AVR_MISO
Text Label 6800 4100 2    50   ~ 0
AVR_SCLK
Wire Wire Line
	8400 4250 6800 4250
Wire Wire Line
	6400 3900 6800 3900
Wire Wire Line
	6400 4000 6800 4000
Wire Wire Line
	6400 4100 6800 4100
Text HLabel 6800 3900 2    50   Input ~ 0
AVRMOSI_FROM_DETC
Text Label 4750 2600 2    50   ~ 0
VCC
Text GLabel 4450 2750 0    50   Input ~ 0
VCC
$Comp
L Connector_Generic:Conn_01x06 J_Board3
U 1 1 5E21166B
P 9000 3500
F 0 "J_Board3" H 9080 3492 50  0000 L CNN
F 1 "Conn_01x06" H 9080 3401 50  0000 L CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x06_P2.00mm_Vertical" H 9000 3500 50  0001 C CNN
F 3 "~" H 9000 3500 50  0001 C CNN
F 4 "1849-1001-ND" H 9000 3500 50  0001 C CNN "DigikeyPN"
	1    9000 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 3300 6400 3300
Wire Wire Line
	6400 3400 8800 3400
Wire Wire Line
	8800 3500 6400 3500
Wire Wire Line
	6400 3600 8800 3600
Wire Wire Line
	8800 3700 6400 3700
Wire Wire Line
	8650 3800 8800 3800
Wire Wire Line
	2450 3350 1850 3350
Wire Wire Line
	6650 4600 7200 4600
Connection ~ 6650 4600
Text Label 7200 4600 2    50   ~ 0
AVR_RESET
Text Label 8050 3300 0    50   ~ 0
Dect5
Text Label 8050 3400 0    50   ~ 0
Dect6
Text Label 8050 3500 0    50   ~ 0
Dect7
Text Label 8050 3600 0    50   ~ 0
Dect8
Text Label 8050 3700 0    50   ~ 0
Dect9
Text Label 8700 3800 0    50   ~ 0
Dect10
$EndSCHEMATC
