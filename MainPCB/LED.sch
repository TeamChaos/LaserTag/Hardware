EESchema Schematic File Version 4
LIBS:MainPCB-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R?
U 1 1 5DED8B7A
P 6450 3850
AR Path="/5B6AF6DC/5DED8B7A" Ref="R?"  Part="1" 
AR Path="/5DED8B7A" Ref="R?"  Part="1" 
AR Path="/5DEC99EE/5DED8B7A" Ref="R7"  Part="1" 
F 0 "R7" V 6243 3850 50  0000 C CNN
F 1 "330" V 6334 3850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6380 3850 50  0001 C CNN
F 3 "~" H 6450 3850 50  0001 C CNN
F 4 "CF14JT330RCT-ND" H 6450 3850 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/stackpole-electronics-inc/CF14JT330R/CF14JT330RCT-ND/1830338" H 6450 3850 50  0001 C CNN "DigikeyPL"
	1    6450 3850
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 5DED8B80
P 6450 3500
AR Path="/5B6AF6DC/5DED8B80" Ref="D?"  Part="1" 
AR Path="/5DED8B80" Ref="D?"  Part="1" 
AR Path="/5DEC99EE/5DED8B80" Ref="D2"  Part="1" 
F 0 "D2" H 6442 3245 50  0000 C CNN
F 1 "LED_Status1" H 6442 3336 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 6450 3500 50  0001 C CNN
F 3 "~" H 6450 3500 50  0001 C CNN
	1    6450 3500
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D?
U 1 1 5DED8B86
P 6800 3500
AR Path="/5B6AF6DC/5DED8B86" Ref="D?"  Part="1" 
AR Path="/5DED8B86" Ref="D?"  Part="1" 
AR Path="/5DEC99EE/5DED8B86" Ref="D3"  Part="1" 
F 0 "D3" H 6792 3245 50  0000 C CNN
F 1 "LED_Status2" H 6792 3336 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 6800 3500 50  0001 C CNN
F 3 "~" H 6800 3500 50  0001 C CNN
	1    6800 3500
	0    -1   -1   0   
$EndComp
$Comp
L LasertagComponents:PCA9634 U?
U 1 1 5DED8B8E
P 4550 4550
AR Path="/5B6AF6DC/5DED8B8E" Ref="U?"  Part="1" 
AR Path="/5DED8B8E" Ref="U?"  Part="1" 
AR Path="/5DEC99EE/5DED8B8E" Ref="U7"  Part="1" 
F 0 "U7" H 4550 5315 50  0000 C CNN
F 1 "PCA9634" H 4550 5224 50  0000 C CNN
F 2 "LasertagFootprints:SO20_NXP_SOT163-1_Hand" H 4550 4550 50  0001 C CNN
F 3 "https://www.nxp.com/docs/en/data-sheet/PCA9634.pdf" H 4550 4550 50  0001 C CNN
F 4 "568-4065-1-ND" H 4550 4550 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/nxp-usa-inc/PCA9634D118/568-4065-1-ND/1534152" H 4550 4550 50  0001 C CNN "DigikeyPL"
	1    4550 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:LED_RAGB D?
U 1 1 5DED8B94
P 5650 3450
AR Path="/5B6AF6DC/5DED8B94" Ref="D?"  Part="1" 
AR Path="/5DED8B94" Ref="D?"  Part="1" 
AR Path="/5DEC99EE/5DED8B94" Ref="D1"  Part="1" 
F 0 "D1" V 5696 3120 50  0000 R CNN
F 1 "LED_RAGB" V 5605 3120 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm-4_RGB_Staggered_Pins" H 5650 3400 50  0001 C CNN
F 3 "~" H 5650 3400 50  0001 C CNN
	1    5650 3450
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5DED8B9C
P 5450 3850
AR Path="/5B6AF6DC/5DED8B9C" Ref="R?"  Part="1" 
AR Path="/5DED8B9C" Ref="R?"  Part="1" 
AR Path="/5DEC99EE/5DED8B9C" Ref="R4"  Part="1" 
F 0 "R4" H 5520 3896 50  0000 L CNN
F 1 "120" H 5520 3805 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5380 3850 50  0001 C CNN
F 3 "~" H 5450 3850 50  0001 C CNN
F 4 "CF14JT120RCT-ND" H 5450 3850 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/stackpole-electronics-inc/CF14JT120R/CF14JT120RCT-ND/1830329" H 5450 3850 50  0001 C CNN "DigikeyPL"
	1    5450 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DED8BA4
P 5850 3850
AR Path="/5B6AF6DC/5DED8BA4" Ref="R?"  Part="1" 
AR Path="/5DED8BA4" Ref="R?"  Part="1" 
AR Path="/5DEC99EE/5DED8BA4" Ref="R6"  Part="1" 
F 0 "R6" H 5920 3896 50  0000 L CNN
F 1 "82" H 5920 3805 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5780 3850 50  0001 C CNN
F 3 "~" H 5850 3850 50  0001 C CNN
F 4 "82QBK-ND" H 5850 3850 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/yageo/CFR-25JB-52-82R/82QBK-ND/3609" H 5850 3850 50  0001 C CNN "DigikeyPL"
	1    5850 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 3700 5450 3650
Wire Wire Line
	5650 3700 5650 3650
Wire Wire Line
	5850 3700 5850 3650
Wire Wire Line
	5450 4000 5450 4050
Wire Wire Line
	5650 4000 5650 4150
Wire Wire Line
	5850 4000 5850 4250
Wire Wire Line
	6450 3650 6450 3700
Wire Wire Line
	6800 3700 6800 3650
Wire Wire Line
	5450 4050 5200 4050
Wire Wire Line
	5650 4150 5200 4150
Wire Wire Line
	5850 4250 5200 4250
Wire Wire Line
	6450 4000 6450 4350
Wire Wire Line
	6450 4350 5200 4350
Wire Wire Line
	6800 4000 6800 4450
Wire Wire Line
	6800 4450 5200 4450
Text Notes 6350 3450 0    50   ~ 0
Green\n
Text Notes 6750 3450 0    50   ~ 0
Red
Text GLabel 3900 4400 0    50   Input ~ 0
I2C_SDA
Text GLabel 3900 4500 0    50   Input ~ 0
I2C_SCL
$Comp
L power:GND #PWR?
U 1 1 5DED8BBD
P 3400 4150
AR Path="/5B6AF6DC/5DED8BBD" Ref="#PWR?"  Part="1" 
AR Path="/5DED8BBD" Ref="#PWR?"  Part="1" 
AR Path="/5DEC99EE/5DED8BBD" Ref="#PWR019"  Part="1" 
F 0 "#PWR019" H 3400 3900 50  0001 C CNN
F 1 "GND" H 3405 3977 50  0000 C CNN
F 2 "" H 3400 4150 50  0001 C CNN
F 3 "" H 3400 4150 50  0001 C CNN
	1    3400 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 4150 3400 4150
Wire Wire Line
	3900 4050 3400 4050
Text Label 3650 4050 0    50   ~ 0
VCC
$Comp
L power:+3.3V #PWR?
U 1 1 5DED8BC6
P 3400 4050
AR Path="/5B6AF6DC/5DED8BC6" Ref="#PWR?"  Part="1" 
AR Path="/5DED8BC6" Ref="#PWR?"  Part="1" 
AR Path="/5DEC99EE/5DED8BC6" Ref="#PWR018"  Part="1" 
F 0 "#PWR018" H 3400 3900 50  0001 C CNN
F 1 "+3.3V" H 3415 4223 50  0000 C CNN
F 2 "" H 3400 4050 50  0001 C CNN
F 3 "" H 3400 4050 50  0001 C CNN
	1    3400 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 5300 4250 5550
Wire Wire Line
	4350 5300 4350 5550
Wire Wire Line
	4450 5300 4450 5550
Wire Wire Line
	4550 5300 4550 5550
Wire Wire Line
	4650 5300 4650 5550
Wire Wire Line
	4750 5300 4750 5550
Wire Wire Line
	4850 5300 4850 5550
$Comp
L power:GND #PWR?
U 1 1 5DED8BD3
P 4250 5550
AR Path="/5B6AF6DC/5DED8BD3" Ref="#PWR?"  Part="1" 
AR Path="/5DED8BD3" Ref="#PWR?"  Part="1" 
AR Path="/5DEC99EE/5DED8BD3" Ref="#PWR021"  Part="1" 
F 0 "#PWR021" H 4250 5300 50  0001 C CNN
F 1 "GND" H 4255 5377 50  0000 C CNN
F 2 "" H 4250 5550 50  0001 C CNN
F 3 "" H 4250 5550 50  0001 C CNN
	1    4250 5550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DED8BD9
P 4350 5550
AR Path="/5B6AF6DC/5DED8BD9" Ref="#PWR?"  Part="1" 
AR Path="/5DED8BD9" Ref="#PWR?"  Part="1" 
AR Path="/5DEC99EE/5DED8BD9" Ref="#PWR022"  Part="1" 
F 0 "#PWR022" H 4350 5300 50  0001 C CNN
F 1 "GND" H 4355 5377 50  0000 C CNN
F 2 "" H 4350 5550 50  0001 C CNN
F 3 "" H 4350 5550 50  0001 C CNN
	1    4350 5550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DED8BDF
P 4550 5550
AR Path="/5B6AF6DC/5DED8BDF" Ref="#PWR?"  Part="1" 
AR Path="/5DED8BDF" Ref="#PWR?"  Part="1" 
AR Path="/5DEC99EE/5DED8BDF" Ref="#PWR023"  Part="1" 
F 0 "#PWR023" H 4550 5300 50  0001 C CNN
F 1 "GND" H 4555 5377 50  0000 C CNN
F 2 "" H 4550 5550 50  0001 C CNN
F 3 "" H 4550 5550 50  0001 C CNN
	1    4550 5550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DED8BE5
P 4750 5550
AR Path="/5B6AF6DC/5DED8BE5" Ref="#PWR?"  Part="1" 
AR Path="/5DED8BE5" Ref="#PWR?"  Part="1" 
AR Path="/5DEC99EE/5DED8BE5" Ref="#PWR024"  Part="1" 
F 0 "#PWR024" H 4750 5300 50  0001 C CNN
F 1 "GND" H 4755 5377 50  0000 C CNN
F 2 "" H 4750 5550 50  0001 C CNN
F 3 "" H 4750 5550 50  0001 C CNN
	1    4750 5550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DED8BEB
P 4850 5550
AR Path="/5B6AF6DC/5DED8BEB" Ref="#PWR?"  Part="1" 
AR Path="/5DED8BEB" Ref="#PWR?"  Part="1" 
AR Path="/5DEC99EE/5DED8BEB" Ref="#PWR025"  Part="1" 
F 0 "#PWR025" H 4850 5300 50  0001 C CNN
F 1 "GND" H 4855 5377 50  0000 C CNN
F 2 "" H 4850 5550 50  0001 C CNN
F 3 "" H 4850 5550 50  0001 C CNN
	1    4850 5550
	1    0    0    -1  
$EndComp
Text Label 4450 5450 1    50   ~ 0
VCC
Text Label 4650 5450 1    50   ~ 0
VCC
Wire Wire Line
	5650 3200 5650 3250
Wire Wire Line
	6450 3200 6450 3350
Wire Wire Line
	5650 3200 6450 3200
Wire Wire Line
	6450 3200 6800 3200
Wire Wire Line
	6800 3200 6800 3350
Connection ~ 6450 3200
Text Label 6200 3200 0    50   ~ 0
5V
$Comp
L power:GND #PWR?
U 1 1 5DED8BFA
P 3650 4900
AR Path="/5B6AF6DC/5DED8BFA" Ref="#PWR?"  Part="1" 
AR Path="/5DED8BFA" Ref="#PWR?"  Part="1" 
AR Path="/5DEC99EE/5DED8BFA" Ref="#PWR020"  Part="1" 
F 0 "#PWR020" H 3650 4650 50  0001 C CNN
F 1 "GND" H 3655 4727 50  0000 C CNN
F 2 "" H 3650 4900 50  0001 C CNN
F 3 "" H 3650 4900 50  0001 C CNN
	1    3650 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 4900 3900 4900
Text Notes 4500 5900 0    39   ~ 0
0x14
Text Notes 5400 4650 0    39   ~ 0
VOL around 0.4V @25mA\n
NoConn ~ 5200 4550
NoConn ~ 5200 4650
NoConn ~ 5200 4750
$Comp
L Device:R R?
U 1 1 5DED8C08
P 6800 3850
AR Path="/5B6AF6DC/5DED8C08" Ref="R?"  Part="1" 
AR Path="/5DED8C08" Ref="R?"  Part="1" 
AR Path="/5DEC99EE/5DED8C08" Ref="R8"  Part="1" 
F 0 "R8" V 6593 3850 50  0000 C CNN
F 1 "330" V 6684 3850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6730 3850 50  0001 C CNN
F 3 "~" H 6800 3850 50  0001 C CNN
F 4 "CF14JT330RCT-ND" H 6800 3850 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/stackpole-electronics-inc/CF14JT330R/CF14JT330RCT-ND/1830338" H 6800 3850 50  0001 C CNN "DigikeyPL"
	1    6800 3850
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 5DED8C10
P 5650 3850
AR Path="/5B6AF6DC/5DED8C10" Ref="R?"  Part="1" 
AR Path="/5DED8C10" Ref="R?"  Part="1" 
AR Path="/5DEC99EE/5DED8C10" Ref="R5"  Part="1" 
F 0 "R5" H 5720 3896 50  0000 L CNN
F 1 "82" H 5720 3805 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5580 3850 50  0001 C CNN
F 3 "~" H 5650 3850 50  0001 C CNN
F 4 "82QBK-ND" H 5650 3850 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/yageo/CFR-25JB-52-82R/82QBK-ND/3609" H 5650 3850 50  0001 C CNN "DigikeyPL"
	1    5650 3850
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5DED8C16
P 6450 3200
AR Path="/5B6AF6DC/5DED8C16" Ref="#PWR?"  Part="1" 
AR Path="/5DED8C16" Ref="#PWR?"  Part="1" 
AR Path="/5DEC99EE/5DED8C16" Ref="#PWR026"  Part="1" 
F 0 "#PWR026" H 6450 3050 50  0001 C CNN
F 1 "+5V" H 6465 3373 50  0000 C CNN
F 2 "" H 6450 3200 50  0001 C CNN
F 3 "" H 6450 3200 50  0001 C CNN
	1    6450 3200
	1    0    0    -1  
$EndComp
Text GLabel 3250 4050 0    50   Input ~ 0
VCC
Wire Wire Line
	3250 4050 3400 4050
Connection ~ 3400 4050
$EndSCHEMATC
