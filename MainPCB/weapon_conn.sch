EESchema Schematic File Version 4
LIBS:MainPCB-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R18
U 1 1 5DFC8C02
P 6550 3650
F 0 "R18" H 6620 3696 50  0000 L CNN
F 1 "10k" H 6620 3605 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 6480 3650 50  0001 C CNN
F 3 "~" H 6550 3650 50  0001 C CNN
F 4 "RMCF1206JT10K0CT-ND" H 6550 3650 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/stackpole-electronics-inc/RMCF1206JT10K0/RMCF1206JT10K0CT-ND/1942803" H 6550 3650 50  0001 C CNN "DigikeyPL"
	1    6550 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 3850 7400 3850
Wire Wire Line
	6550 3800 6550 3850
Wire Wire Line
	6000 3850 6200 3850
Connection ~ 6550 3850
Wire Wire Line
	4300 3550 4600 3550
$Comp
L Device:R R17
U 1 1 5DFC8C15
P 6150 3650
F 0 "R17" H 6220 3696 50  0000 L CNN
F 1 "10k" H 6220 3605 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 6080 3650 50  0001 C CNN
F 3 "~" H 6150 3650 50  0001 C CNN
F 4 "RMCF1206JT10K0CT-ND" H 6150 3650 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/stackpole-electronics-inc/RMCF1206JT10K0/RMCF1206JT10K0CT-ND/1942803" H 6150 3650 50  0001 C CNN "DigikeyPL"
	1    6150 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 3500 6500 3500
Wire Wire Line
	6150 3800 6150 3950
Wire Wire Line
	6150 3950 7400 3950
Wire Wire Line
	4800 3950 4950 3950
Connection ~ 6150 3950
Wire Wire Line
	4600 3650 4600 3550
Connection ~ 4600 3550
Wire Wire Line
	4600 3550 5000 3550
$Comp
L Device:R R15
U 1 1 5DFC8C26
P 5500 3700
F 0 "R15" H 5570 3746 50  0000 L CNN
F 1 "10k" H 5570 3655 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5430 3700 50  0001 C CNN
F 3 "~" H 5500 3700 50  0001 C CNN
F 4 "RMCF1206JT10K0CT-ND" H 5500 3700 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/stackpole-electronics-inc/CF14JT10K0/CF14JT10K0CT-ND/1830374" H 5500 3700 50  0001 C CNN "DigikeyPL"
	1    5500 3700
	1    0    0    -1  
$EndComp
Connection ~ 5500 3550
Wire Wire Line
	5500 3550 5800 3550
Connection ~ 5500 3850
Wire Wire Line
	5500 3850 5600 3850
$Comp
L Device:R R13
U 1 1 5DFC8C32
P 4300 3700
F 0 "R13" H 4370 3746 50  0000 L CNN
F 1 "10k" H 4370 3655 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4230 3700 50  0001 C CNN
F 3 "~" H 4300 3700 50  0001 C CNN
F 4 "RMCF1206JT10K0CT-ND" H 4300 3700 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/stackpole-electronics-inc/RMCF1206JT10K0/RMCF1206JT10K0CT-ND/1942803" H 4300 3700 50  0001 C CNN "DigikeyPL"
	1    4300 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 3850 4300 3950
Connection ~ 4300 3950
Wire Wire Line
	4300 3950 4400 3950
Text Notes 5400 3450 0    28   ~ 0
PullUps probably optional
$Comp
L Device:R R14
U 1 1 5DFC8C3C
P 4600 4500
F 0 "R14" V 4393 4500 50  0000 C CNN
F 1 "DNP" V 4484 4500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4530 4500 50  0001 C CNN
F 3 "~" H 4600 4500 50  0001 C CNN
	1    4600 4500
	0    1    1    0   
$EndComp
$Comp
L Device:R R16
U 1 1 5DFC8C42
P 5800 4450
F 0 "R16" V 5593 4450 50  0000 C CNN
F 1 "DNP" V 5684 4450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5730 4450 50  0001 C CNN
F 3 "~" H 5800 4450 50  0001 C CNN
	1    5800 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	4450 4500 4300 4500
Wire Wire Line
	4300 4500 4300 3950
Wire Wire Line
	4750 4500 4950 4500
Wire Wire Line
	4950 4500 4950 3950
Connection ~ 4950 3950
Wire Wire Line
	4950 3950 6150 3950
Wire Wire Line
	5400 4450 5400 3850
Wire Wire Line
	5400 4450 5650 4450
Wire Wire Line
	5400 3850 5500 3850
Wire Wire Line
	5950 4450 6200 4450
Wire Wire Line
	6200 4450 6200 3850
Connection ~ 6200 3850
Wire Wire Line
	6200 3850 6550 3850
$Comp
L Transistor_FET:BSS138 Q2
U 1 1 5DFC8C58
P 5800 3750
F 0 "Q2" V 6051 3750 50  0000 C CNN
F 1 "BSS138" V 6142 3750 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6000 3675 50  0001 L CIN
F 3 "https://www.diodes.com/assets/Datasheets/ds30144.pdf" H 5800 3750 50  0001 L CNN
F 4 "BSS138-FDICT-ND" V 5800 3750 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/diodes-incorporated/BSS138-7-F/BSS138-FDICT-ND/717843" V 5800 3750 50  0001 C CNN "DigikeyPL"
	1    5800 3750
	0    1    1    0   
$EndComp
$Comp
L Transistor_FET:BSS138 Q1
U 1 1 5DFC8C60
P 4600 3850
F 0 "Q1" V 4851 3850 50  0000 C CNN
F 1 "BSS138" V 4942 3850 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4800 3775 50  0001 L CIN
F 3 "https://www.diodes.com/assets/Datasheets/ds30144.pdf" H 4600 3850 50  0001 L CNN
F 4 "BSS138-FDICT-ND" V 4600 3850 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/diodes-incorporated/BSS138-7-F/BSS138-FDICT-ND/717843" V 4600 3850 50  0001 C CNN "DigikeyPL"
	1    4600 3850
	0    1    1    0   
$EndComp
Text HLabel 5400 3850 0    50   Input ~ 0
I2C_SDA
Text HLabel 4200 3950 0    50   Input ~ 0
I2C_SCL
Wire Wire Line
	4200 3950 4300 3950
Text GLabel 4600 3150 0    50   Input ~ 0
VCC
Text GLabel 6500 3150 0    50   Input ~ 0
5V
Wire Wire Line
	6500 3150 6500 3500
Connection ~ 6500 3500
Wire Wire Line
	6500 3500 6550 3500
Wire Wire Line
	4600 3150 5000 3150
Wire Wire Line
	5000 3150 5000 3550
Connection ~ 5000 3550
Wire Wire Line
	5000 3550 5500 3550
Text HLabel 7400 3850 2    50   Output ~ 0
I2C_SDA_HV
Text HLabel 7400 3950 2    50   Output ~ 0
I2C_SCL_HV
$EndSCHEMATC
