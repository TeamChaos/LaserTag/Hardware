EESchema Schematic File Version 4
LIBS:MainPCB-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MainPCB-rescue:ATtiny861A-PU-MCU_Microchip_ATtiny U3
U 1 1 5B6AF7C9
P 2950 2300
F 0 "U3" H 2400 3350 50  0000 R CNN
F 1 "ATtiny861A-PU" H 3000 2150 50  0000 R CNN
F 2 "Package_DIP:DIP-20_W7.62mm_Socket_LongPads" H 2950 2300 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc8197.pdf" H 2950 2300 50  0001 C CNN
	1    2950 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 2550 1800 3400
Wire Wire Line
	1800 3400 1850 3400
Wire Wire Line
	1800 1200 2950 1200
Wire Wire Line
	3050 1200 3050 1100
Wire Wire Line
	3050 1100 2200 1100
Wire Wire Line
	1450 1100 1450 2250
Wire Wire Line
	1450 2550 1450 3500
Wire Wire Line
	1450 3500 1550 3500
Wire Wire Line
	3050 3500 3050 3400
Wire Wire Line
	1800 1200 1800 2250
$Comp
L power:GND #PWR0108
U 1 1 5B6AFB1F
P 1550 3500
F 0 "#PWR0108" H 1550 3250 50  0001 C CNN
F 1 "GND" H 1555 3327 50  0000 C CNN
F 2 "" H 1550 3500 50  0001 C CNN
F 3 "" H 1550 3500 50  0001 C CNN
	1    1550 3500
	1    0    0    -1  
$EndComp
Connection ~ 1550 3500
Wire Wire Line
	1550 3500 3050 3500
$Comp
L power:GND #PWR0109
U 1 1 5B6AFB4C
P 1850 3400
F 0 "#PWR0109" H 1850 3150 50  0001 C CNN
F 1 "GND" H 1855 3227 50  0000 C CNN
F 2 "" H 1850 3400 50  0001 C CNN
F 3 "" H 1850 3400 50  0001 C CNN
	1    1850 3400
	1    0    0    -1  
$EndComp
Connection ~ 1850 3400
Wire Wire Line
	1850 3400 2950 3400
Text Label 1600 1100 0    50   ~ 0
VCC
Text Label 1900 1200 0    50   ~ 0
VCC
$Comp
L Device:R R19
U 1 1 5B6B1E11
P 10000 1500
F 0 "R19" V 9793 1500 50  0000 C CNN
F 1 "330" V 9884 1500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 9930 1500 50  0001 C CNN
F 3 "~" H 10000 1500 50  0001 C CNN
F 4 "CF14JT330RCT-ND" H 10000 1500 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/stackpole-electronics-inc/CF14JT330R/CF14JT330RCT-ND/1830338" H 10000 1500 50  0001 C CNN "DigikeyPL"
	1    10000 1500
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D3
U 1 1 5B6B1F0F
P 10000 1150
F 0 "D3" H 9992 895 50  0000 C CNN
F 1 "LED_Status1" H 9992 986 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 10000 1150 50  0001 C CNN
F 3 "~" H 10000 1150 50  0001 C CNN
	1    10000 1150
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D2
U 1 1 5B6B1FBD
P 10350 1150
F 0 "D2" H 10342 895 50  0000 C CNN
F 1 "LED_Status2" H 10342 986 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 10350 1150 50  0001 C CNN
F 3 "~" H 10350 1150 50  0001 C CNN
	1    10350 1150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3950 3100 3650 3100
Wire Wire Line
	3950 3400 3950 3700
Text Label 3950 3600 1    50   ~ 0
VCC
$Comp
L Device:C C?
U 1 1 5B7199BC
P 1800 2400
AR Path="/5B4D0E2A/5B7199BC" Ref="C?"  Part="1" 
AR Path="/5B6AF6DC/5B7199BC" Ref="C7"  Part="1" 
F 0 "C7" H 1825 2500 50  0000 L CNN
F 1 "100n" H 1825 2300 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.8mm_W2.6mm_P2.50mm" H 1838 2250 50  0001 C CNN
F 3 "" H 1800 2400 50  0001 C CNN
F 4 "BC5137-ND" H 1800 2400 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/vishay-bc-components/K104K10X7RF53L2/BC5137-ND/2820500" H 1800 2400 50  0001 C CNN "DigikeyPL"
	1    1800 2400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5B71A7E8
P 1450 2400
AR Path="/5B4D0E2A/5B71A7E8" Ref="C?"  Part="1" 
AR Path="/5B6AF6DC/5B71A7E8" Ref="C6"  Part="1" 
F 0 "C6" H 1475 2500 50  0000 L CNN
F 1 "100n" H 1475 2300 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.8mm_W2.6mm_P2.50mm" H 1488 2250 50  0001 C CNN
F 3 "" H 1450 2400 50  0001 C CNN
F 4 "BC5137-ND" H 1450 2400 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/vishay-bc-components/K104K10X7RF53L2/BC5137-ND/2820500" H 1450 2400 50  0001 C CNN "DigikeyPL"
	1    1450 2400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5B72825B
P 3950 3250
AR Path="/5B4D0E2A/5B72825B" Ref="R?"  Part="1" 
AR Path="/5B6AF6DC/5B72825B" Ref="R11"  Part="1" 
F 0 "R11" V 4030 3250 50  0000 C CNN
F 1 "10k" V 3950 3250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3880 3250 50  0001 C CNN
F 3 "" H 3950 3250 50  0001 C CNN
F 4 "CF14JT10K0CT-ND" H 3950 3250 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/stackpole-electronics-inc/CF14JT10K0/CF14JT10K0CT-ND/1830374" H 3950 3250 50  0001 C CNN "DigikeyPL"
	1    3950 3250
	1    0    0    -1  
$EndComp
Text GLabel 3650 2400 2    50   Input ~ 0
AVR_MOSI
Text GLabel 3650 2500 2    50   Input ~ 0
AVR_MISO
Text GLabel 3650 2600 2    50   Input ~ 0
AVR_SCK
Text GLabel 3650 1500 2    50   Input ~ 0
I2C_SDA
Text GLabel 3650 1700 2    50   Input ~ 0
I2C_SCL
$Comp
L power:+3.3V #PWR0115
U 1 1 5B7A7587
P 2200 1100
F 0 "#PWR0115" H 2200 950 50  0001 C CNN
F 1 "+3.3V" H 2215 1273 50  0000 C CNN
F 2 "" H 2200 1100 50  0001 C CNN
F 3 "" H 2200 1100 50  0001 C CNN
	1    2200 1100
	1    0    0    -1  
$EndComp
Connection ~ 2200 1100
Wire Wire Line
	2200 1100 1450 1100
Text HLabel 4100 3100 2    50   Input ~ 0
AVR_Reset
Wire Wire Line
	4100 3100 3950 3100
Connection ~ 3950 3100
$Comp
L LasertagComponents:ISD2360 U9
U 1 1 5BFDE94D
P 7700 5700
F 0 "U9" H 7675 6465 50  0000 C CNN
F 1 "ISD2360" H 7675 6374 50  0000 C CNN
F 2 "LasertagFootprints:SOP-16_7.5x10.4mm_P1.27mm_Hand" H 7700 5700 50  0001 C CNN
F 3 "http://www.nuvoton.com/resource-files/EN_ISD2360_Datasheet_Rev1-0.pdf" H 7700 5700 50  0001 C CNN
F 4 "ISD2360SYI-ND" H 7700 5700 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/nuvoton-technology-corporation-of-america/ISD2360SYI/ISD2360SYI-ND/3507392" H 7700 5700 50  0001 C CNN "DigikeyPL"
	1    7700 5700
	1    0    0    -1  
$EndComp
$Comp
L LasertagComponents:ULN2003LV U6
U 1 1 5BFDEB43
P 2200 6750
F 0 "U6" H 2200 7417 50  0000 C CNN
F 1 "ULN2003LV" H 2200 7326 50  0000 C CNN
F 2 "LasertagFootprints:SOIC-16_3.9x9.9mm_P1.27mm_Hand" H 2250 6200 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/uln2003lv.pdf" H 2300 6550 50  0001 C CNN
F 4 "296-30403-1-ND" H 2200 6750 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/texas-instruments/ULN2003LVDR/296-30403-1-ND/3283552" H 2200 6750 50  0001 C CNN "DigikeyPL"
	1    2200 6750
	1    0    0    -1  
$EndComp
$Comp
L LasertagComponents:PCA9634 U8
U 1 1 5BFF0C7F
P 8100 2200
F 0 "U8" H 8100 2965 50  0000 C CNN
F 1 "PCA9634" H 8100 2874 50  0000 C CNN
F 2 "LasertagFootprints:SO20_NXP_SOT163-1_Hand" H 8100 2200 50  0001 C CNN
F 3 "https://www.nxp.com/docs/en/data-sheet/PCA9634.pdf" H 8100 2200 50  0001 C CNN
F 4 "568-4065-1-ND" H 8100 2200 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/nxp-usa-inc/PCA9634D118/568-4065-1-ND/1534152" H 8100 2200 50  0001 C CNN "DigikeyPL"
	1    8100 2200
	1    0    0    -1  
$EndComp
$Comp
L Device:LED_RAGB D4
U 1 1 5BFF280E
P 9200 1100
F 0 "D4" V 9246 770 50  0000 R CNN
F 1 "LED_RAGB" V 9155 770 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm-4_RGB_Staggered_Pins" H 9200 1050 50  0001 C CNN
F 3 "~" H 9200 1050 50  0001 C CNN
	1    9200 1100
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R5
U 1 1 5BFF2D48
P 9000 1500
F 0 "R5" H 9070 1546 50  0000 L CNN
F 1 "120" H 9070 1455 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8930 1500 50  0001 C CNN
F 3 "~" H 9000 1500 50  0001 C CNN
F 4 "CF14JT120RCT-ND" H 9000 1500 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/stackpole-electronics-inc/CF14JT120R/CF14JT120RCT-ND/1830329" H 9000 1500 50  0001 C CNN "DigikeyPL"
	1    9000 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R12
U 1 1 5BFF2EDF
P 9400 1500
F 0 "R12" H 9470 1546 50  0000 L CNN
F 1 "82" H 9470 1455 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 9330 1500 50  0001 C CNN
F 3 "~" H 9400 1500 50  0001 C CNN
F 4 "82QBK-ND" H 9400 1500 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/yageo/CFR-25JB-52-82R/82QBK-ND/3609" H 9400 1500 50  0001 C CNN "DigikeyPL"
	1    9400 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9000 1350 9000 1300
Wire Wire Line
	9200 1350 9200 1300
Wire Wire Line
	9400 1350 9400 1300
Wire Wire Line
	9000 1650 9000 1700
Wire Wire Line
	9200 1650 9200 1800
Wire Wire Line
	9400 1650 9400 1900
Wire Wire Line
	10000 1300 10000 1350
Wire Wire Line
	10350 1350 10350 1300
Wire Wire Line
	9000 1700 8750 1700
Wire Wire Line
	9200 1800 8750 1800
Wire Wire Line
	9400 1900 8750 1900
Wire Wire Line
	10000 1650 10000 2000
Wire Wire Line
	10000 2000 8750 2000
Wire Wire Line
	10350 1650 10350 2100
Wire Wire Line
	10350 2100 8750 2100
Text Notes 9900 1100 0    50   ~ 0
Green\n
Text Notes 10300 1100 0    50   ~ 0
Red
Text GLabel 7450 2050 0    50   Input ~ 0
I2C_SDA
Text GLabel 7450 2150 0    50   Input ~ 0
I2C_SCL
$Comp
L power:GND #PWR0110
U 1 1 5C005A80
P 6950 1800
F 0 "#PWR0110" H 6950 1550 50  0001 C CNN
F 1 "GND" H 6955 1627 50  0000 C CNN
F 2 "" H 6950 1800 50  0001 C CNN
F 3 "" H 6950 1800 50  0001 C CNN
	1    6950 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 1800 6950 1800
Wire Wire Line
	7450 1700 6950 1700
Text Label 7200 1700 0    50   ~ 0
VCC
$Comp
L power:+3.3V #PWR0111
U 1 1 5C006EB3
P 6950 1700
F 0 "#PWR0111" H 6950 1550 50  0001 C CNN
F 1 "+3.3V" H 6965 1873 50  0000 C CNN
F 2 "" H 6950 1700 50  0001 C CNN
F 3 "" H 6950 1700 50  0001 C CNN
	1    6950 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 2950 7800 3200
Wire Wire Line
	7900 2950 7900 3200
Wire Wire Line
	8000 2950 8000 3200
Wire Wire Line
	8100 2950 8100 3200
Wire Wire Line
	8200 2950 8200 3200
Wire Wire Line
	8300 2950 8300 3200
Wire Wire Line
	8400 2950 8400 3200
$Comp
L power:GND #PWR0112
U 1 1 5C00C150
P 7800 3200
F 0 "#PWR0112" H 7800 2950 50  0001 C CNN
F 1 "GND" H 7805 3027 50  0000 C CNN
F 2 "" H 7800 3200 50  0001 C CNN
F 3 "" H 7800 3200 50  0001 C CNN
	1    7800 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 5C00C183
P 7900 3200
F 0 "#PWR0113" H 7900 2950 50  0001 C CNN
F 1 "GND" H 7905 3027 50  0000 C CNN
F 2 "" H 7900 3200 50  0001 C CNN
F 3 "" H 7900 3200 50  0001 C CNN
	1    7900 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0118
U 1 1 5C00C1B6
P 8100 3200
F 0 "#PWR0118" H 8100 2950 50  0001 C CNN
F 1 "GND" H 8105 3027 50  0000 C CNN
F 2 "" H 8100 3200 50  0001 C CNN
F 3 "" H 8100 3200 50  0001 C CNN
	1    8100 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0119
U 1 1 5C00C1E9
P 8300 3200
F 0 "#PWR0119" H 8300 2950 50  0001 C CNN
F 1 "GND" H 8305 3027 50  0000 C CNN
F 2 "" H 8300 3200 50  0001 C CNN
F 3 "" H 8300 3200 50  0001 C CNN
	1    8300 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0120
U 1 1 5C00C21C
P 8400 3200
F 0 "#PWR0120" H 8400 2950 50  0001 C CNN
F 1 "GND" H 8405 3027 50  0000 C CNN
F 2 "" H 8400 3200 50  0001 C CNN
F 3 "" H 8400 3200 50  0001 C CNN
	1    8400 3200
	1    0    0    -1  
$EndComp
Text Label 8000 3100 1    50   ~ 0
VCC
Text Label 8200 3100 1    50   ~ 0
VCC
Wire Wire Line
	9200 850  9200 900 
Wire Wire Line
	10000 850  10000 1000
Wire Wire Line
	9200 850  10000 850 
Wire Wire Line
	10000 850  10350 850 
Wire Wire Line
	10350 850  10350 1000
Connection ~ 10000 850 
Text Label 9750 850  0    50   ~ 0
5V
$Comp
L Device:CP_Small C2
U 1 1 5C00F978
P 6250 5350
F 0 "C2" H 6338 5396 50  0000 L CNN
F 1 "10u" H 6338 5305 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 6250 5350 50  0001 C CNN
F 3 "~" H 6250 5350 50  0001 C CNN
F 4 "732-8593-1-ND" H 6250 5350 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/wurth-electronics-inc/860010372001/732-8593-1-ND/5728550" H 6250 5350 50  0001 C CNN "DigikeyPL"
	1    6250 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 5700 6900 5700
Wire Wire Line
	6900 5700 6900 5650
Wire Wire Line
	6900 5650 6600 5650
Wire Wire Line
	7000 5800 7000 5850
Wire Wire Line
	7000 5850 6600 5850
Wire Wire Line
	7000 5500 7000 5400
Wire Wire Line
	7000 5500 6550 5500
Wire Wire Line
	6250 5500 6250 5450
Connection ~ 7000 5500
Wire Wire Line
	6550 5450 6550 5500
Connection ~ 6550 5500
Wire Wire Line
	6550 5500 6250 5500
Wire Wire Line
	7000 5300 7000 5200
Wire Wire Line
	7000 5200 6550 5200
Wire Wire Line
	6250 5200 6250 5250
Connection ~ 7000 5200
Wire Wire Line
	6550 5250 6550 5200
Connection ~ 6550 5200
Wire Wire Line
	6550 5200 6250 5200
$Comp
L power:GND #PWR0121
U 1 1 5C015880
P 6600 5850
F 0 "#PWR0121" H 6600 5600 50  0001 C CNN
F 1 "GND" H 6605 5677 50  0000 C CNN
F 2 "" H 6600 5850 50  0001 C CNN
F 3 "" H 6600 5850 50  0001 C CNN
	1    6600 5850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0122
U 1 1 5C0158D8
P 6250 5500
F 0 "#PWR0122" H 6250 5250 50  0001 C CNN
F 1 "GND" H 6255 5327 50  0000 C CNN
F 2 "" H 6250 5500 50  0001 C CNN
F 3 "" H 6250 5500 50  0001 C CNN
	1    6250 5500
	1    0    0    -1  
$EndComp
Connection ~ 6250 5500
Text Label 6300 5200 0    50   ~ 0
VCC
Text Label 6650 5650 0    50   ~ 0
VCC
Wire Wire Line
	8350 5200 8900 5200
Wire Wire Line
	8350 5300 8900 5300
Wire Wire Line
	8350 5400 8900 5400
Wire Wire Line
	8350 5500 8900 5500
Text Label 8450 5200 0    50   ~ 0
ISD_MOSI
Text Label 8450 5300 0    50   ~ 0
ISD_SCLK
Text Label 8450 5400 0    50   ~ 0
ISD_MISO
Text Label 8450 5500 0    50   ~ 0
ISD_SSB
Wire Wire Line
	3650 2700 4500 2700
Wire Wire Line
	3650 2800 4500 2800
Wire Wire Line
	3650 2900 4500 2900
Wire Wire Line
	3650 3000 4500 3000
Text Label 4100 3000 0    50   ~ 0
ISD_MOSI_MCU
Text Label 4100 2900 0    50   ~ 0
ISD_SSB_MCU
Text Label 4100 2800 0    50   ~ 0
ISD_SCLK_MCU
Text Label 4100 2700 0    50   ~ 0
ISD_MISO_MCU
$Comp
L Connector_Generic:Conn_01x02 J_Speak1
U 1 1 5C024676
P 8800 6100
F 0 "J_Speak1" H 8880 6092 50  0000 L CNN
F 1 "Conn_Speaker" H 8880 6001 50  0000 L CNN
F 2 "LasertagFootprints:SolderWirePad_2x_0-8mmDrill" H 8800 6100 50  0001 C CNN
F 3 "~" H 8800 6100 50  0001 C CNN
	1    8800 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 6100 8350 6100
Wire Wire Line
	8350 6200 8600 6200
$Comp
L Connector_Generic:Conn_01x06 J_ISDProg1
U 1 1 5C027731
P 10500 5500
F 0 "J_ISDProg1" H 10580 5542 50  0000 L CNN
F 1 "ISD_Programmer" H 10580 5451 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 10500 5500 50  0001 C CNN
F 3 "~" H 10500 5500 50  0001 C CNN
	1    10500 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	10300 5300 9800 5300
Wire Wire Line
	9800 5400 10300 5400
Wire Wire Line
	9800 5500 10300 5500
Wire Wire Line
	9800 5600 10300 5600
Wire Wire Line
	9800 5700 10300 5700
Text Label 9900 5600 0    50   ~ 0
ISD_MOSI
Text Label 9900 5300 0    50   ~ 0
ISD_MISO
Text Label 9900 5400 0    50   ~ 0
ISD_SCLK
Text Label 9900 5500 0    50   ~ 0
ISD_SSB
Text Label 9900 5700 0    50   ~ 0
ISD_BSYB
$Comp
L power:GND #PWR0123
U 1 1 5C031357
P 10300 5800
F 0 "#PWR0123" H 10300 5550 50  0001 C CNN
F 1 "GND" H 10305 5627 50  0000 C CNN
F 2 "" H 10300 5800 50  0001 C CNN
F 3 "" H 10300 5800 50  0001 C CNN
	1    10300 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 5700 8350 5700
Text Label 8450 5600 0    50   ~ 0
ISD_INTB
Text Label 8450 5700 0    50   ~ 0
ISD_BSYB
$Comp
L power:GND #PWR0124
U 1 1 5C0380B6
P 2200 7450
F 0 "#PWR0124" H 2200 7200 50  0001 C CNN
F 1 "GND" H 2205 7277 50  0000 C CNN
F 2 "" H 2200 7450 50  0001 C CNN
F 3 "" H 2200 7450 50  0001 C CNN
	1    2200 7450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 6550 1200 6550
Wire Wire Line
	1200 6650 1800 6650
Wire Wire Line
	1200 6750 1800 6750
Wire Wire Line
	1200 6850 1800 6850
Wire Wire Line
	1200 6950 1800 6950
Wire Wire Line
	1200 7050 1800 7050
Text Label 1300 6550 0    50   ~ 0
EN_MOTOR1
Text Label 1300 6650 0    50   ~ 0
EN_MOTOR2
Text Label 1300 6750 0    50   ~ 0
EN_MOTOR3
Text Label 1300 6850 0    50   ~ 0
EN_MOTOR4
Text Label 1300 6950 0    50   ~ 0
EN_MOTOR5
Text Label 1300 7050 0    50   ~ 0
EN_MOTOR6
Text HLabel 3150 6550 2    50   3State ~ 0
SINK_MOTOR1
Wire Wire Line
	3150 6550 2600 6550
Text HLabel 3150 6650 2    50   3State ~ 0
SINK_MOTOR2
Text HLabel 3150 6750 2    50   3State ~ 0
SINK_MOTOR3
Text HLabel 3150 6850 2    50   3State ~ 0
SINK_MOTOR4
Text HLabel 3150 6950 2    50   3State ~ 0
SINK_MOTOR5
Text HLabel 3150 7050 2    50   3State ~ 0
SINK_MOTOR6
Wire Wire Line
	3150 6650 2600 6650
Wire Wire Line
	2600 6750 3150 6750
Wire Wire Line
	3150 6850 2600 6850
Wire Wire Line
	2600 6950 3150 6950
Wire Wire Line
	2600 7050 3150 7050
Wire Wire Line
	3650 1600 4500 1600
Wire Wire Line
	3650 1800 4500 1800
Wire Wire Line
	3650 1900 4500 1900
Wire Wire Line
	4500 2000 3650 2000
Wire Wire Line
	4500 2100 3650 2100
Wire Wire Line
	4500 2200 3650 2200
Text Label 4500 1600 2    50   ~ 0
EN_MOTOR6
Text Label 4500 1800 2    50   ~ 0
EN_MOTOR5
Text Label 4500 1900 2    50   ~ 0
EN_MOTOR4
Text Label 4500 2000 2    50   ~ 0
EN_MOTOR3
Text Label 4500 2100 2    50   ~ 0
EN_MOTOR2
Text Label 4500 2200 2    50   ~ 0
EN_MOTOR1
Wire Wire Line
	8350 5600 9150 5600
$Comp
L Device:Jumper_NC_Dual JP_Prog1
U 1 1 5C0B9B6A
P 2400 5550
F 0 "JP_Prog1" V 2354 5651 50  0000 L CNN
F 1 "Jumper_Prog" V 2445 5651 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 2400 5550 50  0001 C CNN
F 3 "~" H 2400 5550 50  0001 C CNN
F 4 "JUMPER 2,54 SW" H 2400 5550 50  0001 C CNN "ReicheltPN"
F 5 "https://www.reichelt.de/kurzschlussbruecke-schwarz-rm-2-54-jumper-2-54-sw-p9017.html?&trstct=pos_0" H 2400 5550 50  0001 C CNN "ReicheltPL"
	1    2400 5550
	0    -1   -1   0   
$EndComp
$Comp
L LasertagComponents:SN74AHC541 U7
U 1 1 5BFEFB2F
P 4950 4950
F 0 "U7" H 4700 5500 50  0000 C CNN
F 1 "SN74AHC541" H 5200 5500 50  0000 C CNN
F 2 "Package_DIP:DIP-20_W7.62mm_LongPads" H 4950 4950 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74ahc541.pdf" H 4950 4950 50  0001 C CNN
F 4 "296-4610-5-ND" H 4950 4950 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/texas-instruments/SN74AHC541N/296-4610-5-ND/376228" H 4950 4950 50  0001 C CNN "DigikeyPL"
	1    4950 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 4600 3850 4600
Wire Wire Line
	3850 4700 4450 4700
Wire Wire Line
	3850 4800 4450 4800
Wire Wire Line
	3850 4900 4450 4900
Wire Wire Line
	3850 5000 4450 5000
Wire Wire Line
	3850 5100 4450 5100
Wire Wire Line
	3850 5300 4450 5300
Wire Wire Line
	5450 4600 6100 4600
Wire Wire Line
	6100 4700 5450 4700
Wire Wire Line
	6100 4800 5450 4800
Wire Wire Line
	6100 4900 5450 4900
Wire Wire Line
	4950 4250 4950 4000
Wire Wire Line
	4500 5450 4500 5550
Connection ~ 4500 5550
Text Label 4950 4000 3    50   ~ 0
VCC
$Comp
L power:GND #PWR0125
U 1 1 5C044A81
P 4950 5850
F 0 "#PWR0125" H 4950 5600 50  0001 C CNN
F 1 "GND" H 4955 5677 50  0000 C CNN
F 2 "" H 4950 5850 50  0001 C CNN
F 3 "" H 4950 5850 50  0001 C CNN
	1    4950 5850
	1    0    0    -1  
$EndComp
Text Label 3850 4600 0    50   ~ 0
ISD_SCLK_MCU
Text Label 6100 4600 2    50   ~ 0
ISD_SCLK
Text Label 3850 4700 0    50   ~ 0
ISD_SSB_MCU
Text Label 6100 4700 2    50   ~ 0
ISD_SSB
Text Label 3850 4800 0    50   ~ 0
ISD_MOSI_MCU
Text Label 6100 4800 2    50   ~ 0
ISD_MOSI
Text Label 3850 4900 0    50   ~ 0
ISD_MISO
Text Label 6100 4900 2    50   ~ 0
ISD_MISO_MCU
Text Label 3850 5000 0    50   ~ 0
ISD_INTB
Text Label 3850 5100 0    50   ~ 0
ISD_BSYB
Wire Wire Line
	9150 5300 9150 5050
Wire Wire Line
	9150 5050 9300 5050
Text Label 9300 5050 2    50   ~ 0
VCC
$Comp
L power:GND #PWR0126
U 1 1 5C04F9D9
P 2400 5800
F 0 "#PWR0126" H 2400 5550 50  0001 C CNN
F 1 "GND" H 2405 5627 50  0000 C CNN
F 2 "" H 2400 5800 50  0001 C CNN
F 3 "" H 2400 5800 50  0001 C CNN
	1    2400 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 5300 2400 5100
Text Label 2400 5100 3    50   ~ 0
VCC
Text HLabel 3200 5200 0    50   Input ~ 0
DETC_TO_AVRMOSI
Text GLabel 5450 5200 2    50   Input ~ 0
AVR_MOSI
$Comp
L power:GND #PWR05
U 1 1 5C0662D9
P 3850 5300
F 0 "#PWR05" H 3850 5050 50  0001 C CNN
F 1 "GND" H 3855 5127 50  0000 C CNN
F 2 "" H 3850 5300 50  0001 C CNN
F 3 "" H 3850 5300 50  0001 C CNN
	1    3850 5300
	1    0    0    -1  
$EndComp
NoConn ~ 5450 5300
$Comp
L power:GND #PWR04
U 1 1 5C06AA5A
P 1650 7150
F 0 "#PWR04" H 1650 6900 50  0001 C CNN
F 1 "GND" H 1655 6977 50  0000 C CNN
F 2 "" H 1650 7150 50  0001 C CNN
F 3 "" H 1650 7150 50  0001 C CNN
	1    1650 7150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 7150 1800 7150
NoConn ~ 2600 7150
Text GLabel 5450 5100 2    50   Input ~ 0
AVR_SCK
Text GLabel 5450 5000 2    50   Input ~ 0
AVR_MISO
NoConn ~ 8350 5800
$Comp
L power:GND #PWR06
U 1 1 5C078971
P 7200 2550
F 0 "#PWR06" H 7200 2300 50  0001 C CNN
F 1 "GND" H 7205 2377 50  0000 C CNN
F 2 "" H 7200 2550 50  0001 C CNN
F 3 "" H 7200 2550 50  0001 C CNN
	1    7200 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 2550 7450 2550
$Comp
L Connector:TestPoint TP7
U 1 1 5C1199E9
P 9150 5600
F 0 "TP7" H 9092 5627 50  0000 R CNN
F 1 "TP_INTB" H 9092 5718 50  0000 R CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 9350 5600 50  0001 C CNN
F 3 "~" H 9350 5600 50  0001 C CNN
	1    9150 5600
	-1   0    0    1   
$EndComp
Text Notes 1250 5400 0    20   ~ 0
Disconnect (HighImp) SPI wiring between ICs during programming
Text Notes 10500 5950 0    20   ~ 0
SoundIC programming interface\n\n
Text Notes 3750 6850 0    20   ~ 0
If enabled VOL arouind 0.4V (@100mA) here\n
Text Notes 5550 4500 0    20   ~ 0
Up to 10ns propagation delay\n
Text Notes 8050 3550 0    39   ~ 0
0x14
Text Notes 8950 2300 0    39   ~ 0
VOL around 0.4V @25mA\n
NoConn ~ 8750 2200
NoConn ~ 8750 2300
NoConn ~ 8750 2400
$Comp
L Device:R R?
U 1 1 5C021C85
P 9150 5450
AR Path="/5B4D0E2A/5C021C85" Ref="R?"  Part="1" 
AR Path="/5B6AF6DC/5C021C85" Ref="R13"  Part="1" 
F 0 "R13" V 9230 5450 50  0000 C CNN
F 1 "10k" V 9150 5450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 9080 5450 50  0001 C CNN
F 3 "" H 9150 5450 50  0001 C CNN
F 4 "CF14JT10K0CT-ND" H 9150 5450 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/stackpole-electronics-inc/CF14JT10K0/CF14JT10K0CT-ND/1830374" H 9150 5450 50  0001 C CNN "DigikeyPL"
	1    9150 5450
	1    0    0    -1  
$EndComp
Connection ~ 9150 5600
$Comp
L Device:R R14
U 1 1 5C02B507
P 10350 1500
F 0 "R14" V 10143 1500 50  0000 C CNN
F 1 "330" V 10234 1500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 10280 1500 50  0001 C CNN
F 3 "~" H 10350 1500 50  0001 C CNN
F 4 "CF14JT330RCT-ND" H 10350 1500 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/stackpole-electronics-inc/CF14JT330R/CF14JT330RCT-ND/1830338" H 10350 1500 50  0001 C CNN "DigikeyPL"
	1    10350 1500
	-1   0    0    1   
$EndComp
$Comp
L Device:R R7
U 1 1 5C02B6E7
P 9200 1500
F 0 "R7" H 9270 1546 50  0000 L CNN
F 1 "82" H 9270 1455 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 9130 1500 50  0001 C CNN
F 3 "~" H 9200 1500 50  0001 C CNN
F 4 "82QBK-ND" H 9200 1500 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/yageo/CFR-25JB-52-82R/82QBK-ND/3609" H 9200 1500 50  0001 C CNN "DigikeyPL"
	1    9200 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5C031186
P 6600 5750
AR Path="/5B4D0E2A/5C031186" Ref="C?"  Part="1" 
AR Path="/5B6AF6DC/5C031186" Ref="C8"  Part="1" 
F 0 "C8" H 6625 5850 50  0000 L CNN
F 1 "100n" H 6625 5650 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.8mm_W2.6mm_P2.50mm" H 6638 5600 50  0001 C CNN
F 3 "" H 6600 5750 50  0001 C CNN
F 4 "BC5137-ND" H 6600 5750 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/vishay-bc-components/K104K10X7RF53L2/BC5137-ND/2820500" H 6600 5750 50  0001 C CNN "DigikeyPL"
	1    6600 5750
	1    0    0    -1  
$EndComp
Connection ~ 6600 5850
$Comp
L Device:C_Small C?
U 1 1 5C03551A
P 6550 5350
AR Path="/5B4D0E2A/5C03551A" Ref="C?"  Part="1" 
AR Path="/5B6AF6DC/5C03551A" Ref="C3"  Part="1" 
F 0 "C3" H 6575 5450 50  0000 L CNN
F 1 "100n" H 6575 5250 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.8mm_W2.6mm_P2.50mm" H 6588 5200 50  0001 C CNN
F 3 "" H 6550 5350 50  0001 C CNN
F 4 "BC5137-ND" H 6550 5350 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/vishay-bc-components/K104K10X7RF53L2/BC5137-ND/2820500" H 6550 5350 50  0001 C CNN "DigikeyPL"
	1    6550 5350
	1    0    0    -1  
$EndComp
Text Notes 2750 6450 0    20   ~ 0
Should be connected close to the motor connector\nto allow the internal inductive kickback protection to work\n
Wire Wire Line
	2200 7350 2200 7450
Text HLabel 3150 6350 2    50   BiDi ~ 0
HEAD_VCC
Wire Wire Line
	3150 6350 2600 6350
$Comp
L power:+5V #PWR?
U 1 1 5DBFC44E
P 10000 850
F 0 "#PWR?" H 10000 700 50  0001 C CNN
F 1 "+5V" H 10015 1023 50  0000 C CNN
F 2 "" H 10000 850 50  0001 C CNN
F 3 "" H 10000 850 50  0001 C CNN
	1    10000 850 
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DB10AA8
P 3450 4950
AR Path="/5B4D0E2A/5DB10AA8" Ref="R?"  Part="1" 
AR Path="/5B6AF6DC/5DB10AA8" Ref="R?"  Part="1" 
F 0 "R?" V 3530 4950 50  0000 C CNN
F 1 "10k" V 3450 4950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3380 4950 50  0001 C CNN
F 3 "" H 3450 4950 50  0001 C CNN
F 4 "CF14JT10K0CT-ND" H 3450 4950 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/stackpole-electronics-inc/CF14JT10K0/CF14JT10K0CT-ND/1830374" H 3450 4950 50  0001 C CNN "DigikeyPL"
	1    3450 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 5200 3450 5200
Wire Wire Line
	3450 5100 3450 5200
Connection ~ 3450 5200
Wire Wire Line
	3450 5200 3200 5200
Wire Wire Line
	3450 4800 3450 4750
Wire Wire Line
	3450 4750 3250 4750
Text Label 3250 4750 0    50   ~ 0
VCC
Wire Wire Line
	2500 5550 4500 5550
$EndSCHEMATC
