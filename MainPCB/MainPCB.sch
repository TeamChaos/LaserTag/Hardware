EESchema Schematic File Version 4
LIBS:MainPCB-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1400 5350 1450 850 
U 5B4D0E2A
F0 "DetectorIC" 60
F1 "DetectorIC.sch" 60
F2 "Detector_Available" I L 1400 5450 50 
F3 "DETECTOR_HEAD1" I R 2850 5550 50 
F4 "DETECTOR_HEAD2" I R 2850 5700 50 
F5 "DETECTOR_HEAD3" I R 2850 5850 50 
F6 "DETECTOR_HEAD4" I R 2850 6000 50 
F7 "DETC_TO_AVRMOSI" O L 1400 5900 50 
F8 "AVRMOSI_FROM_DETC" I L 1400 6100 50 
$EndSheet
Wire Wire Line
	1400 5450 600  5450
Text Label 600  5450 0    50   ~ 0
DETECTOR_AVAILABLE
$Comp
L Connector_Generic:Conn_01x10 J_Head1
U 1 1 5C061AF8
P 3950 6350
F 0 "J_Head1" H 4030 6342 50  0000 L CNN
F 1 "Conn_Head" H 4030 6251 50  0000 L CNN
F 2 "Connector_JST:JST_XH_S10B-XH-A_1x10_P2.50mm_Horizontal" H 3950 6350 50  0001 C CNN
F 3 "~" H 3950 6350 50  0001 C CNN
F 4 "455-2228-ND" H 3950 6350 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/jst-sales-america-inc/S10B-XH-A-LF-SN/455-2228-ND/1651026" H 3950 6350 50  0001 C CNN "DigikeyPL"
	1    3950 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 5950 3750 5950
Wire Wire Line
	3750 6850 3500 6850
$Comp
L power:GND #PWR0105
U 1 1 5C0679BB
P 3500 6850
F 0 "#PWR0105" H 3500 6600 50  0001 C CNN
F 1 "GND" H 3505 6677 50  0000 C CNN
F 2 "" H 3500 6850 50  0001 C CNN
F 3 "" H 3500 6850 50  0001 C CNN
	1    3500 6850
	1    0    0    -1  
$EndComp
Text Label 5400 6000 2    50   ~ 0
VCC
Wire Wire Line
	3750 6450 3100 6450
Wire Wire Line
	3750 6550 3200 6550
Wire Wire Line
	3750 6650 3300 6650
Wire Wire Line
	3750 6750 3400 6750
Wire Wire Line
	2850 5550 3350 5550
Wire Wire Line
	3350 5550 3350 6050
Wire Wire Line
	3350 6050 3750 6050
Wire Wire Line
	2850 5700 3250 5700
Wire Wire Line
	3250 5700 3250 6150
Wire Wire Line
	3250 6150 3750 6150
Wire Wire Line
	2850 5850 3150 5850
Wire Wire Line
	3150 5850 3150 6250
Wire Wire Line
	3150 6250 3750 6250
Wire Wire Line
	2850 6000 3050 6000
Wire Wire Line
	3050 6000 3050 6350
Wire Wire Line
	3050 6350 3750 6350
$Comp
L Device:CP C10
U 1 1 5C0796CC
P 4650 6300
AR Path="/5C0796CC" Ref="C10"  Part="1" 
AR Path="/5B6AF6DC/5C0796CC" Ref="C?"  Part="1" 
F 0 "C10" H 4768 6346 50  0000 L CNN
F 1 "6800u" H 4768 6255 50  0000 L CNN
F 2 "LasertagFootprints:CP_Bended_D12.5mm_P5.00mm" H 4688 6150 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Panasonic%20Electronic%20Components/ECA-xxM%20Series,TypeA.pdf" H 4650 6300 50  0001 C CNN
F 4 "P5119-ND" H 4650 6300 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/panasonic-electronic-components/ECA-0JM682/P5119-ND/244978" H 4650 6300 50  0001 C CNN "DigikeyPL"
	1    4650 6300
	1    0    0    -1  
$EndComp
$Comp
L Device:Net-Tie_2 NT2
U 1 1 5C080027
P 5000 6000
F 0 "NT2" H 5000 6178 50  0000 C CNN
F 1 "Net-Tie_2" H 5000 6087 50  0000 C CNN
F 2 "NetTie:NetTie-2_SMD_Pad0.5mm" H 5000 6000 50  0001 C CNN
F 3 "~" H 5000 6000 50  0001 C CNN
	1    5000 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 6150 4650 6000
Wire Wire Line
	4650 6000 4900 6000
Wire Wire Line
	5100 6000 5400 6000
$Comp
L power:GND #PWR0127
U 1 1 5C08DB4C
P 4650 6450
F 0 "#PWR0127" H 4650 6200 50  0001 C CNN
F 1 "GND" H 4655 6277 50  0000 C CNN
F 2 "" H 4650 6450 50  0001 C CNN
F 3 "" H 4650 6450 50  0001 C CNN
	1    4650 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 6000 4650 5750
Wire Wire Line
	3500 5750 3500 5950
Connection ~ 4650 6000
$Sheet
S 1150 750  1050 950 
U 5DEDF112
F0 "Power" 50
F1 "power.sch" 50
F2 "5V" O R 2200 1000 50 
F3 "3V" O R 2200 1150 50 
F4 "SupplyMonitor" O R 2200 1350 50 
F5 "VBAT" I L 1150 1050 50 
F6 "5VGND" B L 1150 1550 50 
F7 "GND" O R 2200 1600 50 
$EndSheet
Wire Wire Line
	2200 1000 2550 1000
Wire Wire Line
	2200 1150 2550 1150
Wire Wire Line
	2200 1350 2850 1350
Wire Wire Line
	2200 1600 2450 1600
$Comp
L power:GND #PWR05
U 1 1 5DF225E7
P 2450 1750
F 0 "#PWR05" H 2450 1500 50  0001 C CNN
F 1 "GND" H 2455 1577 50  0000 C CNN
F 2 "" H 2450 1750 50  0001 C CNN
F 3 "" H 2450 1750 50  0001 C CNN
	1    2450 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 1600 2450 1750
$Comp
L power:+5V #PWR06
U 1 1 5DF26416
P 2550 1000
F 0 "#PWR06" H 2550 850 50  0001 C CNN
F 1 "+5V" H 2565 1173 50  0000 C CNN
F 2 "" H 2550 1000 50  0001 C CNN
F 3 "" H 2550 1000 50  0001 C CNN
	1    2550 1000
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR07
U 1 1 5DF26C7E
P 2550 1150
F 0 "#PWR07" H 2550 1000 50  0001 C CNN
F 1 "+3.3V" H 2565 1323 50  0000 C CNN
F 2 "" H 2550 1150 50  0001 C CNN
F 3 "" H 2550 1150 50  0001 C CNN
	1    2550 1150
	1    0    0    -1  
$EndComp
Text Label 2350 1150 0    50   ~ 0
VCC
Text Label 2350 1000 0    50   ~ 0
5V
NoConn ~ 4750 2500
NoConn ~ 4750 2600
NoConn ~ 4750 2700
NoConn ~ 4750 2800
NoConn ~ 4750 2900
NoConn ~ 4750 3000
$Comp
L RF_Module:ESP32-WROOM-32 U4
U 1 1 5DF97CB3
P 5350 2500
AR Path="/5DF97CB3" Ref="U4"  Part="1" 
AR Path="/5DD74072/5DF97CB3" Ref="U?"  Part="1" 
F 0 "U4" H 5650 1150 50  0000 C CNN
F 1 "ESP32-WROOM-32" H 5350 3850 50  0000 C CNN
F 2 "RF_Module:ESP32-WROOM-32" H 5350 1000 50  0001 C CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32_datasheet_en.pdf" H 5050 2550 50  0001 C CNN
F 4 "1904-1010-1-ND" H 5350 2500 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/espressif-systems/ESP32-WROOM-32/1904-1010-1-ND/8544305" H 5350 2500 50  0001 C CNN "DigikeyPL"
	1    5350 2500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DF97CB9
P 4000 1100
AR Path="/5DD74072/5DF97CB9" Ref="R?"  Part="1" 
AR Path="/5DF97CB9" Ref="R2"  Part="1" 
F 0 "R2" H 4070 1146 50  0000 L CNN
F 1 "10k" H 4070 1055 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3930 1100 50  0001 C CNN
F 3 "~" H 4000 1100 50  0001 C CNN
	1    4000 1100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DF97CBF
P 3250 1450
AR Path="/5DD74072/5DF97CBF" Ref="#PWR?"  Part="1" 
AR Path="/5DF97CBF" Ref="#PWR09"  Part="1" 
F 0 "#PWR09" H 3250 1200 50  0001 C CNN
F 1 "GND" H 3255 1277 50  0000 C CNN
F 2 "" H 3250 1450 50  0001 C CNN
F 3 "" H 3250 1450 50  0001 C CNN
	1    3250 1450
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5DF97CC5
P 3600 900
AR Path="/5DD74072/5DF97CC5" Ref="#PWR?"  Part="1" 
AR Path="/5DF97CC5" Ref="#PWR010"  Part="1" 
F 0 "#PWR010" H 3600 750 50  0001 C CNN
F 1 "+3.3V" H 3615 1073 50  0000 C CNN
F 2 "" H 3600 900 50  0001 C CNN
F 3 "" H 3600 900 50  0001 C CNN
	1    3600 900 
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW?
U 1 1 5DF97CCB
P 3550 1350
AR Path="/5DD74072/5DF97CCB" Ref="SW?"  Part="1" 
AR Path="/5DF97CCB" Ref="SW1"  Part="1" 
F 0 "SW1" H 3550 1635 50  0000 C CNN
F 1 "SW_Push" H 3550 1544 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 3550 1550 50  0001 C CNN
F 3 "~" H 3550 1550 50  0001 C CNN
F 4 "450-1650-ND" H 3550 1350 50  0001 C CNN "DigikeyPN"
	1    3550 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 1450 3250 1350
Wire Wire Line
	3250 1350 3350 1350
Wire Wire Line
	4750 1350 4750 1300
Wire Wire Line
	3750 1350 4000 1350
Wire Wire Line
	4000 1250 4000 1350
Connection ~ 4000 1350
Wire Wire Line
	4000 1350 4750 1350
Wire Wire Line
	3600 900  4000 900 
Wire Wire Line
	4000 900  4000 950 
Text Label 3850 900  0    50   ~ 0
VCC
Text Label 3250 1350 0    50   ~ 0
GND
Wire Wire Line
	4000 900  5350 900 
Wire Wire Line
	5350 900  5350 1100
Connection ~ 4000 900 
Wire Wire Line
	5350 3900 5350 4050
$Comp
L power:GND #PWR?
U 1 1 5DF97CE0
P 5350 4050
AR Path="/5DD74072/5DF97CE0" Ref="#PWR?"  Part="1" 
AR Path="/5DF97CE0" Ref="#PWR011"  Part="1" 
F 0 "#PWR011" H 5350 3800 50  0001 C CNN
F 1 "GND" H 5355 3877 50  0000 C CNN
F 2 "" H 5350 4050 50  0001 C CNN
F 3 "" H 5350 4050 50  0001 C CNN
	1    5350 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 1400 6650 1400
Wire Wire Line
	5950 1600 6650 1600
Text Label 6650 1400 2    50   ~ 0
ESP_UART_TX
Text Label 6650 1600 2    50   ~ 0
ESP_UART_RX
$Comp
L Switch:SW_Push SW?
U 1 1 5DF97CEB
P 7200 1300
AR Path="/5DD74072/5DF97CEB" Ref="SW?"  Part="1" 
AR Path="/5DF97CEB" Ref="SW2"  Part="1" 
F 0 "SW2" H 7200 1585 50  0000 C CNN
F 1 "SW_Push" H 7200 1494 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 7200 1500 50  0001 C CNN
F 3 "~" H 7200 1500 50  0001 C CNN
F 4 "450-1650-ND" H 7200 1300 50  0001 C CNN "DigikeyPN"
	1    7200 1300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DF97CF1
P 7650 1350
AR Path="/5DD74072/5DF97CF1" Ref="#PWR?"  Part="1" 
AR Path="/5DF97CF1" Ref="#PWR014"  Part="1" 
F 0 "#PWR014" H 7650 1100 50  0001 C CNN
F 1 "GND" H 7655 1177 50  0000 C CNN
F 2 "" H 7650 1350 50  0001 C CNN
F 3 "" H 7650 1350 50  0001 C CNN
	1    7650 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 1350 7650 1300
Wire Wire Line
	7650 1300 7400 1300
Wire Wire Line
	5950 2500 6650 2500
Text Label 6650 2500 2    50   ~ 0
SPI_CLK
Wire Wire Line
	5950 2600 6650 2600
Wire Wire Line
	5950 2900 6650 2900
Wire Wire Line
	5950 1800 6650 1800
Text Label 6650 2900 2    50   ~ 0
SPI_MOSI
Text Label 6650 2600 2    50   ~ 0
SPI_MISO
Text Label 6650 1800 2    50   ~ 0
SPI_CS_RF
Wire Wire Line
	5950 2400 6650 2400
Text Label 6650 2800 2    50   ~ 0
I2C_SCL
Text Label 6650 2700 2    50   ~ 0
I2C_SDA
Text Label 6650 2400 2    50   ~ 0
SPI_CS_ISD
Wire Wire Line
	5950 2300 6650 2300
Wire Wire Line
	5950 1700 6650 1700
Text Label 6650 2300 2    50   ~ 0
RF_SDN
Text Label 6650 1700 2    50   ~ 0
RF_IRQ
Wire Wire Line
	5950 3400 6650 3400
Wire Wire Line
	5950 3300 6650 3300
Wire Wire Line
	5950 3200 6650 3200
Wire Wire Line
	5950 3100 6650 3100
Wire Wire Line
	5950 3000 6650 3000
Text Label 6650 3000 2    50   ~ 0
MOTOR4_EN
Text Label 6650 3100 2    50   ~ 0
MOTOR3_EN
Text Label 6650 3200 2    50   ~ 0
MOTOR2_EN
Text Label 6650 3300 2    50   ~ 0
MOTOR6_EN
Text Label 6650 3400 2    50   ~ 0
MOTOR5_EN
Wire Wire Line
	5950 2100 6650 2100
Text Label 6650 2100 2    50   ~ 0
MOTOR1_EN
Wire Wire Line
	5950 2200 6650 2200
Text Label 6650 2200 2    50   ~ 0
SUPPLY_MONITOR
Wire Wire Line
	5950 1300 7000 1300
Wire Wire Line
	4750 1500 4150 1500
Text Label 6800 2000 2    50   ~ 0
DETECTOR_AVAILABLE
Wire Wire Line
	4750 1600 4150 1600
Text Label 4150 1500 0    50   ~ 0
ISD_BSYB_MCU
Text Label 4150 1600 0    50   ~ 0
ISD_INTB_MCU
$Sheet
S 9200 3000 950  500 
U 5DFB6068
F0 "WeaponConn" 50
F1 "weapon_conn.sch" 50
F2 "I2C_SDA" I L 9200 3200 50 
F3 "I2C_SCL" I L 9200 3400 50 
F4 "I2C_SDA_HV" O R 10150 3200 50 
F5 "I2C_SCL_HV" O R 10150 3400 50 
$EndSheet
Wire Wire Line
	2550 1000 2850 1000
Connection ~ 2550 1000
Wire Wire Line
	2550 1150 2850 1150
Connection ~ 2550 1150
Text GLabel 2850 1000 2    50   BiDi ~ 0
5V
Text GLabel 2850 1150 2    50   BiDi ~ 0
VCC
Wire Wire Line
	5950 2700 6850 2700
Wire Wire Line
	5950 2800 6850 2800
Text GLabel 6850 2700 2    50   BiDi ~ 0
I2C_SDA
Text GLabel 6850 2800 2    50   BiDi ~ 0
I2C_SCL
Wire Wire Line
	9200 3200 8850 3200
Wire Wire Line
	9200 3400 8850 3400
Text Label 8850 3200 0    50   ~ 0
I2C_SDA
Text Label 8850 3400 0    50   ~ 0
I2C_SCL
$Comp
L Connector_Generic:Conn_01x06 J_ESPProg1
U 1 1 5DFF60E8
P 5350 4950
AR Path="/5DFF60E8" Ref="J_ESPProg1"  Part="1" 
AR Path="/5DD74072/5DFF60E8" Ref="J_ESPProg?"  Part="1" 
F 0 "J_ESPProg1" H 5270 4425 50  0000 C CNN
F 1 "ESP_Programmer" H 5270 4516 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 5350 4950 50  0001 C CNN
F 3 "~" H 5350 4950 50  0001 C CNN
F 4 "RND 205-00627" H 5350 4950 50  0001 C CNN "ReicheltPN"
	1    5350 4950
	-1   0    0    1   
$EndComp
Wire Wire Line
	5550 4850 6550 4850
Wire Wire Line
	5550 4950 6550 4950
Text Label 5700 4950 0    50   ~ 0
ESP_UART_RX
Text Label 5700 4850 0    50   ~ 0
ESP_UART_TX
Wire Wire Line
	5550 5050 6550 5050
Wire Wire Line
	5550 5150 6550 5150
Text Label 5700 5050 0    50   ~ 0
I2C_SCL
Text Label 5700 5150 0    50   ~ 0
I2C_SDA
Text Notes 5300 5300 0    20   ~ 0
Have I2C here too for debugging purposes
NoConn ~ 5550 4650
$Comp
L power:GND #PWR?
U 1 1 5DFF60F8
P 6750 4750
AR Path="/5DD74072/5DFF60F8" Ref="#PWR?"  Part="1" 
AR Path="/5DFF60F8" Ref="#PWR013"  Part="1" 
F 0 "#PWR013" H 6750 4500 50  0001 C CNN
F 1 "GND" H 6755 4577 50  0000 C CNN
F 2 "" H 6750 4750 50  0001 C CNN
F 3 "" H 6750 4750 50  0001 C CNN
	1    6750 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 4750 6750 4750
$Comp
L LasertagComponents:Si4463Board U6
U 1 1 5E0013A6
P 9750 2050
AR Path="/5E0013A6" Ref="U6"  Part="1" 
AR Path="/5DD74072/5E0013A6" Ref="U?"  Part="1" 
F 0 "U6" H 9750 2765 50  0000 C CNN
F 1 "Si4463Board" H 9750 2674 50  0000 C CNN
F 2 "LasertagFootprints:Si4463Board" H 9750 2050 50  0001 C CNN
F 3 "" H 9750 2050 50  0001 C CNN
	1    9750 2050
	1    0    0    -1  
$EndComp
NoConn ~ 10300 2250
NoConn ~ 10300 2350
NoConn ~ 10300 2450
NoConn ~ 10300 2550
Wire Wire Line
	10300 1650 10900 1650
Wire Wire Line
	10300 1800 10900 1800
Wire Wire Line
	10900 1800 10900 1900
Wire Wire Line
	9200 1650 8600 1650
Wire Wire Line
	9200 1800 8600 1800
Wire Wire Line
	9200 1950 8600 1950
Wire Wire Line
	9200 2100 8600 2100
Wire Wire Line
	9200 2550 8600 2550
Text Label 8600 2400 0    50   ~ 0
RF_IRQ
Text Label 8600 2550 0    50   ~ 0
RF_SDN
Text Label 8600 1650 0    50   ~ 0
SPI_MOSI
Text Label 8600 1800 0    50   ~ 0
SPI_MISO
Text Label 8600 1950 0    50   ~ 0
SPI_CLK
Text Label 8600 2100 0    50   ~ 0
SPI_CS_RF
$Comp
L power:GND #PWR?
U 1 1 5E0013BF
P 10900 1900
AR Path="/5DD74072/5E0013BF" Ref="#PWR?"  Part="1" 
AR Path="/5E0013BF" Ref="#PWR017"  Part="1" 
F 0 "#PWR017" H 10900 1650 50  0001 C CNN
F 1 "GND" H 10905 1727 50  0000 C CNN
F 2 "" H 10900 1900 50  0001 C CNN
F 3 "" H 10900 1900 50  0001 C CNN
	1    10900 1900
	1    0    0    -1  
$EndComp
Text Label 10900 1650 2    50   ~ 0
VCC
$Comp
L LasertagComponents:ISD2360 U?
U 1 1 5E014C9F
P 9350 5800
AR Path="/5B6AF6DC/5E014C9F" Ref="U?"  Part="1" 
AR Path="/5DD74072/5E014C9F" Ref="U?"  Part="1" 
AR Path="/5E014C9F" Ref="U5"  Part="1" 
F 0 "U5" H 9325 6565 50  0000 C CNN
F 1 "ISD2360" H 9325 6474 50  0000 C CNN
F 2 "LasertagFootprints:SOP-16_7.5x10.4mm_P1.27mm_Hand" H 9350 5800 50  0001 C CNN
F 3 "http://www.nuvoton.com/resource-files/EN_ISD2360_Datasheet_Rev1-0.pdf" H 9350 5800 50  0001 C CNN
F 4 "ISD2360SYI-ND" H 9350 5800 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/nuvoton-technology-corporation-of-america/ISD2360SYI/ISD2360SYI-ND/3507392" H 9350 5800 50  0001 C CNN "DigikeyPL"
	1    9350 5800
	1    0    0    -1  
$EndComp
$Comp
L Device:CP_Small C?
U 1 1 5E014CA7
P 7900 5450
AR Path="/5B6AF6DC/5E014CA7" Ref="C?"  Part="1" 
AR Path="/5DD74072/5E014CA7" Ref="C?"  Part="1" 
AR Path="/5E014CA7" Ref="C1"  Part="1" 
F 0 "C1" H 7988 5496 50  0000 L CNN
F 1 "10u" H 7988 5405 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 7900 5450 50  0001 C CNN
F 3 "~" H 7900 5450 50  0001 C CNN
F 4 "732-8593-1-ND" H 7900 5450 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/wurth-electronics-inc/860010372001/732-8593-1-ND/5728550" H 7900 5450 50  0001 C CNN "DigikeyPL"
	1    7900 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 5800 8550 5800
Wire Wire Line
	8550 5800 8550 5750
Wire Wire Line
	8550 5750 8250 5750
Wire Wire Line
	8650 5900 8650 5950
Wire Wire Line
	8650 5950 8250 5950
Wire Wire Line
	8650 5600 8650 5500
Wire Wire Line
	8650 5600 8200 5600
Wire Wire Line
	7900 5600 7900 5550
Connection ~ 8650 5600
Wire Wire Line
	8200 5550 8200 5600
Connection ~ 8200 5600
Wire Wire Line
	8200 5600 7900 5600
Wire Wire Line
	8650 5400 8650 5300
Wire Wire Line
	8650 5300 8200 5300
Wire Wire Line
	7900 5300 7900 5350
Connection ~ 8650 5300
Wire Wire Line
	8200 5350 8200 5300
Connection ~ 8200 5300
Wire Wire Line
	8200 5300 7900 5300
$Comp
L power:GND #PWR?
U 1 1 5E014CC0
P 8250 5950
AR Path="/5B6AF6DC/5E014CC0" Ref="#PWR?"  Part="1" 
AR Path="/5DD74072/5E014CC0" Ref="#PWR?"  Part="1" 
AR Path="/5E014CC0" Ref="#PWR016"  Part="1" 
F 0 "#PWR016" H 8250 5700 50  0001 C CNN
F 1 "GND" H 8255 5777 50  0000 C CNN
F 2 "" H 8250 5950 50  0001 C CNN
F 3 "" H 8250 5950 50  0001 C CNN
	1    8250 5950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E014CC6
P 7900 5600
AR Path="/5B6AF6DC/5E014CC6" Ref="#PWR?"  Part="1" 
AR Path="/5DD74072/5E014CC6" Ref="#PWR?"  Part="1" 
AR Path="/5E014CC6" Ref="#PWR015"  Part="1" 
F 0 "#PWR015" H 7900 5350 50  0001 C CNN
F 1 "GND" H 7905 5427 50  0000 C CNN
F 2 "" H 7900 5600 50  0001 C CNN
F 3 "" H 7900 5600 50  0001 C CNN
	1    7900 5600
	1    0    0    -1  
$EndComp
Connection ~ 7900 5600
Text Label 7950 5300 0    50   ~ 0
VCC
Text Label 8300 5750 0    50   ~ 0
VCC
Wire Wire Line
	10000 5300 10550 5300
Wire Wire Line
	10000 5400 10550 5400
Wire Wire Line
	10000 5500 10550 5500
Wire Wire Line
	10000 5600 10550 5600
Text Label 10100 5300 0    50   ~ 0
ISD_MOSI
Text Label 10100 5400 0    50   ~ 0
ISD_SCLK
Text Label 10100 5500 0    50   ~ 0
SPI_MISO
Text Label 10100 5600 0    50   ~ 0
ISD_SSB
$Comp
L Connector_Generic:Conn_01x02 J_Speak?
U 1 1 5E014CD7
P 10450 6200
AR Path="/5B6AF6DC/5E014CD7" Ref="J_Speak?"  Part="1" 
AR Path="/5DD74072/5E014CD7" Ref="J_Speak?"  Part="1" 
AR Path="/5E014CD7" Ref="J_Speak1"  Part="1" 
F 0 "J_Speak1" H 10530 6192 50  0000 L CNN
F 1 "Conn_Speaker" H 10530 6101 50  0000 L CNN
F 2 "LasertagFootprints:SolderWirePad_2x_0-8mmDrill" H 10450 6200 50  0001 C CNN
F 3 "~" H 10450 6200 50  0001 C CNN
	1    10450 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	10250 6200 10000 6200
Wire Wire Line
	10000 6300 10250 6300
$Comp
L Connector_Generic:Conn_01x06 J_ISDProg?
U 1 1 5E014CDF
P 6750 5850
AR Path="/5B6AF6DC/5E014CDF" Ref="J_ISDProg?"  Part="1" 
AR Path="/5DD74072/5E014CDF" Ref="J_ISDProg?"  Part="1" 
AR Path="/5E014CDF" Ref="J_ISDProg1"  Part="1" 
F 0 "J_ISDProg1" H 6830 5892 50  0000 L CNN
F 1 "ISD_Programmer" H 6830 5801 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 6750 5850 50  0001 C CNN
F 3 "~" H 6750 5850 50  0001 C CNN
F 4 "RND 205-00627" H 6750 5850 50  0001 C CNN "ReicheltPN"
	1    6750 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 5650 6050 5650
Wire Wire Line
	6050 5750 6550 5750
Wire Wire Line
	6050 5850 6550 5850
Wire Wire Line
	6050 5950 6550 5950
Wire Wire Line
	6050 6050 6550 6050
Text Label 6150 5950 0    50   ~ 0
ISD_MOSI
Text Label 6150 5650 0    50   ~ 0
SPI_MISO
Text Label 6150 5750 0    50   ~ 0
ISD_SCLK
Text Label 6150 5850 0    50   ~ 0
ISD_SSB
Text Label 6150 6050 0    50   ~ 0
ISD_BSYB
$Comp
L power:GND #PWR?
U 1 1 5E014CEF
P 6550 6150
AR Path="/5B6AF6DC/5E014CEF" Ref="#PWR?"  Part="1" 
AR Path="/5DD74072/5E014CEF" Ref="#PWR?"  Part="1" 
AR Path="/5E014CEF" Ref="#PWR012"  Part="1" 
F 0 "#PWR012" H 6550 5900 50  0001 C CNN
F 1 "GND" H 6555 5977 50  0000 C CNN
F 2 "" H 6550 6150 50  0001 C CNN
F 3 "" H 6550 6150 50  0001 C CNN
	1    6550 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	10550 5800 10000 5800
Text Label 10100 5700 0    50   ~ 0
ISD_INTB
Text Label 10100 5800 0    50   ~ 0
ISD_BSYB
Wire Wire Line
	10000 5700 10800 5700
Wire Wire Line
	10800 5400 10800 5150
Wire Wire Line
	10800 5150 10950 5150
Text Label 10950 5150 2    50   ~ 0
VCC
NoConn ~ 10000 5900
$Comp
L Connector:TestPoint TP?
U 1 1 5E014CFD
P 10800 5700
AR Path="/5B6AF6DC/5E014CFD" Ref="TP?"  Part="1" 
AR Path="/5DD74072/5E014CFD" Ref="TP?"  Part="1" 
AR Path="/5E014CFD" Ref="TP1"  Part="1" 
F 0 "TP1" H 10742 5727 50  0000 R CNN
F 1 "TP_INTB" H 10742 5818 50  0000 R CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 11000 5700 50  0001 C CNN
F 3 "~" H 11000 5700 50  0001 C CNN
	1    10800 5700
	-1   0    0    1   
$EndComp
Text Notes 6750 6300 0    20   ~ 0
SoundIC programming interface\n\n
$Comp
L Device:R R?
U 1 1 5E014D06
P 10800 5550
AR Path="/5B4D0E2A/5E014D06" Ref="R?"  Part="1" 
AR Path="/5B6AF6DC/5E014D06" Ref="R?"  Part="1" 
AR Path="/5DD74072/5E014D06" Ref="R?"  Part="1" 
AR Path="/5E014D06" Ref="R3"  Part="1" 
F 0 "R3" V 10880 5550 50  0000 C CNN
F 1 "10k" V 10800 5550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 10730 5550 50  0001 C CNN
F 3 "" H 10800 5550 50  0001 C CNN
F 4 "CF14JT10K0CT-ND" H 10800 5550 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/stackpole-electronics-inc/CF14JT10K0/CF14JT10K0CT-ND/1830374" H 10800 5550 50  0001 C CNN "DigikeyPL"
	1    10800 5550
	1    0    0    -1  
$EndComp
Connection ~ 10800 5700
$Comp
L Device:C_Small C?
U 1 1 5E014D0F
P 8250 5850
AR Path="/5B4D0E2A/5E014D0F" Ref="C?"  Part="1" 
AR Path="/5B6AF6DC/5E014D0F" Ref="C?"  Part="1" 
AR Path="/5DD74072/5E014D0F" Ref="C?"  Part="1" 
AR Path="/5E014D0F" Ref="C3"  Part="1" 
F 0 "C3" H 8275 5950 50  0000 L CNN
F 1 "100n" H 8275 5750 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.8mm_W2.6mm_P2.50mm" H 8288 5700 50  0001 C CNN
F 3 "" H 8250 5850 50  0001 C CNN
F 4 "BC5137-ND" H 8250 5850 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/vishay-bc-components/K104K10X7RF53L2/BC5137-ND/2820500" H 8250 5850 50  0001 C CNN "DigikeyPL"
	1    8250 5850
	1    0    0    -1  
$EndComp
Connection ~ 8250 5950
$Comp
L Device:C_Small C?
U 1 1 5E014D18
P 8200 5450
AR Path="/5B4D0E2A/5E014D18" Ref="C?"  Part="1" 
AR Path="/5B6AF6DC/5E014D18" Ref="C?"  Part="1" 
AR Path="/5DD74072/5E014D18" Ref="C?"  Part="1" 
AR Path="/5E014D18" Ref="C2"  Part="1" 
F 0 "C2" H 8225 5550 50  0000 L CNN
F 1 "100n" H 8225 5350 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.8mm_W2.6mm_P2.50mm" H 8238 5300 50  0001 C CNN
F 3 "" H 8200 5450 50  0001 C CNN
F 4 "BC5137-ND" H 8200 5450 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/vishay-bc-components/K104K10X7RF53L2/BC5137-ND/2820500" H 8200 5450 50  0001 C CNN "DigikeyPL"
	1    8200 5450
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper_NC_Dual JP_Prog?
U 1 1 5E073F9A
P 1700 4250
AR Path="/5B6AF6DC/5E073F9A" Ref="JP_Prog?"  Part="1" 
AR Path="/5DD74072/5E073F9A" Ref="JP_Prog?"  Part="1" 
AR Path="/5E073F9A" Ref="JP_Prog1"  Part="1" 
F 0 "JP_Prog1" V 1654 4351 50  0000 L CNN
F 1 "Jumper_Prog" V 1745 4351 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1700 4250 50  0001 C CNN
F 3 "~" H 1700 4250 50  0001 C CNN
F 4 "JUMPER 2,54 SW" H 1700 4250 50  0001 C CNN "ReicheltPN"
F 5 "https://www.reichelt.de/kurzschlussbruecke-schwarz-rm-2-54-jumper-2-54-sw-p9017.html?&trstct=pos_0" H 1700 4250 50  0001 C CNN "ReicheltPL"
	1    1700 4250
	0    -1   -1   0   
$EndComp
$Comp
L LasertagComponents:SN74AHC541 U?
U 1 1 5E073FA2
P 3050 3450
AR Path="/5B6AF6DC/5E073FA2" Ref="U?"  Part="1" 
AR Path="/5DD74072/5E073FA2" Ref="U?"  Part="1" 
AR Path="/5E073FA2" Ref="U3"  Part="1" 
F 0 "U3" H 2800 4000 50  0000 C CNN
F 1 "SN74AHC541" H 3300 4000 50  0000 C CNN
F 2 "Package_DIP:DIP-20_W7.62mm_LongPads" H 3050 3450 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74ahc541.pdf" H 3050 3450 50  0001 C CNN
F 4 "296-4610-5-ND" H 3050 3450 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/texas-instruments/SN74AHC541N/296-4610-5-ND/376228" H 3050 3450 50  0001 C CNN "DigikeyPL"
	1    3050 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 3100 1950 3100
Wire Wire Line
	1950 3200 2550 3200
Wire Wire Line
	1950 3300 2550 3300
Wire Wire Line
	1950 3500 2550 3500
Wire Wire Line
	1950 3600 2550 3600
Wire Wire Line
	1950 3800 2550 3800
Wire Wire Line
	3550 3100 4200 3100
Wire Wire Line
	4200 3200 3550 3200
Wire Wire Line
	4200 3300 3550 3300
Wire Wire Line
	4200 3400 3550 3400
Wire Wire Line
	3050 2750 3050 2500
Wire Wire Line
	2600 3950 2600 4050
Text Label 3050 2500 3    50   ~ 0
VCC
$Comp
L power:GND #PWR?
U 1 1 5E073FB7
P 3050 4350
AR Path="/5B6AF6DC/5E073FB7" Ref="#PWR?"  Part="1" 
AR Path="/5DD74072/5E073FB7" Ref="#PWR?"  Part="1" 
AR Path="/5E073FB7" Ref="#PWR08"  Part="1" 
F 0 "#PWR08" H 3050 4100 50  0001 C CNN
F 1 "GND" H 3055 4177 50  0000 C CNN
F 2 "" H 3050 4350 50  0001 C CNN
F 3 "" H 3050 4350 50  0001 C CNN
	1    3050 4350
	1    0    0    -1  
$EndComp
Text Label 4200 3100 2    50   ~ 0
ISD_SCLK
Text Label 4200 3200 2    50   ~ 0
ISD_SSB
Text Label 4200 3300 2    50   ~ 0
ISD_MOSI
Text Label 1950 3500 0    50   ~ 0
ISD_INTB
Text Label 1950 3600 0    50   ~ 0
ISD_BSYB
$Comp
L power:GND #PWR?
U 1 1 5E073FC3
P 1700 4500
AR Path="/5B6AF6DC/5E073FC3" Ref="#PWR?"  Part="1" 
AR Path="/5DD74072/5E073FC3" Ref="#PWR?"  Part="1" 
AR Path="/5E073FC3" Ref="#PWR02"  Part="1" 
F 0 "#PWR02" H 1700 4250 50  0001 C CNN
F 1 "GND" H 1705 4327 50  0000 C CNN
F 2 "" H 1700 4500 50  0001 C CNN
F 3 "" H 1700 4500 50  0001 C CNN
	1    1700 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 4000 1700 3800
Text Label 1700 3800 3    50   ~ 0
VCC
$Comp
L power:GND #PWR?
U 1 1 5E073FCC
P 1950 3800
AR Path="/5B6AF6DC/5E073FCC" Ref="#PWR?"  Part="1" 
AR Path="/5DD74072/5E073FCC" Ref="#PWR?"  Part="1" 
AR Path="/5E073FCC" Ref="#PWR04"  Part="1" 
F 0 "#PWR04" H 1950 3550 50  0001 C CNN
F 1 "GND" H 1955 3627 50  0000 C CNN
F 2 "" H 1950 3800 50  0001 C CNN
F 3 "" H 1950 3800 50  0001 C CNN
	1    1950 3800
	1    0    0    -1  
$EndComp
NoConn ~ 3550 3800
Text Notes 550  4100 0    20   ~ 0
Disconnect (HighImp) SPI wiring between ICs during programming
Text Notes 3650 3000 0    20   ~ 0
Up to 10ns propagation delay\n
$Comp
L Device:R R?
U 1 1 5E073FD7
P 1550 3450
AR Path="/5B4D0E2A/5E073FD7" Ref="R?"  Part="1" 
AR Path="/5B6AF6DC/5E073FD7" Ref="R?"  Part="1" 
AR Path="/5DD74072/5E073FD7" Ref="R?"  Part="1" 
AR Path="/5E073FD7" Ref="R1"  Part="1" 
F 0 "R1" V 1630 3450 50  0000 C CNN
F 1 "10k" V 1550 3450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1480 3450 50  0001 C CNN
F 3 "" H 1550 3450 50  0001 C CNN
F 4 "CF14JT10K0CT-ND" H 1550 3450 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/stackpole-electronics-inc/CF14JT10K0/CF14JT10K0CT-ND/1830374" H 1550 3450 50  0001 C CNN "DigikeyPL"
	1    1550 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 3700 1550 3700
Wire Wire Line
	1550 3600 1550 3700
Connection ~ 1550 3700
Wire Wire Line
	1550 3300 1550 3250
Wire Wire Line
	1550 3250 1350 3250
Text Label 1350 3250 0    50   ~ 0
VCC
Text Label 1950 3100 0    50   ~ 0
SPI_CLK
Text Label 1950 3200 0    50   ~ 0
SPI_CS_ISD
Text Label 1950 3300 0    50   ~ 0
SPI_MOSI
Wire Wire Line
	3550 3500 4200 3500
Wire Wire Line
	3550 3600 4200 3600
Text Label 4200 3600 2    50   ~ 0
ISD_BSYB_MCU
Text Label 4200 3500 2    50   ~ 0
ISD_INTB_MCU
Wire Wire Line
	1800 4250 2600 4250
Wire Wire Line
	2600 4250 2600 4050
Connection ~ 2600 4050
Wire Wire Line
	1050 3700 1550 3700
Text Label 1050 3700 0    50   ~ 0
DETC_TO_AVRMOSI
Text Label 4300 3700 2    50   ~ 0
AVRMOSI_FROM_DETC
Wire Wire Line
	3550 3700 4300 3700
$Comp
L LasertagComponents:ULN2003LV U?
U 1 1 5E0E647C
P 1900 7000
AR Path="/5B6AF6DC/5E0E647C" Ref="U?"  Part="1" 
AR Path="/5DD74072/5E0E647C" Ref="U?"  Part="1" 
AR Path="/5E0E647C" Ref="U1"  Part="1" 
F 0 "U1" H 1900 7667 50  0000 C CNN
F 1 "ULN2003LV" H 1900 7576 50  0000 C CNN
F 2 "LasertagFootprints:SOIC-16_3.9x9.9mm_P1.27mm_Hand" H 1950 6450 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/uln2003lv.pdf" H 2000 6800 50  0001 C CNN
F 4 "296-30403-1-ND" H 1900 7000 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/texas-instruments/ULN2003LVDR/296-30403-1-ND/3283552" H 1900 7000 50  0001 C CNN "DigikeyPL"
	1    1900 7000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E0E6482
P 2050 7700
AR Path="/5B6AF6DC/5E0E6482" Ref="#PWR?"  Part="1" 
AR Path="/5DD74072/5E0E6482" Ref="#PWR?"  Part="1" 
AR Path="/5E0E6482" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 2050 7450 50  0001 C CNN
F 1 "GND" H 2055 7527 50  0000 C CNN
F 2 "" H 2050 7700 50  0001 C CNN
F 3 "" H 2050 7700 50  0001 C CNN
	1    2050 7700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 6800 900  6800
Wire Wire Line
	900  6900 1500 6900
Wire Wire Line
	900  7000 1500 7000
Wire Wire Line
	900  7100 1500 7100
Wire Wire Line
	900  7200 1500 7200
Wire Wire Line
	900  7300 1500 7300
Text Label 1000 6800 0    50   ~ 0
MOTOR1_EN
Text Label 1000 6900 0    50   ~ 0
MOTOR2_EN
Text Label 1000 7000 0    50   ~ 0
MOTOR3_EN
Text Label 1000 7100 0    50   ~ 0
MOTOR4_EN
Text Label 1000 7200 0    50   ~ 0
MOTOR5_EN
Text Label 1000 7300 0    50   ~ 0
MOTOR6_EN
$Comp
L power:GND #PWR?
U 1 1 5E0E64A0
P 1350 7400
AR Path="/5B6AF6DC/5E0E64A0" Ref="#PWR?"  Part="1" 
AR Path="/5DD74072/5E0E64A0" Ref="#PWR?"  Part="1" 
AR Path="/5E0E64A0" Ref="#PWR01"  Part="1" 
F 0 "#PWR01" H 1350 7150 50  0001 C CNN
F 1 "GND" H 1355 7227 50  0000 C CNN
F 2 "" H 1350 7400 50  0001 C CNN
F 3 "" H 1350 7400 50  0001 C CNN
	1    1350 7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 7400 1500 7400
NoConn ~ 2300 7400
Text Notes 2500 7400 0    20   ~ 0
If enabled VOL arouind 0.4V (@100mA) here\n
Text Notes 2250 6700 0    20   ~ 0
Should be connected close to the motor connector\nto allow the internal inductive kickback protection to work\n
Wire Wire Line
	1900 7600 1900 7700
Wire Wire Line
	2850 6600 2300 6600
Text Label 2850 6600 2    50   ~ 0
VIB_VCC
Wire Wire Line
	3100 6450 3100 6800
Wire Wire Line
	2300 6800 3100 6800
Wire Wire Line
	3200 6550 3200 6900
Wire Wire Line
	2300 6900 3200 6900
Wire Wire Line
	3300 6650 3300 7000
Wire Wire Line
	2300 7000 3300 7000
Wire Wire Line
	3500 5750 4650 5750
Wire Wire Line
	3400 6750 3400 7100
Wire Wire Line
	2300 7100 3400 7100
Wire Wire Line
	2300 7200 3900 7200
Wire Wire Line
	2300 7300 3900 7300
Text Label 4300 5750 2    50   ~ 0
VIB_VCC
Wire Wire Line
	600  6100 1400 6100
Wire Wire Line
	600  5900 1400 5900
Text Label 600  5900 0    50   ~ 0
DETC_TO_AVRMOSI
Text Label 600  6100 0    50   ~ 0
AVRMOSI_FROM_DETC
$Comp
L Connector_Generic:Conn_01x06 J_Board2
U 1 1 5E228A68
P 10850 4000
F 0 "J_Board2" H 10930 3992 50  0000 L CNN
F 1 "Conn_01x06" H 10930 3901 50  0000 L CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x06_P2.00mm_Vertical" H 10850 4000 50  0001 C CNN
F 3 "~" H 10850 4000 50  0001 C CNN
F 4 "1849-1001-ND" H 10850 4000 50  0001 C CNN "DigikeyPN"
	1    10850 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	10150 3200 10500 3200
Wire Wire Line
	10500 3200 10500 3800
Wire Wire Line
	10500 3800 10650 3800
Wire Wire Line
	10650 3900 10300 3900
Wire Wire Line
	10300 3900 10300 3400
Wire Wire Line
	10300 3400 10150 3400
Text Label 3900 7200 2    50   ~ 0
VIB5
Text Label 3900 7300 2    50   ~ 0
VIB6
Wire Wire Line
	10300 4000 10650 4000
Text Label 10300 4000 0    50   ~ 0
5V
Text Label 10300 4100 0    50   ~ 0
5VGND
$Comp
L Connector_Generic:Conn_01x06 J_Board1
U 1 1 5E26DE5C
P 900 2650
F 0 "J_Board1" H 818 2125 50  0000 C CNN
F 1 "Conn_01x06" H 818 2216 50  0000 C CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x06_P2.00mm_Vertical" H 900 2650 50  0001 C CNN
F 3 "~" H 900 2650 50  0001 C CNN
F 4 "1849-1001-ND" H 900 2650 50  0001 C CNN "DigikeyPN"
	1    900  2650
	-1   0    0    1   
$EndComp
Wire Wire Line
	1100 2350 1600 2350
Wire Wire Line
	1100 2450 1600 2450
Wire Wire Line
	1100 2550 1600 2550
Wire Wire Line
	1100 2650 1600 2650
Text Label 1600 2350 2    50   ~ 0
VCC
Text Label 1600 2450 2    50   ~ 0
GND
Text Label 1600 2550 2    50   ~ 0
VIB_VCC
Wire Wire Line
	1100 2750 1600 2750
Text Label 1600 2650 2    50   ~ 0
VIB5
Text Label 1600 2750 2    50   ~ 0
VIB6
Wire Wire Line
	8350 3900 7950 3900
Wire Wire Line
	8350 4000 7950 4000
Wire Wire Line
	8350 4200 7950 4200
Wire Wire Line
	8350 4300 7950 4300
Wire Wire Line
	8350 4400 7950 4400
Wire Wire Line
	8350 4500 7950 4500
Wire Wire Line
	8350 4600 7950 4600
Wire Wire Line
	7950 4700 8350 4700
Text Label 7950 3900 0    50   ~ 0
VCC
Text Label 7950 4000 0    50   ~ 0
5V
Text Label 7950 4700 0    50   ~ 0
GND
Text Label 7950 4200 0    50   ~ 0
I2C_SDA
Text Label 7950 4300 0    50   ~ 0
I2C_SCL
Text Label 7950 4400 0    50   ~ 0
BTN1
Text Label 7950 4500 0    50   ~ 0
BTN2
Text Label 7950 4600 0    50   ~ 0
BTN3
Wire Wire Line
	5950 3500 6650 3500
Text Label 6650 1500 2    50   ~ 0
BTN1
Text Label 6650 3500 2    50   ~ 0
BTN3
Wire Wire Line
	5950 1900 6650 1900
Text Label 6650 1900 2    50   ~ 0
BTN2
Text Label 10300 4200 0    50   ~ 0
VBAT
Wire Wire Line
	1150 1550 800  1550
Wire Wire Line
	800  1050 1150 1050
Text Label 800  1550 0    50   ~ 0
5VGND
Text Label 800  1050 0    50   ~ 0
VBAT
$Comp
L Connector_Generic:Conn_01x10 J_HID1
U 1 1 5E3FD935
P 8550 4300
F 0 "J_HID1" H 8630 4292 50  0000 L CNN
F 1 "Conn_01x10" H 8630 4201 50  0000 L CNN
F 2 "Connector_IDC:IDC-Header_2x05_P2.54mm_Vertical" H 8550 4300 50  0001 C CNN
F 3 "~" H 8550 4300 50  0001 C CNN
F 4 "AE11232-ND" H 8550 4300 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/assmann-wsw-components/AWHW-10G-0202-T/AE11232-ND/5224930" H 8550 4300 50  0001 C CNN "DigikeyPL"
	1    8550 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 4800 8350 4800
Text Label 7950 4800 0    50   ~ 0
GND
Wire Wire Line
	8350 4100 7950 4100
Text Label 7950 4100 0    50   ~ 0
5V
Wire Wire Line
	5950 1500 6650 1500
Text Label 6650 3600 2    50   ~ 0
RF_MB_Switch
$Comp
L Switch:SW_SPDT SW_RF1
U 1 1 5E44C8F5
P 7000 3600
F 0 "SW_RF1" H 7000 3885 50  0000 C CNN
F 1 "SW_SPDT" H 7000 3794 50  0000 C CNN
F 2 "Button_Switch_THT:SW_Slide_1P2T_CK_OS102011MS2Q" H 7000 3600 50  0001 C CNN
F 3 "~" H 7000 3600 50  0001 C CNN
F 4 "A107673-ND" H 7000 3600 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/te-connectivity-alcoswitch-switches/1825232-1/A107673-ND/4021554" H 7000 3600 50  0001 C CNN "DigikeyPL"
	1    7000 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 3500 7500 3500
Wire Wire Line
	7200 3700 7500 3700
Wire Wire Line
	5950 3600 6800 3600
Text Label 7500 3500 2    50   ~ 0
VCC
Text Label 7500 3700 2    50   ~ 0
GND
$Comp
L Connector:TestPoint TP_D1
U 1 1 5DD76C8B
P 6950 2000
F 0 "TP_D1" H 7008 2118 50  0000 L CNN
F 1 "TestPoint" H 7008 2027 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D2.0mm" H 7150 2000 50  0001 C CNN
F 3 "~" H 7150 2000 50  0001 C CNN
	1    6950 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 2000 6950 2000
$Comp
L Connector:TestPoint TP_IRQ1
U 1 1 5DD87BE3
P 8250 2400
F 0 "TP_IRQ1" H 8308 2518 50  0000 L CNN
F 1 "TestPoint" H 8308 2427 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D2.0mm" H 8450 2400 50  0001 C CNN
F 3 "~" H 8450 2400 50  0001 C CNN
	1    8250 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 2400 9200 2400
$Comp
L power:GND #PWR?
U 1 1 5DD29208
P 1850 3400
AR Path="/5B6AF6DC/5DD29208" Ref="#PWR?"  Part="1" 
AR Path="/5DD74072/5DD29208" Ref="#PWR?"  Part="1" 
AR Path="/5DD29208" Ref="#PWR0101"  Part="1" 
F 0 "#PWR0101" H 1850 3150 50  0001 C CNN
F 1 "GND" H 1855 3227 50  0000 C CNN
F 2 "" H 1850 3400 50  0001 C CNN
F 3 "" H 1850 3400 50  0001 C CNN
	1    1850 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 3400 2550 3400
NoConn ~ 4200 3400
Wire Wire Line
	2050 7700 1900 7700
$Comp
L power:GNDPWR #PWR0102
U 1 1 5DD8CE29
P 800 1600
F 0 "#PWR0102" H 800 1400 50  0001 C CNN
F 1 "GNDPWR" H 804 1446 50  0000 C CNN
F 2 "" H 800 1550 50  0001 C CNN
F 3 "" H 800 1550 50  0001 C CNN
	1    800  1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	800  1600 800  1550
Text Label 2850 1350 2    50   ~ 0
SUPPLY_MONITOR
Wire Wire Line
	10000 4100 10650 4100
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5DDC906F
P 10000 4100
F 0 "#FLG0101" H 10000 4175 50  0001 C CNN
F 1 "PWR_FLAG" H 10000 4273 50  0000 C CNN
F 2 "" H 10000 4100 50  0001 C CNN
F 3 "~" H 10000 4100 50  0001 C CNN
	1    10000 4100
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5DDC93EF
P 9800 4200
F 0 "#FLG0102" H 9800 4275 50  0001 C CNN
F 1 "PWR_FLAG" H 9800 4373 50  0000 C CNN
F 2 "" H 9800 4200 50  0001 C CNN
F 3 "~" H 9800 4200 50  0001 C CNN
	1    9800 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9800 4200 10500 4200
Wire Wire Line
	10650 4300 10500 4300
Wire Wire Line
	10500 4300 10500 4200
Connection ~ 10500 4200
Wire Wire Line
	10500 4200 10650 4200
Wire Wire Line
	1100 2850 1600 2850
Text Label 1600 2850 2    50   ~ 0
5VGND
$Comp
L Device:C_Small C?
U 1 1 5DD91405
P 3250 1050
AR Path="/5B4D0E2A/5DD91405" Ref="C?"  Part="1" 
AR Path="/5B6AF6DC/5DD91405" Ref="C?"  Part="1" 
AR Path="/5DD74072/5DD91405" Ref="C?"  Part="1" 
AR Path="/5DD91405" Ref="C12"  Part="1" 
F 0 "C12" H 3275 1150 50  0000 L CNN
F 1 "100n" H 3275 950 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.8mm_W2.6mm_P2.50mm" H 3288 900 50  0001 C CNN
F 3 "" H 3250 1050 50  0001 C CNN
F 4 "BC5137-ND" H 3250 1050 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/vishay-bc-components/K104K10X7RF53L2/BC5137-ND/2820500" H 3250 1050 50  0001 C CNN "DigikeyPL"
	1    3250 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 1150 3250 1350
Connection ~ 3250 1350
Wire Wire Line
	3250 950  3250 900 
Wire Wire Line
	3250 900  3600 900 
Connection ~ 3600 900 
$EndSCHEMATC
