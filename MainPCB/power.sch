EESchema Schematic File Version 4
LIBS:MainPCB-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 4150 5300 0    60   ~ 0
5V
Text Label 6250 6400 0    60   ~ 0
GND
$Comp
L power:GND #PWR?
U 1 1 5DEEED09
P 5200 6400
AR Path="/5DEEED09" Ref="#PWR?"  Part="1" 
AR Path="/5DEDF112/5DEEED09" Ref="#PWR030"  Part="1" 
F 0 "#PWR030" H 5200 6150 50  0001 C CNN
F 1 "GND" H 5200 6250 50  0000 C CNN
F 2 "" H 5200 6400 50  0001 C CNN
F 3 "" H 5200 6400 50  0001 C CNN
	1    5200 6400
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR?
U 1 1 5DEEED0F
P 3200 4200
AR Path="/5DEEED0F" Ref="#PWR?"  Part="1" 
AR Path="/5DEDF112/5DEEED0F" Ref="#PWR029"  Part="1" 
F 0 "#PWR029" H 3200 4000 50  0001 C CNN
F 1 "GNDPWR" H 3200 4070 50  0000 C CNN
F 2 "" H 3200 4150 50  0001 C CNN
F 3 "" H 3200 4150 50  0001 C CNN
	1    3200 4200
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5DEEED15
P 6300 3450
AR Path="/5DEEED15" Ref="#PWR?"  Part="1" 
AR Path="/5DEDF112/5DEEED15" Ref="#PWR031"  Part="1" 
F 0 "#PWR031" H 6300 3300 50  0001 C CNN
F 1 "+5V" H 6300 3590 50  0000 C CNN
F 2 "" H 6300 3450 50  0001 C CNN
F 3 "" H 6300 3450 50  0001 C CNN
	1    6300 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 5300 5850 5300
$Comp
L Device:CP C?
U 1 1 5DEEED37
P 5850 5450
AR Path="/5DEEED37" Ref="C?"  Part="1" 
AR Path="/5DEDF112/5DEEED37" Ref="C8"  Part="1" 
F 0 "C8" H 5968 5496 50  0000 L CNN
F 1 "6800u" H 5968 5405 50  0000 L CNN
F 2 "LasertagFootprints:CP_Bended_D12.5mm_P5.00mm" H 5888 5300 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Panasonic%20Electronic%20Components/ECA-xxM%20Series,TypeA.pdf" H 5850 5450 50  0001 C CNN
F 4 "P5119-ND" H 5850 5450 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/panasonic-electronic-components/ECA-0JM682/P5119-ND/244978" H 5850 5450 50  0001 C CNN "DigikeyPL"
	1    5850 5450
	1    0    0    -1  
$EndComp
Connection ~ 5850 5300
$Comp
L Device:R R?
U 1 1 5DEEED44
P 4300 5450
AR Path="/5DEEED44" Ref="R?"  Part="1" 
AR Path="/5DEDF112/5DEEED44" Ref="R12"  Part="1" 
F 0 "R12" H 4370 5496 50  0000 L CNN
F 1 "22k" H 4370 5405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4230 5450 50  0001 C CNN
F 3 "~" H 4300 5450 50  0001 C CNN
F 4 "CF14JT22K0CT-ND" H 4300 5450 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/products/de?keywords=%20CF14JT22K0CT-ND%20" H 4300 5450 50  0001 C CNN "DigikeyPL"
	1    4300 5450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DEEED4E
P 4300 5850
AR Path="/5DEEED4E" Ref="R?"  Part="1" 
AR Path="/5DEDF112/5DEEED4E" Ref="R9"  Part="1" 
F 0 "R9" H 4370 5896 50  0000 L CNN
F 1 "33k" H 4370 5805 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4230 5850 50  0001 C CNN
F 3 "~" H 4300 5850 50  0001 C CNN
F 4 "CF14JT33K0CT-ND" H 4300 5850 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/stackpole-electronics-inc/CF14JT33K0/CF14JT33K0CT-ND/1830387" H 4300 5850 50  0001 C CNN "DigikeyPL"
	1    4300 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 5700 4300 5600
$Comp
L Connector:TestPoint TP?
U 1 1 5DEEED5C
P 5600 6400
AR Path="/5DEEED5C" Ref="TP?"  Part="1" 
AR Path="/5DEDF112/5DEEED5C" Ref="TP_GND2"  Part="1" 
F 0 "TP_GND2" H 5658 6520 50  0000 L CNN
F 1 "TP_GND" H 5658 6429 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_4.0x4.0mm" H 5800 6400 50  0001 C CNN
F 3 "~" H 5800 6400 50  0001 C CNN
	1    5600 6400
	1    0    0    -1  
$EndComp
$Comp
L Device:Net-Tie_2 NT?
U 1 1 5DEEED64
P 5950 6200
AR Path="/5DEEED64" Ref="NT?"  Part="1" 
AR Path="/5DEDF112/5DEEED64" Ref="NT1"  Part="1" 
F 0 "NT1" V 5904 6241 50  0000 L CNN
F 1 "Net-Tie_2" V 5995 6241 50  0000 L CNN
F 2 "NetTie:NetTie-2_SMD_Pad2.0mm" H 5950 6200 50  0001 C CNN
F 3 "~" H 5950 6200 50  0001 C CNN
	1    5950 6200
	0    1    1    0   
$EndComp
Text Notes 4950 6150 0    20   ~ 0
Place at board edge (at opposite sides) for oszi
Text Label 3100 3550 0    47   ~ 0
GND5V
Text Label 6150 3450 0    47   ~ 0
5V
$Comp
L power:+BATT #PWR?
U 1 1 5DEEED8B
P 3100 3450
AR Path="/5DEEED8B" Ref="#PWR?"  Part="1" 
AR Path="/5DEDF112/5DEEED8B" Ref="#PWR028"  Part="1" 
F 0 "#PWR028" H 3100 3300 50  0001 C CNN
F 1 "+BATT" H 3115 3623 50  0000 C CNN
F 2 "" H 3100 3450 50  0001 C CNN
F 3 "" H 3100 3450 50  0001 C CNN
	1    3100 3450
	1    0    0    -1  
$EndComp
Connection ~ 3100 3450
Wire Wire Line
	4300 5300 5300 5300
$Comp
L Device:D_Schottky D?
U 1 1 5DEEED96
P 5450 5300
AR Path="/5DEEED96" Ref="D?"  Part="1" 
AR Path="/5DEDF112/5DEEED96" Ref="D4"  Part="1" 
F 0 "D4" H 5450 5084 50  0000 C CNN
F 1 "LSM115J" H 5450 5175 50  0000 C CNN
F 2 "Diode_SMD:D_SMA_Handsoldering" H 5450 5300 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Microsemi%20PDFs/LSM115J.pdf" H 5450 5300 50  0001 C CNN
F 4 " LSM115JE3/TR13CT-ND " H 5450 5300 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/microsemi-corporation/LSM115JE3-TR13/LSM115JE3-TR13CT-ND/1634547" H 5450 5300 50  0001 C CNN "DigikeyPL"
	1    5450 5300
	-1   0    0    1   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 5DEEED9F
P 3250 3450
AR Path="/5DEEED9F" Ref="TP?"  Part="1" 
AR Path="/5DEDF112/5DEEED9F" Ref="TP_Bat1"  Part="1" 
F 0 "TP_Bat1" H 3308 3570 50  0000 L CNN
F 1 "TP_Batt" H 3308 3479 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 3450 3450 50  0001 C CNN
F 3 "~" H 3450 3450 50  0001 C CNN
	1    3250 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 3450 3100 3450
$Comp
L Connector:TestPoint TP?
U 1 1 5DEEEDA6
P 5750 3450
AR Path="/5DEEEDA6" Ref="TP?"  Part="1" 
AR Path="/5DEDF112/5DEEEDA6" Ref="TP_5V1"  Part="1" 
F 0 "TP_5V1" H 5808 3570 50  0000 L CNN
F 1 "TP_5V" H 5808 3479 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 5950 3450 50  0001 C CNN
F 3 "~" H 5950 3450 50  0001 C CNN
	1    5750 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 3450 6050 3450
Text Notes 5400 5250 0    20   ~ 0
V_f 0.15V @0.1A,25°C\n
Wire Wire Line
	3250 3450 3600 3450
Connection ~ 3250 3450
$Comp
L Device:C C?
U 1 1 5DEEEDCB
P 3600 3750
AR Path="/5DEEEDCB" Ref="C?"  Part="1" 
AR Path="/5DEDF112/5DEEEDCB" Ref="C6"  Part="1" 
F 0 "C6" H 3715 3796 50  0000 L CNN
F 1 "47u X7R" H 3715 3705 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3638 3600 50  0001 C CNN
F 3 "~" H 3600 3750 50  0001 C CNN
F 4 "587-3400-1-ND" H 3600 3750 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/taiyo-yuden/JMK316ABJ476ML-T/587-3400-1-ND/4157513" H 3600 3750 50  0001 C CNN "DigikeyPL"
	1    3600 3750
	1    0    0    -1  
$EndComp
Connection ~ 3600 3450
Wire Wire Line
	6050 3550 6050 3450
Connection ~ 6050 3450
Wire Wire Line
	6050 3450 6300 3450
Wire Wire Line
	6050 3850 5750 3850
Wire Wire Line
	3600 3600 3600 3450
Wire Wire Line
	3600 3900 3600 4100
Wire Wire Line
	3600 4100 3200 4100
Wire Wire Line
	3200 4100 3200 4200
Wire Wire Line
	3200 3550 3200 4100
Connection ~ 3200 4100
Text Notes 4850 3200 0    50   ~ 0
Limit current to 500mA.
Text HLabel 6600 3450 2    50   Output ~ 0
5V
Text HLabel 8700 5250 2    50   Output ~ 0
3V
Text HLabel 3950 5800 0    50   Output ~ 0
SupplyMonitor
Text HLabel 1400 3450 0    50   Input ~ 0
VBAT
Text HLabel 1400 3550 0    50   BiDi ~ 0
5VGND
Wire Wire Line
	1400 3550 3200 3550
Wire Wire Line
	1400 3450 3100 3450
Wire Wire Line
	6300 3450 6600 3450
Connection ~ 6300 3450
Wire Wire Line
	3950 5800 4050 5800
Wire Wire Line
	4050 5800 4050 5700
Wire Wire Line
	4050 5700 4300 5700
Text HLabel 7050 6400 2    50   Output ~ 0
GND
Wire Wire Line
	4150 5300 4300 5300
Connection ~ 4300 5300
Wire Wire Line
	8700 5250 8600 5250
$Comp
L power:+3.3V #PWR032
U 1 1 5DF0AA1E
P 8600 5250
F 0 "#PWR032" H 8600 5100 50  0001 C CNN
F 1 "+3.3V" H 8615 5423 50  0000 C CNN
F 2 "" H 8600 5250 50  0001 C CNN
F 3 "" H 8600 5250 50  0001 C CNN
	1    8600 5250
	1    0    0    -1  
$EndComp
Connection ~ 8600 5250
Wire Wire Line
	8600 5250 8550 5250
Wire Wire Line
	5850 5300 7250 5300
Wire Wire Line
	7250 5300 7250 5200
Wire Wire Line
	7250 5200 7400 5200
Wire Wire Line
	8000 5200 8150 5200
Wire Wire Line
	8550 5200 8550 5250
$Comp
L Device:C C?
U 1 1 5E3D5E04
P 6050 3700
AR Path="/5E3D5E04" Ref="C?"  Part="1" 
AR Path="/5DEDF112/5E3D5E04" Ref="C9"  Part="1" 
F 0 "C9" H 6165 3746 50  0000 L CNN
F 1 "47u X7R" H 6165 3655 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6088 3550 50  0001 C CNN
F 3 "~" H 6050 3700 50  0001 C CNN
F 4 "587-3400-1-ND" H 6050 3700 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/taiyo-yuden/JMK316ABJ476ML-T/587-3400-1-ND/4157513" H 6050 3700 50  0001 C CNN "DigikeyPL"
	1    6050 3700
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 5DD4D49E
P 8150 5200
AR Path="/5DD4D49E" Ref="TP?"  Part="1" 
AR Path="/5DEDF112/5DD4D49E" Ref="TP_3V1"  Part="1" 
F 0 "TP_3V1" H 8208 5320 50  0000 L CNN
F 1 "TP_3V" H 8208 5229 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 8350 5200 50  0001 C CNN
F 3 "~" H 8350 5200 50  0001 C CNN
	1    8150 5200
	1    0    0    -1  
$EndComp
Connection ~ 8150 5200
Wire Wire Line
	8150 5200 8200 5200
Wire Wire Line
	5950 6100 5950 6050
Connection ~ 5950 6050
Wire Wire Line
	5950 6050 7700 6050
Wire Wire Line
	5950 6300 5950 6400
Connection ~ 5950 6400
Wire Wire Line
	5950 6400 6600 6400
Text Label 6150 6050 0    50   ~ 0
GND5V
Wire Wire Line
	5850 5600 5850 6050
Connection ~ 5850 6050
Wire Wire Line
	5850 6050 5950 6050
Connection ~ 7700 6050
Wire Wire Line
	7700 6050 8200 6050
Connection ~ 4300 5700
Wire Wire Line
	4300 6000 4300 6050
Wire Wire Line
	4300 6050 5850 6050
$Comp
L Connector:TestPoint TP?
U 1 1 5DD6BC55
P 6600 6400
AR Path="/5DD6BC55" Ref="TP?"  Part="1" 
AR Path="/5DEDF112/5DD6BC55" Ref="TP_GND3"  Part="1" 
F 0 "TP_GND3" H 6658 6520 50  0000 L CNN
F 1 "TP_GND" H 6658 6429 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 6800 6400 50  0001 C CNN
F 3 "~" H 6800 6400 50  0001 C CNN
	1    6600 6400
	1    0    0    -1  
$EndComp
Connection ~ 6600 6400
Wire Wire Line
	6600 6400 7050 6400
$Comp
L Connector:TestPoint TP?
U 1 1 5DD71140
P 5200 6400
AR Path="/5DD71140" Ref="TP?"  Part="1" 
AR Path="/5DEDF112/5DD71140" Ref="TP_GND1"  Part="1" 
F 0 "TP_GND1" H 5258 6520 50  0000 L CNN
F 1 "TP_GND" H 5258 6429 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_4.0x4.0mm" H 5400 6400 50  0001 C CNN
F 3 "~" H 5400 6400 50  0001 C CNN
	1    5200 6400
	1    0    0    -1  
$EndComp
Connection ~ 5200 6400
Connection ~ 5600 6400
Wire Wire Line
	5600 6400 5950 6400
Wire Wire Line
	5200 6400 5600 6400
$Comp
L Regulator_Linear:MCP1825S U9
U 1 1 5DD4331B
P 7700 5200
F 0 "U9" H 7700 5442 50  0000 C CNN
F 1 "MCP1825S" H 7700 5351 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223" H 7600 5350 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/devicedoc/22056b.pdf" H 7700 5450 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/microchip-technology/MCP1825ST-3302E-DB/MCP1825ST-3302E-DBCT-ND/5013522" H 7700 5200 50  0001 C CNN "DigikeyPL"
F 5 "MCP1825ST-3302E/DBCT-ND" H 7700 5200 50  0001 C CNN "DigikeyPN"
	1    7700 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 5500 7700 6050
$Comp
L Device:C C?
U 1 1 5DD718A3
P 8200 5650
AR Path="/5DD718A3" Ref="C?"  Part="1" 
AR Path="/5DEDF112/5DD718A3" Ref="C11"  Part="1" 
F 0 "C11" H 8315 5696 50  0000 L CNN
F 1 "10u X7R" H 8315 5605 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 8238 5500 50  0001 C CNN
F 3 "~" H 8200 5650 50  0001 C CNN
F 4 "399-3525-1-ND" H 8200 5650 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/kemet/C1206C106K4RACTU/399-3525-1-ND/789665" H 8200 5650 50  0001 C CNN "DigikeyPL"
	1    8200 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 5500 8200 5200
Connection ~ 8200 5200
Wire Wire Line
	8200 5200 8550 5200
Wire Wire Line
	8200 5800 8200 6050
Connection ~ 8200 6050
Wire Wire Line
	8200 6050 8700 6050
$Comp
L LasertagComponents:NCP380LSN05 U2
U 1 1 5DD7C5E4
P 5300 3750
F 0 "U2" H 5300 4315 50  0000 C CNN
F 1 "NCP380LSN05" H 5300 4224 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TSOT-23-5_HandSoldering" H 5300 3750 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/NCP380-D.PDF" H 5300 3750 50  0001 C CNN
F 4 "NCP380LSN05AAT1GOSCT-ND" H 5300 3750 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/on-semiconductor/NCP380LSN05AAT1G/NCP380LSN05AAT1GOSCT-ND/2683271" H 5300 3750 50  0001 C CNN "DigikeyPL"
	1    5300 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 3450 5750 3450
Connection ~ 5750 3450
Wire Wire Line
	3600 4100 3600 4250
Wire Wire Line
	3600 4250 4850 4250
Connection ~ 3600 4100
Wire Wire Line
	5300 4250 5750 4250
Wire Wire Line
	5750 3850 5750 4250
Connection ~ 5300 4250
Wire Wire Line
	3600 3450 4900 3450
Wire Wire Line
	4900 4000 4850 4000
Wire Wire Line
	4850 4000 4850 4250
Connection ~ 4850 4250
Wire Wire Line
	4850 4250 5300 4250
$Comp
L Connector:TestPoint TP?
U 1 1 5DD88B66
P 4500 3850
AR Path="/5DD88B66" Ref="TP?"  Part="1" 
AR Path="/5DEDF112/5DD88B66" Ref="TP_FLT1"  Part="1" 
F 0 "TP_FLT1" H 4558 3970 50  0000 L CNN
F 1 "TP_FAULT" H 4558 3879 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 4700 3850 50  0001 C CNN
F 3 "~" H 4700 3850 50  0001 C CNN
	1    4500 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 3850 4900 3850
$EndSCHEMATC
