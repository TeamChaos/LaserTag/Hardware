EESchema Schematic File Version 4
LIBS:HidPCB-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R?
U 1 1 5DED8B7A
P 9000 3450
AR Path="/5B6AF6DC/5DED8B7A" Ref="R?"  Part="1" 
AR Path="/5DED8B7A" Ref="R4"  Part="1" 
AR Path="/5DEC99EE/5DED8B7A" Ref="R?"  Part="1" 
F 0 "R4" V 8793 3450 50  0000 C CNN
F 1 "330" V 8884 3450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8930 3450 50  0001 C CNN
F 3 "~" H 9000 3450 50  0001 C CNN
F 4 "CF14JT330RCT-ND" H 9000 3450 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/stackpole-electronics-inc/CF14JT330R/CF14JT330RCT-ND/1830338" H 9000 3450 50  0001 C CNN "DigikeyPL"
	1    9000 3450
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D?
U 1 1 5DED8B80
P 9000 3100
AR Path="/5B6AF6DC/5DED8B80" Ref="D?"  Part="1" 
AR Path="/5DED8B80" Ref="D2"  Part="1" 
AR Path="/5DEC99EE/5DED8B80" Ref="D?"  Part="1" 
F 0 "D2" H 8992 2845 50  0000 C CNN
F 1 "LED_Status1" H 8992 2936 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 9000 3100 50  0001 C CNN
F 3 "~" H 9000 3100 50  0001 C CNN
F 4 "LED 5MM GN" H 9000 3100 50  0001 C CNN "ReicheltPN"
F 5 "https://www.reichelt.de/led-5-mm-bedrahtet-gruen-18-mcd-60-led-5mm-gn-p10232.html?&trstct=pol_1" H 9000 3100 50  0001 C CNN "ReicheltPL"
	1    9000 3100
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D?
U 1 1 5DED8B86
P 9350 3100
AR Path="/5B6AF6DC/5DED8B86" Ref="D?"  Part="1" 
AR Path="/5DED8B86" Ref="D3"  Part="1" 
AR Path="/5DEC99EE/5DED8B86" Ref="D?"  Part="1" 
F 0 "D3" H 9342 2845 50  0000 C CNN
F 1 "LED_Status2" H 9342 2936 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 9350 3100 50  0001 C CNN
F 3 "~" H 9350 3100 50  0001 C CNN
F 4 "LED 5MM RT" H 9350 3100 50  0001 C CNN "ReicheltPN"
F 5 "https://www.reichelt.de/led-5-mm-bedrahtet-rot-4-5-mcd-60-led-5mm-rt-p10233.html?r=1" H 9350 3100 50  0001 C CNN "ReicheltPL"
	1    9350 3100
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED_RAGB D?
U 1 1 5DED8B94
P 8200 3050
AR Path="/5B6AF6DC/5DED8B94" Ref="D?"  Part="1" 
AR Path="/5DED8B94" Ref="D1"  Part="1" 
AR Path="/5DEC99EE/5DED8B94" Ref="D?"  Part="1" 
F 0 "D1" V 8246 2720 50  0000 R CNN
F 1 "LED_RAGB" V 8155 2720 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm-4_RGB_Staggered_Pins" H 8200 3000 50  0001 C CNN
F 3 "~" H 8200 3000 50  0001 C CNN
	1    8200 3050
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5DED8B9C
P 8000 3450
AR Path="/5B6AF6DC/5DED8B9C" Ref="R?"  Part="1" 
AR Path="/5DED8B9C" Ref="R1"  Part="1" 
AR Path="/5DEC99EE/5DED8B9C" Ref="R?"  Part="1" 
F 0 "R1" H 8070 3496 50  0000 L CNN
F 1 "120" H 8070 3405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7930 3450 50  0001 C CNN
F 3 "~" H 8000 3450 50  0001 C CNN
F 4 "CF14JT120RCT-ND" H 8000 3450 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/stackpole-electronics-inc/CF14JT120R/CF14JT120RCT-ND/1830329" H 8000 3450 50  0001 C CNN "DigikeyPL"
	1    8000 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DED8BA4
P 8400 3450
AR Path="/5B6AF6DC/5DED8BA4" Ref="R?"  Part="1" 
AR Path="/5DED8BA4" Ref="R3"  Part="1" 
AR Path="/5DEC99EE/5DED8BA4" Ref="R?"  Part="1" 
F 0 "R3" H 8470 3496 50  0000 L CNN
F 1 "120" H 8470 3405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8330 3450 50  0001 C CNN
F 3 "~" H 8400 3450 50  0001 C CNN
F 4 "82QBK-ND" H 8400 3450 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/yageo/CFR-25JB-52-82R/82QBK-ND/3609" H 8400 3450 50  0001 C CNN "DigikeyPL"
	1    8400 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 3300 8000 3250
Wire Wire Line
	8200 3300 8200 3250
Wire Wire Line
	8400 3300 8400 3250
Wire Wire Line
	8000 3600 8000 3650
Wire Wire Line
	8200 3600 8200 3750
Wire Wire Line
	8400 3600 8400 3850
Wire Wire Line
	9000 3250 9000 3300
Wire Wire Line
	9350 3300 9350 3250
Wire Wire Line
	8000 3650 7750 3650
Wire Wire Line
	8200 3750 7750 3750
Wire Wire Line
	8400 3850 7750 3850
Wire Wire Line
	9000 3600 9000 3950
Wire Wire Line
	9000 3950 7750 3950
Wire Wire Line
	9350 3600 9350 4050
Wire Wire Line
	9350 4050 7750 4050
Text Notes 8900 3050 0    50   ~ 0
Green\n
Text Notes 9300 3050 0    50   ~ 0
Red
$Comp
L power:GND #PWR?
U 1 1 5DED8BBD
P 5950 3750
AR Path="/5B6AF6DC/5DED8BBD" Ref="#PWR?"  Part="1" 
AR Path="/5DED8BBD" Ref="#PWR05"  Part="1" 
AR Path="/5DEC99EE/5DED8BBD" Ref="#PWR?"  Part="1" 
F 0 "#PWR05" H 5950 3500 50  0001 C CNN
F 1 "GND" H 5955 3577 50  0000 C CNN
F 2 "" H 5950 3750 50  0001 C CNN
F 3 "" H 5950 3750 50  0001 C CNN
	1    5950 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 3750 5950 3750
Wire Wire Line
	6450 3650 5950 3650
Text Label 6200 3650 0    50   ~ 0
VCC
$Comp
L power:+3.3V #PWR?
U 1 1 5DED8BC6
P 5950 3650
AR Path="/5B6AF6DC/5DED8BC6" Ref="#PWR?"  Part="1" 
AR Path="/5DED8BC6" Ref="#PWR04"  Part="1" 
AR Path="/5DEC99EE/5DED8BC6" Ref="#PWR?"  Part="1" 
F 0 "#PWR04" H 5950 3500 50  0001 C CNN
F 1 "+3.3V" H 5965 3823 50  0000 C CNN
F 2 "" H 5950 3650 50  0001 C CNN
F 3 "" H 5950 3650 50  0001 C CNN
	1    5950 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 4900 6800 5150
Wire Wire Line
	6900 4900 6900 5150
Wire Wire Line
	7000 4900 7000 5150
Wire Wire Line
	7100 4900 7100 5150
Wire Wire Line
	7200 4900 7200 5150
Wire Wire Line
	7300 4900 7300 5150
Wire Wire Line
	7400 4900 7400 5150
$Comp
L power:GND #PWR?
U 1 1 5DED8BD3
P 6800 5150
AR Path="/5B6AF6DC/5DED8BD3" Ref="#PWR?"  Part="1" 
AR Path="/5DED8BD3" Ref="#PWR07"  Part="1" 
AR Path="/5DEC99EE/5DED8BD3" Ref="#PWR?"  Part="1" 
F 0 "#PWR07" H 6800 4900 50  0001 C CNN
F 1 "GND" H 6805 4977 50  0000 C CNN
F 2 "" H 6800 5150 50  0001 C CNN
F 3 "" H 6800 5150 50  0001 C CNN
	1    6800 5150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DED8BD9
P 6900 5150
AR Path="/5B6AF6DC/5DED8BD9" Ref="#PWR?"  Part="1" 
AR Path="/5DED8BD9" Ref="#PWR08"  Part="1" 
AR Path="/5DEC99EE/5DED8BD9" Ref="#PWR?"  Part="1" 
F 0 "#PWR08" H 6900 4900 50  0001 C CNN
F 1 "GND" H 6905 4977 50  0000 C CNN
F 2 "" H 6900 5150 50  0001 C CNN
F 3 "" H 6900 5150 50  0001 C CNN
	1    6900 5150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DED8BDF
P 7100 5150
AR Path="/5B6AF6DC/5DED8BDF" Ref="#PWR?"  Part="1" 
AR Path="/5DED8BDF" Ref="#PWR09"  Part="1" 
AR Path="/5DEC99EE/5DED8BDF" Ref="#PWR?"  Part="1" 
F 0 "#PWR09" H 7100 4900 50  0001 C CNN
F 1 "GND" H 7105 4977 50  0000 C CNN
F 2 "" H 7100 5150 50  0001 C CNN
F 3 "" H 7100 5150 50  0001 C CNN
	1    7100 5150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DED8BE5
P 7300 5150
AR Path="/5B6AF6DC/5DED8BE5" Ref="#PWR?"  Part="1" 
AR Path="/5DED8BE5" Ref="#PWR010"  Part="1" 
AR Path="/5DEC99EE/5DED8BE5" Ref="#PWR?"  Part="1" 
F 0 "#PWR010" H 7300 4900 50  0001 C CNN
F 1 "GND" H 7305 4977 50  0000 C CNN
F 2 "" H 7300 5150 50  0001 C CNN
F 3 "" H 7300 5150 50  0001 C CNN
	1    7300 5150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DED8BEB
P 7400 5150
AR Path="/5B6AF6DC/5DED8BEB" Ref="#PWR?"  Part="1" 
AR Path="/5DED8BEB" Ref="#PWR011"  Part="1" 
AR Path="/5DEC99EE/5DED8BEB" Ref="#PWR?"  Part="1" 
F 0 "#PWR011" H 7400 4900 50  0001 C CNN
F 1 "GND" H 7405 4977 50  0000 C CNN
F 2 "" H 7400 5150 50  0001 C CNN
F 3 "" H 7400 5150 50  0001 C CNN
	1    7400 5150
	1    0    0    -1  
$EndComp
Text Label 7000 5050 1    50   ~ 0
VCC
Text Label 7200 5050 1    50   ~ 0
VCC
Wire Wire Line
	8200 2800 8200 2850
Wire Wire Line
	9000 2800 9000 2950
Wire Wire Line
	8200 2800 9000 2800
Wire Wire Line
	9000 2800 9350 2800
Wire Wire Line
	9350 2800 9350 2950
Connection ~ 9000 2800
Text Label 8750 2800 0    50   ~ 0
5V
$Comp
L power:GND #PWR?
U 1 1 5DED8BFA
P 6200 4500
AR Path="/5B6AF6DC/5DED8BFA" Ref="#PWR?"  Part="1" 
AR Path="/5DED8BFA" Ref="#PWR06"  Part="1" 
AR Path="/5DEC99EE/5DED8BFA" Ref="#PWR?"  Part="1" 
F 0 "#PWR06" H 6200 4250 50  0001 C CNN
F 1 "GND" H 6205 4327 50  0000 C CNN
F 2 "" H 6200 4500 50  0001 C CNN
F 3 "" H 6200 4500 50  0001 C CNN
	1    6200 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 4500 6450 4500
Text Notes 7050 5500 0    39   ~ 0
0x14
Text Notes 7850 4550 0    39   ~ 0
VOL around 0.4V @25mA\n
$Comp
L Device:R R?
U 1 1 5DED8C08
P 9350 3450
AR Path="/5B6AF6DC/5DED8C08" Ref="R?"  Part="1" 
AR Path="/5DED8C08" Ref="R5"  Part="1" 
AR Path="/5DEC99EE/5DED8C08" Ref="R?"  Part="1" 
F 0 "R5" V 9143 3450 50  0000 C CNN
F 1 "330" V 9234 3450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 9280 3450 50  0001 C CNN
F 3 "~" H 9350 3450 50  0001 C CNN
F 4 "CF14JT330RCT-ND" H 9350 3450 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/stackpole-electronics-inc/CF14JT330R/CF14JT330RCT-ND/1830338" H 9350 3450 50  0001 C CNN "DigikeyPL"
	1    9350 3450
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 5DED8C10
P 8200 3450
AR Path="/5B6AF6DC/5DED8C10" Ref="R?"  Part="1" 
AR Path="/5DED8C10" Ref="R2"  Part="1" 
AR Path="/5DEC99EE/5DED8C10" Ref="R?"  Part="1" 
F 0 "R2" H 8270 3496 50  0000 L CNN
F 1 "120" H 8270 3405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8130 3450 50  0001 C CNN
F 3 "~" H 8200 3450 50  0001 C CNN
F 4 "82QBK-ND" H 8200 3450 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/yageo/CFR-25JB-52-82R/82QBK-ND/3609" H 8200 3450 50  0001 C CNN "DigikeyPL"
	1    8200 3450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5DED8C16
P 9000 2800
AR Path="/5B6AF6DC/5DED8C16" Ref="#PWR?"  Part="1" 
AR Path="/5DED8C16" Ref="#PWR012"  Part="1" 
AR Path="/5DEC99EE/5DED8C16" Ref="#PWR?"  Part="1" 
F 0 "#PWR012" H 9000 2650 50  0001 C CNN
F 1 "+5V" H 9015 2973 50  0000 C CNN
F 2 "" H 9000 2800 50  0001 C CNN
F 3 "" H 9000 2800 50  0001 C CNN
	1    9000 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 4100 6100 4100
Wire Wire Line
	6100 4000 6450 4000
Text Label 6100 4000 0    50   ~ 0
I2C_SDA
Text Label 6100 4100 0    50   ~ 0
I2C_SDA
$Comp
L Switch:SW_Push SW1
U 1 1 5E3A9325
P 3550 5150
F 0 "SW1" H 3550 5435 50  0000 C CNN
F 1 "SW_Push" H 3550 5344 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 3550 5350 50  0001 C CNN
F 3 "~" H 3550 5350 50  0001 C CNN
	1    3550 5150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5E3A9861
P 4100 5250
F 0 "#PWR01" H 4100 5000 50  0001 C CNN
F 1 "GND" H 4105 5077 50  0000 C CNN
F 2 "" H 4100 5250 50  0001 C CNN
F 3 "" H 4100 5250 50  0001 C CNN
	1    4100 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 5150 4100 5150
Wire Wire Line
	4100 5150 4100 5250
Wire Wire Line
	3350 5150 2750 5150
$Comp
L Switch:SW_Push SW2
U 1 1 5E3B0017
P 3600 6050
F 0 "SW2" H 3600 6335 50  0000 C CNN
F 1 "SW_Push" H 3600 6244 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 3600 6250 50  0001 C CNN
F 3 "~" H 3600 6250 50  0001 C CNN
	1    3600 6050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5E3B0021
P 4150 6150
F 0 "#PWR02" H 4150 5900 50  0001 C CNN
F 1 "GND" H 4155 5977 50  0000 C CNN
F 2 "" H 4150 6150 50  0001 C CNN
F 3 "" H 4150 6150 50  0001 C CNN
	1    4150 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 6050 4150 6050
Wire Wire Line
	4150 6050 4150 6150
Wire Wire Line
	3400 6050 2800 6050
$Comp
L Switch:SW_Push SW3
U 1 1 5E3B7BE1
P 3600 7000
F 0 "SW3" H 3600 7285 50  0000 C CNN
F 1 "SW_Push" H 3600 7194 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 3600 7200 50  0001 C CNN
F 3 "~" H 3600 7200 50  0001 C CNN
	1    3600 7000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5E3B7BEB
P 4150 7100
F 0 "#PWR03" H 4150 6850 50  0001 C CNN
F 1 "GND" H 4155 6927 50  0000 C CNN
F 2 "" H 4150 7100 50  0001 C CNN
F 3 "" H 4150 7100 50  0001 C CNN
	1    4150 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 7000 4150 7000
Wire Wire Line
	4150 7000 4150 7100
Wire Wire Line
	3400 7000 2800 7000
Text Label 2750 5150 0    50   ~ 0
BTN1
Text Label 2800 6050 0    50   ~ 0
BTN2
Text Label 2800 7000 0    50   ~ 0
BTN3
$Comp
L Connector_Generic:Conn_01x10 J_Main1
U 1 1 5DCDAF5A
P 1400 2900
F 0 "J_Main1" H 1318 3517 50  0000 C CNN
F 1 "Conn_01x10" H 1318 3426 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x05_P2.54mm_Vertical" H 1400 2900 50  0001 C CNN
F 3 "~" H 1400 2900 50  0001 C CNN
F 4 "1175-1658-ND" H 1400 2900 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/cnc-tech/3040-10-00/1175-1658-ND/3883740" H 1400 2900 50  0001 C CNN "DigikeyPL"
	1    1400 2900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1600 2500 2200 2500
Wire Wire Line
	1600 2600 2200 2600
Wire Wire Line
	1600 2700 2200 2700
Wire Wire Line
	1600 2800 2200 2800
Wire Wire Line
	1600 2900 2200 2900
Wire Wire Line
	1600 3000 2200 3000
Wire Wire Line
	1600 3100 2200 3100
Wire Wire Line
	1600 3200 2200 3200
Wire Wire Line
	1600 3300 2200 3300
Wire Wire Line
	1600 3400 2200 3400
Text Label 2200 2500 2    50   ~ 0
VCC
Text Label 2200 2600 2    50   ~ 0
5V
Text Label 2200 2700 2    50   ~ 0
5V
Text Label 2200 2800 2    50   ~ 0
I2C_SDA
Text Label 2200 2900 2    50   ~ 0
I2C_SCL
Text Label 2200 3000 2    50   ~ 0
BTN1
Text Label 2200 3100 2    50   ~ 0
BTN2
Text Label 2200 3200 2    50   ~ 0
BTN3
Text Label 2200 3300 2    50   ~ 0
GND
Text Label 2200 3400 2    50   ~ 0
GND
$Comp
L Device:LED_RAGB D?
U 1 1 5DCF1C80
P 10200 3000
AR Path="/5B6AF6DC/5DCF1C80" Ref="D?"  Part="1" 
AR Path="/5DCF1C80" Ref="D4"  Part="1" 
AR Path="/5DEC99EE/5DCF1C80" Ref="D?"  Part="1" 
F 0 "D4" V 10246 2670 50  0000 R CNN
F 1 "LED_RAGB" V 10155 2670 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm-4_RGB_Staggered_Pins" H 10200 2950 50  0001 C CNN
F 3 "~" H 10200 2950 50  0001 C CNN
	1    10200 3000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5DCF1C8C
P 10000 3400
AR Path="/5B6AF6DC/5DCF1C8C" Ref="R?"  Part="1" 
AR Path="/5DCF1C8C" Ref="R6"  Part="1" 
AR Path="/5DEC99EE/5DCF1C8C" Ref="R?"  Part="1" 
F 0 "R6" H 10070 3446 50  0000 L CNN
F 1 "120" H 10070 3355 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 9930 3400 50  0001 C CNN
F 3 "~" H 10000 3400 50  0001 C CNN
F 4 "CF14JT120RCT-ND" H 10000 3400 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/stackpole-electronics-inc/CF14JT120R/CF14JT120RCT-ND/1830329" H 10000 3400 50  0001 C CNN "DigikeyPL"
	1    10000 3400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5DCF1C98
P 10400 3400
AR Path="/5B6AF6DC/5DCF1C98" Ref="R?"  Part="1" 
AR Path="/5DCF1C98" Ref="R8"  Part="1" 
AR Path="/5DEC99EE/5DCF1C98" Ref="R?"  Part="1" 
F 0 "R8" H 10470 3446 50  0000 L CNN
F 1 "120" H 10470 3355 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 10330 3400 50  0001 C CNN
F 3 "~" H 10400 3400 50  0001 C CNN
F 4 "82QBK-ND" H 10400 3400 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/yageo/CFR-25JB-52-82R/82QBK-ND/3609" H 10400 3400 50  0001 C CNN "DigikeyPL"
	1    10400 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 3250 10000 3200
Wire Wire Line
	10200 3250 10200 3200
Wire Wire Line
	10400 3250 10400 3200
$Comp
L Device:R R?
U 1 1 5DCF1CAA
P 10200 3400
AR Path="/5B6AF6DC/5DCF1CAA" Ref="R?"  Part="1" 
AR Path="/5DCF1CAA" Ref="R7"  Part="1" 
AR Path="/5DEC99EE/5DCF1CAA" Ref="R?"  Part="1" 
F 0 "R7" H 10270 3446 50  0000 L CNN
F 1 "120" H 10270 3355 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 10130 3400 50  0001 C CNN
F 3 "~" H 10200 3400 50  0001 C CNN
F 4 "82QBK-ND" H 10200 3400 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/yageo/CFR-25JB-52-82R/82QBK-ND/3609" H 10200 3400 50  0001 C CNN "DigikeyPL"
	1    10200 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	10200 2800 9350 2800
Connection ~ 9350 2800
$Comp
L LasertagComponents:PCA9634 U?
U 1 1 5DED8B8E
P 7100 4150
AR Path="/5B6AF6DC/5DED8B8E" Ref="U?"  Part="1" 
AR Path="/5DED8B8E" Ref="U1"  Part="1" 
AR Path="/5DEC99EE/5DED8B8E" Ref="U?"  Part="1" 
F 0 "U1" H 7100 4915 50  0000 C CNN
F 1 "PCA9634" H 7100 4824 50  0000 C CNN
F 2 "LasertagFootprints:SO20_NXP_SOT163-1_Hand" H 7100 4150 50  0001 C CNN
F 3 "https://www.nxp.com/docs/en/data-sheet/PCA9634.pdf" H 7100 4150 50  0001 C CNN
F 4 "568-4065-1-ND" H 7100 4150 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/nxp-usa-inc/PCA9634D118/568-4065-1-ND/1534152" H 7100 4150 50  0001 C CNN "DigikeyPL"
	1    7100 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 4150 10000 4150
Wire Wire Line
	10000 3550 10000 4150
Wire Wire Line
	7750 4250 10200 4250
Wire Wire Line
	10200 3550 10200 4250
Wire Wire Line
	10400 4350 7750 4350
Wire Wire Line
	10400 3550 10400 4350
$EndSCHEMATC
