EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Interface_Optical:TSOP331xx U1
U 1 1 5B2CB006
P 4500 3700
F 0 "U1" H 4100 4000 50  0000 L CNN
F 1 "TSOP331xx" H 4100 3400 50  0000 L CNN
F 2 "OptoDevice:Vishay_MINIMOLD-3Pin" H 4450 3325 50  0001 C CNN
F 3 "" H 5150 4000 50  0001 C CNN
	1    4500 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 5BFED567
P 5550 4700
F 0 "D1" V 5588 4583 50  0000 R CNN
F 1 "LED" V 5497 4583 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 5550 4700 50  0001 C CNN
F 3 "~" H 5550 4700 50  0001 C CNN
	1    5550 4700
	0    -1   -1   0   
$EndComp
$Comp
L Motor:Motor_DC M1
U 1 1 5BFED70E
P 5950 4600
F 0 "M1" H 6108 4596 50  0000 L CNN
F 1 "Motor_DC" H 6108 4505 50  0000 L CNN
F 2 "Footprints:SolderWirePad_2x_0-8mmDrill" H 5950 4510 50  0001 C CNN
F 3 "~" H 5950 4510 50  0001 C CNN
	1    5950 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5BFED7BD
P 5550 4400
F 0 "R1" H 5620 4446 50  0000 L CNN
F 1 "330" H 5620 4355 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5480 4400 50  0001 C CNN
F 3 "~" H 5550 4400 50  0001 C CNN
F 4 "RHM330KCT-ND" H 5550 4400 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/rohm-semiconductor/ESR10EZPJ331/RHM330KCT-ND/1763012" H 5550 4400 50  0001 C CNN "DigikeyPL"
	1    5550 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5BFED868
P 5950 4250
F 0 "R2" H 6020 4296 50  0000 L CNN
F 1 "22" H 6020 4205 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5880 4250 50  0001 C CNN
F 3 "~" H 5950 4250 50  0001 C CNN
F 4 "RHM22KCT-ND" H 5950 4250 50  0001 C CNN "DigikeyPN"
F 5 "https://www.digikey.de/product-detail/de/rohm-semiconductor/ESR10EZPJ220/RHM22KCT-ND/1762989" H 5950 4250 50  0001 C CNN "DigikeyPL"
	1    5950 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 4850 5550 5000
Wire Wire Line
	5550 5000 5950 5000
Wire Wire Line
	5950 5000 5950 4900
Wire Wire Line
	5950 5000 6600 5000
Connection ~ 5950 5000
Wire Wire Line
	5550 4250 5550 4100
Wire Wire Line
	5550 4100 5750 4100
Wire Wire Line
	5750 4100 5750 3500
Connection ~ 5750 4100
Wire Wire Line
	5750 4100 5950 4100
Connection ~ 5750 3500
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 5BFEE458
P 7900 3500
F 0 "J1" H 7980 3492 50  0000 L CNN
F 1 "Conn_Power1" H 7980 3401 50  0000 L CNN
F 2 "Footprints:SolderWirePad_2x_0-8mmDrill" H 7900 3500 50  0001 C CNN
F 3 "~" H 7900 3500 50  0001 C CNN
	1    7900 3500
	1    0    0    -1  
$EndComp
Text Notes 4800 5150 0    50   ~ 0
This is connected to an open drain driver. If on we have around 0.4V here\n
$Comp
L Connector_Generic:Conn_01x02 J2
U 1 1 5BFEF837
P 7900 3800
F 0 "J2" H 7980 3792 50  0000 L CNN
F 1 "Conn_Power2" H 7980 3701 50  0000 L CNN
F 2 "Footprints:SolderWirePad_2x_0-8mmDrill" H 7900 3800 50  0001 C CNN
F 3 "~" H 7900 3800 50  0001 C CNN
	1    7900 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 3500 7550 3800
Wire Wire Line
	7550 3800 7700 3800
Connection ~ 7550 3500
Wire Wire Line
	7550 3500 7700 3500
Wire Wire Line
	5750 3500 7000 3500
Wire Wire Line
	4900 3900 7000 3900
Wire Wire Line
	7600 3900 7600 3600
Wire Wire Line
	7600 3600 7700 3600
Connection ~ 7600 3900
Wire Wire Line
	7600 3900 7700 3900
$Comp
L power:GND #PWR02
U 1 1 5BFEFED1
P 7350 3900
F 0 "#PWR02" H 7350 3650 50  0001 C CNN
F 1 "GND" H 7355 3727 50  0000 C CNN
F 2 "" H 7350 3900 50  0001 C CNN
F 3 "" H 7350 3900 50  0001 C CNN
	1    7350 3900
	1    0    0    -1  
$EndComp
Connection ~ 7350 3900
Wire Wire Line
	7350 3900 7500 3900
$Comp
L power:+3.3V #PWR01
U 1 1 5BFEFF11
P 7350 3500
F 0 "#PWR01" H 7350 3350 50  0001 C CNN
F 1 "+3.3V" H 7365 3673 50  0000 C CNN
F 2 "" H 7350 3500 50  0001 C CNN
F 3 "" H 7350 3500 50  0001 C CNN
	1    7350 3500
	1    0    0    -1  
$EndComp
Connection ~ 7350 3500
Wire Wire Line
	7350 3500 7550 3500
$Comp
L Connector_Generic:Conn_01x02 J3
U 1 1 5BFF0028
P 7900 4350
F 0 "J3" H 7980 4342 50  0000 L CNN
F 1 "Conn_Data" H 7980 4251 50  0000 L CNN
F 2 "Footprints:SolderWirePad_2x_0-8mmDrill" H 7900 4350 50  0001 C CNN
F 3 "~" H 7900 4350 50  0001 C CNN
	1    7900 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 5000 6600 4450
Wire Wire Line
	6600 4450 7350 4450
Wire Wire Line
	4900 3700 6600 3700
Wire Wire Line
	6600 3700 6600 4350
Wire Wire Line
	6600 4350 7450 4350
$Comp
L Device:C_Small C1
U 1 1 5FC03363
P 7000 3700
F 0 "C1" H 7092 3746 50  0000 L CNN
F 1 "C_Small" H 7092 3655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 7000 3700 50  0001 C CNN
F 3 "~" H 7000 3700 50  0001 C CNN
	1    7000 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 3500 5750 3500
Wire Wire Line
	7000 3800 7000 3900
Connection ~ 7000 3900
Wire Wire Line
	7000 3900 7350 3900
Wire Wire Line
	7000 3600 7000 3500
Connection ~ 7000 3500
Wire Wire Line
	7000 3500 7200 3500
$Comp
L Connector_Generic:Conn_01x02 J4
U 1 1 5FC08D48
P 7900 4700
F 0 "J4" H 7980 4692 50  0000 L CNN
F 1 "Conn_Data_Out" H 7980 4601 50  0000 L CNN
F 2 "Footprints:SolderWirePad_2x_0-8mmDrill" H 7900 4700 50  0001 C CNN
F 3 "~" H 7900 4700 50  0001 C CNN
	1    7900 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 4350 7450 4700
Wire Wire Line
	7450 4700 7700 4700
Connection ~ 7450 4350
Wire Wire Line
	7450 4350 7700 4350
Wire Wire Line
	7350 4450 7350 4800
Wire Wire Line
	7350 4800 7700 4800
Connection ~ 7350 4450
Wire Wire Line
	7350 4450 7700 4450
$Comp
L Connector_Generic:Conn_01x02 J5
U 1 1 5FC0A110
P 10700 3000
F 0 "J5" H 10780 2992 50  0000 L CNN
F 1 "Conn_Pass1" H 10780 2901 50  0000 L CNN
F 2 "Footprints:SolderWirePad_2x_0-8mmDrill" H 10700 3000 50  0001 C CNN
F 3 "~" H 10700 3000 50  0001 C CNN
	1    10700 3000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J6
U 1 1 5FC0AC40
P 8550 2850
F 0 "J6" H 8630 2842 50  0000 L CNN
F 1 "Conn_Pass2" H 8630 2751 50  0000 L CNN
F 2 "Footprints:SolderWirePad_2x_0-8mmDrill" H 8550 2850 50  0001 C CNN
F 3 "~" H 8550 2850 50  0001 C CNN
	1    8550 2850
	-1   0    0    -1  
$EndComp
$Comp
L Device:LED D2
U 1 1 5FC0DF22
P 5100 4700
F 0 "D2" V 5138 4583 50  0000 R CNN
F 1 "LED_ALT" V 5047 4583 50  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5100 4700 50  0001 C CNN
F 3 "~" H 5100 4700 50  0001 C CNN
	1    5100 4700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5100 4850 5100 5000
Wire Wire Line
	5100 5000 5550 5000
Connection ~ 5550 5000
Wire Wire Line
	5100 4550 5550 4550
Connection ~ 5550 4550
$Comp
L Jumper:Jumper_2_Open JP1
U 1 1 5FC1258C
P 10050 3100
F 0 "JP1" V 10004 3198 50  0000 L CNN
F 1 "Jumper_2_Open" V 10095 3198 50  0000 L CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 10050 3100 50  0001 C CNN
F 3 "~" H 10050 3100 50  0001 C CNN
	1    10050 3100
	0    1    1    0   
$EndComp
$Comp
L 74xGxx:74AUC1G00 U2
U 1 1 5FC17F10
P 9600 2900
F 0 "U2" H 9575 3167 50  0000 C CNN
F 1 "Generic Logic" H 9575 3076 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5_HandSoldering" H 9600 2900 50  0001 C CNN
F 3 "" H 9600 2900 50  0001 C CNN
	1    9600 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8750 2850 9050 2850
Wire Wire Line
	9300 2950 9050 2950
Wire Wire Line
	9050 2950 9050 3300
Wire Wire Line
	9050 3300 10050 3300
Connection ~ 9050 2950
Wire Wire Line
	9050 2950 8750 2950
$Comp
L Jumper:Jumper_2_Bridged JP2
U 1 1 5FC24EF5
P 9550 2500
F 0 "JP2" H 9550 2695 50  0000 C CNN
F 1 "Jumper_2_Bridged" H 9550 2604 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged2Bar_RoundedPad1.0x1.5mm" H 9550 2500 50  0001 C CNN
F 3 "~" H 9550 2500 50  0001 C CNN
	1    9550 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 2850 9050 2500
Wire Wire Line
	9050 2500 9350 2500
Connection ~ 9050 2850
Wire Wire Line
	9050 2850 9300 2850
Wire Wire Line
	9750 2500 9950 2500
Wire Wire Line
	9950 2500 9950 2900
Wire Wire Line
	9950 2900 9850 2900
Connection ~ 10050 3300
Wire Wire Line
	10050 2900 9950 2900
Connection ~ 9950 2900
Wire Wire Line
	10050 2900 10500 2900
Wire Wire Line
	10500 2900 10500 3000
Connection ~ 10050 2900
Wire Wire Line
	10500 3100 10500 3300
Wire Wire Line
	10050 3300 10500 3300
Wire Wire Line
	9600 2800 9600 2700
Wire Wire Line
	9600 2700 7200 2700
Wire Wire Line
	7200 2700 7200 3500
Connection ~ 7200 3500
Wire Wire Line
	7200 3500 7350 3500
Wire Wire Line
	9600 4000 7500 4000
Wire Wire Line
	7500 4000 7500 3900
Wire Wire Line
	9600 3000 9600 4000
Connection ~ 7500 3900
Wire Wire Line
	7500 3900 7600 3900
$EndSCHEMATC
