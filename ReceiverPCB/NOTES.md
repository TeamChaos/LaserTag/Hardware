# Notes

## Possible Improvements

- Pull inputs to ground to avoid noise if no input is connected
- Better cable isolation
- Smaller layout
- Bigger caps at Vin and after switchin regulator
- Amplifier footprint was to small



## Other

10 W are quite a lot (e.g. reduce switching power supply output to 9V)

