EESchema Schematic File Version 4
LIBS:VestMountingPlate-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x06 J_Board1
U 1 1 5DCEFE55
P 7950 2150
F 0 "J_Board1" H 8030 2142 50  0000 L CNN
F 1 "Conn_01x06" H 8030 2051 50  0000 L CNN
F 2 "Connector_PinSocket_2.00mm:PinSocket_1x06_P2.00mm_Vertical" H 7950 2150 50  0001 C CNN
F 3 "~" H 7950 2150 50  0001 C CNN
F 4 "S5751-06-ND" H 7950 2150 50  0001 C CNN "DigikeyPN"
	1    7950 2150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x06 J_Board2
U 1 1 5DCF054D
P 8000 3850
F 0 "J_Board2" H 8080 3842 50  0000 L CNN
F 1 "Conn_01x06" H 8080 3751 50  0000 L CNN
F 2 "Connector_PinSocket_2.00mm:PinSocket_1x06_P2.00mm_Vertical" H 8000 3850 50  0001 C CNN
F 3 "~" H 8000 3850 50  0001 C CNN
F 4 "S5751-06-ND" H 8000 3850 50  0001 C CNN "DigikeyPN"
	1    8000 3850
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x06 J_Board3
U 1 1 5DCF0940
P 8000 5250
F 0 "J_Board3" H 8080 5242 50  0000 L CNN
F 1 "Conn_01x06" H 8080 5151 50  0000 L CNN
F 2 "Connector_PinSocket_2.00mm:PinSocket_1x06_P2.00mm_Vertical" H 8000 5250 50  0001 C CNN
F 3 "~" H 8000 5250 50  0001 C CNN
F 4 "S5751-06-ND" H 8000 5250 50  0001 C CNN "DigikeyPN"
	1    8000 5250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J_Dect5
U 1 1 5DCF34AE
P 1850 1450
F 0 "J_Dect5" H 1768 1767 50  0000 C CNN
F 1 "Conn_01x03" H 1768 1676 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x03_P3.175mm_Drill1mm" H 1850 1450 50  0001 C CNN
F 3 "~" H 1850 1450 50  0001 C CNN
	1    1850 1450
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J_Dect6
U 1 1 5DCF3C35
P 1850 1950
F 0 "J_Dect6" H 1768 2267 50  0000 C CNN
F 1 "Conn_01x03" H 1768 2176 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x03_P3.175mm_Drill1mm" H 1850 1950 50  0001 C CNN
F 3 "~" H 1850 1950 50  0001 C CNN
	1    1850 1950
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J_Dect7
U 1 1 5DCF3E3C
P 1850 2450
F 0 "J_Dect7" H 1768 2767 50  0000 C CNN
F 1 "Conn_01x03" H 1768 2676 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x03_P3.175mm_Drill1mm" H 1850 2450 50  0001 C CNN
F 3 "~" H 1850 2450 50  0001 C CNN
	1    1850 2450
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J_Dect8
U 1 1 5DCF4014
P 1850 2950
F 0 "J_Dect8" H 1768 3267 50  0000 C CNN
F 1 "Conn_01x03" H 1768 3176 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x03_P3.175mm_Drill1mm" H 1850 2950 50  0001 C CNN
F 3 "~" H 1850 2950 50  0001 C CNN
	1    1850 2950
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J_Dect9
U 1 1 5DCF428C
P 1850 3450
F 0 "J_Dect9" H 1768 3767 50  0000 C CNN
F 1 "Conn_01x03" H 1768 3676 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x03_P3.175mm_Drill1mm" H 1850 3450 50  0001 C CNN
F 3 "~" H 1850 3450 50  0001 C CNN
	1    1850 3450
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J_Dect10
U 1 1 5DCF44F1
P 1850 3950
F 0 "J_Dect10" H 1768 4267 50  0000 C CNN
F 1 "Conn_01x03" H 1768 4176 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x03_P3.175mm_Drill1mm" H 1850 3950 50  0001 C CNN
F 3 "~" H 1850 3950 50  0001 C CNN
	1    1850 3950
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J_Vib6
U 1 1 5DCF52A4
P 1850 4900
F 0 "J_Vib6" H 1768 5117 50  0000 C CNN
F 1 "Conn_01x02" H 1768 5026 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x02_P3.81mm_Drill1mm" H 1850 4900 50  0001 C CNN
F 3 "~" H 1850 4900 50  0001 C CNN
	1    1850 4900
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J_VIB5
U 1 1 5DCF5A63
P 1850 4450
F 0 "J_VIB5" H 1768 4667 50  0000 C CNN
F 1 "Conn_01x02" H 1768 4576 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x02_P3.81mm_Drill1mm" H 1850 4450 50  0001 C CNN
F 3 "~" H 1850 4450 50  0001 C CNN
	1    1850 4450
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J_Power1
U 1 1 5DCF6BF3
P 1850 900
F 0 "J_Power1" H 1768 1117 50  0000 C CNN
F 1 "Conn_01x02" H 1768 1026 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x02_P3.81mm_Drill1mm" H 1850 900 50  0000 C CNN
F 3 "~" H 1850 900 50  0001 C CNN
	1    1850 900 
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J_Weapon1
U 1 1 5DCF7330
P 1850 5650
F 0 "J_Weapon1" H 1768 5967 50  0000 C CNN
F 1 "Conn_01x04" H 1768 5876 50  0000 C CNN
F 2 "Connector_Wire:SolderWirePad_1x04_P3.81mm_Drill1mm" H 1850 5650 50  0001 C CNN
F 3 "~" H 1850 5650 50  0001 C CNN
	1    1850 5650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2050 900  2650 900 
Wire Wire Line
	2050 1000 2650 1000
Text Label 2650 900  2    50   ~ 0
VBAT
Text Label 2650 1000 2    50   ~ 0
5VGND
Wire Wire Line
	2050 1350 2650 1350
Wire Wire Line
	2050 1450 2650 1450
Wire Wire Line
	2050 1550 2650 1550
Wire Wire Line
	2050 1850 2650 1850
Wire Wire Line
	2050 1950 2650 1950
Wire Wire Line
	2050 2050 2650 2050
Wire Wire Line
	2050 2350 2650 2350
Wire Wire Line
	2050 2450 2650 2450
Wire Wire Line
	2050 2550 2650 2550
Wire Wire Line
	2050 2850 2650 2850
Wire Wire Line
	2050 2950 2650 2950
Wire Wire Line
	2050 3050 2650 3050
Wire Wire Line
	2050 3350 2650 3350
Wire Wire Line
	2050 3450 2650 3450
Wire Wire Line
	2050 3550 2650 3550
Wire Wire Line
	2050 3850 2650 3850
Wire Wire Line
	2050 3950 2650 3950
Wire Wire Line
	2050 4050 2650 4050
Wire Wire Line
	2050 4450 2650 4450
Wire Wire Line
	2050 4550 2650 4550
Wire Wire Line
	2050 4900 2650 4900
Wire Wire Line
	2050 5000 2650 5000
Wire Wire Line
	2050 5550 2650 5550
Wire Wire Line
	2050 5650 2650 5650
Wire Wire Line
	2050 5750 2650 5750
Wire Wire Line
	2050 5850 2650 5850
Text Label 2650 5850 2    50   ~ 0
5VGND
Text Label 2650 5550 2    50   ~ 0
5V
Text Label 2650 5650 2    50   ~ 0
I2C_SDA
Text Label 2650 5750 2    50   ~ 0
I2C_SCL
Text Label 2650 4450 2    50   ~ 0
VIB_VCC
Text Label 2650 4900 2    50   ~ 0
VIB_VCC
Text Label 2650 4550 2    50   ~ 0
VIB5
Text Label 2650 5000 2    50   ~ 0
VIB6
Text Label 2650 1350 2    50   ~ 0
VCC
Text Label 2650 1850 2    50   ~ 0
VCC
Text Label 2650 2350 2    50   ~ 0
VCC
Text Label 2650 2850 2    50   ~ 0
VCC
Text Label 2650 3350 2    50   ~ 0
VCC
Text Label 2650 3850 2    50   ~ 0
VCC
Text Label 2650 1450 2    50   ~ 0
GND
Text Label 2650 1950 2    50   ~ 0
GND
Text Label 2650 2450 2    50   ~ 0
GND
Text Label 2650 2950 2    50   ~ 0
GND
Text Label 2650 3450 2    50   ~ 0
GND
Text Label 2650 3950 2    50   ~ 0
GND
Text Label 2650 1550 2    50   ~ 0
DECT5
Text Label 2650 2050 2    50   ~ 0
DECT6
Text Label 2650 2550 2    50   ~ 0
DECT7
Text Label 2650 3050 2    50   ~ 0
DECT8
Text Label 2650 3550 2    50   ~ 0
DECT9
Text Label 2650 4050 2    50   ~ 0
DECT10
Wire Wire Line
	7750 1950 6600 1950
Wire Wire Line
	6600 2050 7750 2050
Wire Wire Line
	7750 2150 6600 2150
Wire Wire Line
	6600 2250 7750 2250
Wire Wire Line
	6600 2350 7750 2350
Wire Wire Line
	6600 2450 7750 2450
Wire Wire Line
	7800 3650 6600 3650
Wire Wire Line
	6600 3750 7800 3750
Wire Wire Line
	6600 3850 7800 3850
Wire Wire Line
	6600 3950 7800 3950
Wire Wire Line
	6600 4050 7800 4050
Wire Wire Line
	6600 4150 7800 4150
Wire Wire Line
	7800 5050 6600 5050
Wire Wire Line
	6600 5150 7800 5150
Wire Wire Line
	7800 5250 6600 5250
Wire Wire Line
	6600 5350 7800 5350
Wire Wire Line
	6600 5450 7800 5450
Wire Wire Line
	6600 5550 7800 5550
Text Label 6600 2050 0    50   ~ 0
VIB6
Text Label 6600 2150 0    50   ~ 0
VIB5
Text Label 6600 2250 0    50   ~ 0
VIB_VCC
Text Label 6600 2350 0    50   ~ 0
GND
Text Label 6600 2450 0    50   ~ 0
VCC
Text Label 6600 3650 0    50   ~ 0
I2C_SDA
Text Label 6600 3750 0    50   ~ 0
I2C_SCL
Text Label 6600 3850 0    50   ~ 0
5V
Text Label 6600 4050 0    50   ~ 0
VBAT
Text Label 6600 3950 0    50   ~ 0
5VGND
Text Label 6600 5050 0    50   ~ 0
DECT5
Text Label 6600 5150 0    50   ~ 0
DECT6
Text Label 6600 5250 0    50   ~ 0
DECT7
Text Label 6600 5350 0    50   ~ 0
DECT8
Text Label 6600 5450 0    50   ~ 0
DECT9
Text Label 6600 5550 0    50   ~ 0
DECT10
Text Label 6600 4150 0    50   ~ 0
VBAT
Text Label 6600 1950 0    50   ~ 0
5VGND
$EndSCHEMATC
