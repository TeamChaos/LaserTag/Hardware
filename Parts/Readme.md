## Electrical Components

Most electrical components can be ordered from Digikey. A couple components have to be ordered elsewhere or replaced with a similar substitute.  
If you are living in Germany/EU some components might be cheaper to order some parts from Reichelt.de. This mostly applies for smaller quantities (<50 sets probably).

### Parts

#### Main PCB

Digikey BOM (for 1 PCB): See csv table in this folder

Additionally you need two generic LEDs (red, green), a NodeMCUv3, a generic 433MHz receiver as well as a generic 433MHz transmitter and 10 IR detector modules (digital output) with the correct footprint. These components could e.g. be ordered via Aliexpress.  

Furthermore you need the flash LED, some 6pin connector for the head detectors, around 3m of 3 conductor wire, about 0.5m of 6 conductor wire and quite a lot of stranded wire per set. These can e.g. be ordered from Reichelt:

- LIYY 614
- LIYY 314
- LITZE
- MAS 60
- MAK 60
- LED LL 8-13500WS

Some particular components that are cheaper (in smaller quantities) if ordered from Reichelt instead of Digikey:

- BC 557B
- BC 547B
- Attiny 861A-PU
- M-A 220U 6,3



#### Weapon PCB

Digikey BOM (for 1 PCB): See csv table in this folder

Additionally you need three generic LEDs (red, blue, yellow).

Furthermore you need a 8mm very bright white LED like the *LED LL 8-13500WS* from [Reichelt](https://www.reichelt.de/led-8-mm-bedrahtet-kaltweiss-13500-mcd-30-led-ll-8-13500ws-p156354.html?)

Some particular components that are cheaper (in smaller quantities) if ordered from Reichelt instead of Digikey:

- BC 557B
- BC 547B
- Attiny 861A-PU
- RND 1N5817
- M-A 220U 6,3



### Costs

The cost per set very much vary over the quantity ordered.

For quantities between 4 and 10 you can expect something around 20-30€ per set for electronic parts (excluding PCBs).

For more information check out the Cost.md in the Concept repository.