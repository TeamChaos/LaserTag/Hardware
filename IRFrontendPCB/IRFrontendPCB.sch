EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:LED D3
U 1 1 5F99E7C2
P 5100 4050
F 0 "D3" V 5139 3932 50  0000 R CNN
F 1 "SFH4545" V 5048 3932 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 5100 4050 50  0001 C CNN
F 3 "https://dammedia.osram.info/media/resource/hires/osram-dam-2496274/SFH%204545.pdf" H 5100 4050 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/osram-opto-semiconductors-inc/SFH-4545/475-2919-ND/2205955" V 5100 4050 50  0001 C CNN "DigikeyLink"
F 5 "475-2919-ND" V 5100 4050 50  0001 C CNN "DigikeyPN"
	1    5100 4050
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D2
U 1 1 5F99EB5A
P 4750 4050
F 0 "D2" V 4789 3932 50  0000 R CNN
F 1 "VSMB294008G" V 4698 3932 50  0000 R CNN
F 2 "LasertagFootprints:VSMB29_LED_SMD" H 4750 4050 50  0001 C CNN
F 3 "https://www.vishay.com/docs/84228/vsmb294008rg.pdf" H 4750 4050 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/vishay-semiconductor-opto-division/VSMB294008G/VSMB294008GCT-ND/5012875" V 4750 4050 50  0001 C CNN "DigikeyLink"
F 5 "VSMB294008GCT-ND" V 4750 4050 50  0001 C CNN "DigikeyPN"
	1    4750 4050
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D4
U 1 1 5F99ED75
P 6050 4050
F 0 "D4" V 6089 3932 50  0000 R CNN
F 1 "XZMDK54W-8" V 5998 3932 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6050 4050 50  0001 C CNN
F 3 "https://www.sunledusa.com/products/spec/XZMDK54W-8.pdf" H 6050 4050 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/sunled/XZMDK54W-8/1497-1431-1-ND/8259108" V 6050 4050 50  0001 C CNN "DigikeyLink"
F 5 "1497-1431-1-ND" V 6050 4050 50  0001 C CNN "DigikeyPN"
	1    6050 4050
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D5
U 1 1 5F99F19F
P 6400 4050
F 0 "D5" V 6439 3932 50  0000 R CNN
F 1 "SM0805UWC" V 6348 3932 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6302 3932 50  0001 R CNN
F 3 "https://www.bivar.com/parts_content/Datasheets/SM0805UWC.pdf" H 6400 4050 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/bivar-inc/SM0805UWC/492-1331-6-ND/2408564" V 6400 4050 50  0001 C CNN "DigikeyLink"
F 5 "492-1331-6-ND" V 6400 4050 50  0001 C CNN "DigikeyPN"
	1    6400 4050
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R2
U 1 1 5F9A095A
P 4750 4500
F 0 "R2" H 4820 4546 50  0000 L CNN
F 1 "33" H 4820 4455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4680 4500 50  0001 C CNN
F 3 "https://www.rohm.com/datasheet/ESR01MZPF/esr-e" H 4750 4500 50  0001 C CNN
F 4 "https://www.digikey.com/en/products/detail/rohm-semiconductor/ESR10EZPJ330/1762806" H 4750 4500 50  0001 C CNN "DigikeyLink"
F 5 "RHM33KCT-ND" H 4750 4500 50  0001 C CNN "DigikeyPN"
	1    4750 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5F9A0B57
P 5100 4500
F 0 "R3" H 5170 4546 50  0000 L CNN
F 1 "3" H 5170 4455 50  0000 L CNN
F 2 "LasertagFootprints:R_1225_6432Metric_Pad3.2x6.3mm_HandSolder" V 5030 4500 50  0001 C CNN
F 3 "https://www.vishay.com/docs/20046/rcle3.pdf" H 5100 4500 50  0001 C CNN
F 4 "https://www.digikey.com/en/products/detail/vishay-dale/RCL12253R00JNEG/3030233" H 5100 4500 50  0001 C CNN "DigikeyLink"
F 5 "541-2452-1-ND" H 5100 4500 50  0001 C CNN "DigikeyPN"
	1    5100 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5F9A0C9A
P 6050 4500
F 0 "R4" H 6120 4546 50  0000 L CNN
F 1 "RMCF0805JT220R" H 6120 4455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5980 4500 50  0001 C CNN
F 3 "https://www.seielect.com/catalog/sei-rmcf_rmcp.pdf" H 6050 4500 50  0001 C CNN
F 4 "https://www.digikey.com/en/products/detail/stackpole-electronics-inc/RMCF0805JT220R/1757893" H 6050 4500 50  0001 C CNN "DigikeyLink"
F 5 "RMCF0805JT220RCT-ND" H 6050 4500 50  0001 C CNN "DigikeyPN"
	1    6050 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5F9A0F1D
P 6400 4500
F 0 "R5" H 6470 4546 50  0000 L CNN
F 1 "RMCF0805JT68R0" H 6470 4455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6330 4500 50  0001 C CNN
F 3 "https://www.seielect.com/catalog/sei-rmcf_rmcp.pdf" H 6400 4500 50  0001 C CNN
F 4 "https://www.digikey.com/en/products/detail/stackpole-electronics-inc/RMCF0805JT68R0/1757801" H 6400 4500 50  0001 C CNN "DigikeyLink"
F 5 "RMCF0805JT68R0CT-ND" H 6400 4500 50  0001 C CNN "DigikeyPN"
	1    6400 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 4350 4400 4200
Wire Wire Line
	4750 4350 4750 4200
Wire Wire Line
	5100 4200 5100 4350
Wire Wire Line
	6050 4350 6050 4200
Wire Wire Line
	6400 4350 6400 4200
Wire Wire Line
	5100 3900 4750 3900
Connection ~ 4750 3900
Wire Wire Line
	6400 3900 6050 3900
Wire Wire Line
	6050 3900 5100 3900
Connection ~ 6050 3900
Connection ~ 5100 3900
Wire Wire Line
	3850 3900 3850 4250
Wire Wire Line
	3850 4250 3550 4250
Wire Wire Line
	6050 4650 6400 4650
Wire Wire Line
	6050 4650 6050 4850
Connection ~ 6050 4650
$Comp
L Connector_Generic:Conn_01x01 VP1
U 1 1 5F9A6047
P 3350 4250
F 0 "VP1" H 3268 4025 50  0000 C CNN
F 1 "Conn_01x01" H 3268 4116 50  0000 C CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 3350 4250 50  0001 C CNN
F 3 "~" H 3350 4250 50  0001 C CNN
	1    3350 4250
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 IR1
U 1 1 5F9A65E7
P 3350 4450
F 0 "IR1" H 3268 4225 50  0000 C CNN
F 1 "Conn_01x01" H 3268 4316 50  0001 C CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 3350 4450 50  0001 C CNN
F 3 "~" H 3350 4450 50  0001 C CNN
	1    3350 4450
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 JF1
U 1 1 5F9A67D2
P 3350 4850
F 0 "JF1" H 3268 4625 50  0000 C CNN
F 1 "Conn_01x01" H 3268 4716 50  0000 C CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 3350 4850 50  0001 C CNN
F 3 "~" H 3350 4850 50  0001 C CNN
	1    3350 4850
	-1   0    0    1   
$EndComp
Wire Wire Line
	3850 3900 4400 3900
$Comp
L Device:LED D1
U 1 1 5F9B9514
P 4400 4050
F 0 "D1" V 4439 3932 50  0000 R CNN
F 1 "VSMB294008G" V 4348 3932 50  0000 R CNN
F 2 "LasertagFootprints:VSMB29_LED_SMD" H 4400 4050 50  0001 C CNN
F 3 "https://www.vishay.com/docs/84228/vsmb294008rg.pdf" H 4400 4050 50  0001 C CNN
F 4 "https://www.digikey.de/product-detail/de/vishay-semiconductor-opto-division/VSMB294008G/VSMB294008GCT-ND/5012875" V 4400 4050 50  0001 C CNN "DigikeyLink"
F 5 "VSMB294008GCT-ND" V 4400 4050 50  0001 C CNN "DigikeyPN"
	1    4400 4050
	0    -1   -1   0   
$EndComp
Connection ~ 4400 3900
Wire Wire Line
	4400 3900 4750 3900
$Comp
L Device:R R1
U 1 1 5F9B9969
P 4400 4500
F 0 "R1" H 4470 4546 50  0000 L CNN
F 1 "33" H 4470 4455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4330 4500 50  0001 C CNN
F 3 "https://www.rohm.com/datasheet/ESR01MZPF/esr-e" H 4400 4500 50  0001 C CNN
F 4 "https://www.digikey.com/en/products/detail/rohm-semiconductor/ESR10EZPJ330/1762806" H 4400 4500 50  0001 C CNN "DigikeyLink"
F 5 "RHM33KCT-ND" H 4400 4500 50  0001 C CNN "DigikeyPN"
	1    4400 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 4650 4750 4650
$Comp
L Connector_Generic:Conn_01x01 IR2
U 1 1 5F9C888E
P 3350 4650
F 0 "IR2" H 3268 4425 50  0000 C CNN
F 1 "Conn_01x01" H 3268 4516 50  0000 C CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x01_D0.9mm_OD2.1mm" H 3350 4650 50  0001 C CNN
F 3 "~" H 3350 4650 50  0001 C CNN
	1    3350 4650
	-1   0    0    1   
$EndComp
Connection ~ 4400 4650
Wire Wire Line
	3550 4650 3700 4650
Wire Wire Line
	5100 4650 5100 4750
Wire Wire Line
	5100 4750 4100 4750
Wire Wire Line
	4100 4450 3550 4450
Wire Wire Line
	4100 4450 4100 4750
Wire Wire Line
	3550 4850 6050 4850
$Comp
L Jumper:Jumper_2_Bridged JP1
U 1 1 5F9CAD31
P 3900 4750
F 0 "JP1" H 3900 4525 50  0000 C CNN
F 1 "Jumper_2_Bridged" H 3900 4616 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged2Bar_RoundedPad1.0x1.5mm" H 3900 4750 50  0001 C CNN
F 3 "~" H 3900 4750 50  0001 C CNN
	1    3900 4750
	-1   0    0    1   
$EndComp
Connection ~ 4100 4750
Wire Wire Line
	3700 4750 3700 4650
Connection ~ 3700 4650
Wire Wire Line
	3700 4650 4400 4650
Text Label 3950 3900 0    50   ~ 0
VP
Text Label 4650 4750 0    50   ~ 0
IR1
$EndSCHEMATC
