# Hardware

The PCBs for the different elements can be found here.  

They have been created with [Kicad](http://kicad-pcb.org) 5 (free and open source) .

## Local Library

There is a local footprint and component library used by the different Kicad projects.  

Located in the root directory: `LasertagFootprints.pretty` and `LasertagComponents.lib`

It should be configured automatically, but you might have to double check.

## WeaponPCB

#### Generations

Current generation: Very compact PCB featuring some SMD components. Intended to be fabricated panalized to reduce cost and simply solder paste stencil usage ***Sucessfully soldered, functionality untested***

Previous generation: Medium sized PCB using THT components. ***Works reliable***

#### Purpose

This PCB is the heart of each tagger/weapon. It features a microcontroller running the Weapon software to manage functionality. 5V powered. Connected to various LEDs (including IR LED). Connected to MainPCB via I2C (+Power).

## MainPCB

WIP

## ReceiverPCB

Tiny board to attach the IR receiver modules and wire them.  

Small holes for stiching in the corner.

**TODO**: Add second set of wire soldering pads to ease connection of two wires